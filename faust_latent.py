import os, sys, argparse, torch, numpy as np

import surfaces
from tasp.projects.IEVAE.GDVAE import GDVAE
from tasp.projects.IEVAE import DataHandling as DH
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec, run_training
from tasp.structures.PointCloud import PointCloud
from surfaces import Surface


VIS_SETTINGS = {
        'smal' : {
            'R' : np.array([[1.0,           0.0,              0.0],
                            [0, np.cos(np.pi/2), -np.sin(np.pi/2)],
                            [0, np.sin(np.pi/2),  np.cos(np.pi/2)]]),
            'sampling_hpad' : 0.25
        },
        'mnist' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        },
        'smpl' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        }
}


def load_models(AE_model, VAE_model):
    print('Loading AE model', AE_model)
    AE_model = Autoencoder_IEVAE_fixedDec.load(AE_model, mode='eval')
    print('Loading VAE model', VAE_model)
    VAE_model = GDVAE.load(VAE_model, mode='eval')
    print('Converting models to cpu')
    AE_model = AE_model.cpu()
    VAE_model = VAE_model.cpu()
    return AE_model, VAE_model


def vis_latent_save(AE_model, VAE_model, key, filename, output_folder, show=False):
    os.makedirs(output_folder, exist_ok=True)
    file_nude = os.path.basename(filename)
    file, _ = os.path.splitext(file_nude)
    surf = Surface(filename=filename)
    NUM_INTERPS_TO_VIS = 3
    NR, NC = 4, 5
    use_zero_for_zr_in_interps = True
    vsettings = VIS_SETTINGS[key]
    rot_dim = VAE_model.rotation_dim
    ext_dim = VAE_model.extrinsic_dim
    int_dim = VAE_model.intrinsic_dim
    RE_dim = rot_dim + ext_dim
    point_clouds = torch.from_numpy(surf.vertices).float()
    n_sample_points_per_shape = 6890
    only_rotate_z_axis = True
    fixed_R = vsettings['R']
    subsample_pre_display = False # whether to subsample
    subsample_pre_disp_n  = 1100 # Number of points per model to subsample to
    interp_h_padding = 0.05
    interp_v_padding = 0.05
    def to_pc(p, color=None):
        p = PointCloud( p.squeeze(0).detach().cpu().numpy() ).rotate(fixed_R)
        if color is None: # Regular
            p.simple_color_by_coords(2, 0, oth_value=(0.4,0.5)) # Used in paper/poster
        else: # Darker/corner
            p.simple_color_by_coords(2, 2, oth_value=(0.1,0.1)) # Used in paper/poster
        return p
    # Draw two random shapes, and show intrinsic/extrinsic interpolations
    print('\n--Visualizing Interpolations from posterior')
    print('Note: vertical axis is extrinsic; horizontal is intrinsic')
    print('On %s sample %s' % ('data', file))
    from torch.autograd import Variable
    print(point_clouds.shape)
    L = VAE_model.encode( AE_model.encode( Variable(
                        torch.FloatTensor(point_clouds).unsqueeze(0)
                    ) ), return_mu_only = True )
    if use_zero_for_zr_in_interps:
       L[ 0, 0 : rot_dim ] = 0.0
        # Interpolate zEs and zIs
    zE= L[0, 0:RE_dim]
    zI = L[0, RE_dim:]
    np.save(os.path.join(output_folder, "{0}.npy".format(file)),
            [zI.detach().cpu().numpy(), zE.detach().cpu().numpy()])
    if show:
        decoded_pc_rows = to_pc(
                AE_model.decode(
                    VAE_model.decode(
                        z = L
                        )
                    ),
                    color = 'darker')
        if subsample_pre_display:
            print('\tDownsampling from %d to %d' %
                  (decoded_pc_rows[0][0].num_points(), subsample_pre_disp_n))
            decoded_pc_rows = decoded_pc_rows.random_subsample_obj(
                                                    subsample_pre_disp_n)
        # Display the PC
        decoded_pc_rows.display(verbose=False)
        save_mitsuba_dn = False
        if save_mitsuba_dn:
            #combined_vis_grid.set_radius(m_factor=MF)
            decoded_pc_rows.writeToMitsubaFile(
                    str(interp_tag) + '-interp-' + str(KEY) + '-'
                        + str(i) + '.xml',
                    camera_lift=0.5,
                    lookAt_lowering=0.0,
                    img_size=1000,
                    #subsample_points=750,
                    overwrite_output=True)

if __name__ == '__main__':
    faust_path = "/home/human4d/Documents/bases_de_donnees/MPI-FAUST/training/registrations"
    filename = "tr_reg_007.ply"
    AE_model_path = "models/sursmpl2-031919-AE.pt"
    VAE_model_path = "models/sursmpl-GDVAE-032019-T8.pt"
    AE_model, VAE_model = load_models(AE_model_path, VAE_model_path)
    output = "latent"
    #vis_latent_save(AE_model, VAE_model, "smpl", os.path.join(faust_path, filename), output, show=True)
    all_files = [f for f in os.listdir(faust_path) if '.ply' in f]
    for f in all_files:
        vis_latent_save(AE_model, VAE_model, "smpl", os.path.join(faust_path, f), output, show=False)