import torch
import torch.utils.data as data
import json
from tqdm import tqdm
import numpy as np
from human_body_prior.tools.omni_tools import copy2cpu as c2c
from human_body_prior.body_model.body_model import BodyModel
import os


class AMASS_DS(data.Dataset):
    """AMASS: a pytorch loader for unified human motion capture dataset. http://amass.is.tue.mpg.de/"""

    def __init__(self, dataset_file, dataset_folder, bm_paths, num_betas=16, num_dmpls=8, dset="cmu"):
        self.num_betas = num_betas
        self.num_dmpls = num_dmpls
        self.bm_paths = bm_paths[:3]
        self.dmpl_paths = bm_paths[3:]
        mouvs = []
        with open(dataset_file, 'r') as f:
            mouvs_name = json.load(f)
        for id, gt in tqdm(mouvs_name, "Preloading database"):
            id_guy = '_'.join(id.split("_")[:-1])
            if dset == "bml":
                id_guy = '_'.join([id_guy, "MoSh"])
            if dset == "dfaust":
                mouv_file = os.path.join(dataset_folder, id_guy, "{0}_{1}_poses.npz".format(id_guy, gt))
            else:
                mouv_file = os.path.join(dataset_folder, id_guy, "{0}_poses.npz".format(id))
            if os.path.isfile(mouv_file):
                mouvs.append((mouv_file, gt))  # bm_path, l, full_file['betas'][np.newaxis]))
        self.mouvs = mouvs
        bm = BodyModel(bm_fname=self.bm_paths[0], num_betas=self.num_betas)
        self.faces = c2c(bm.f)
        self.device = "cpu"


    def __len__(self):
       return len(self.mouvs)

    def __getitem__(self, idx):
        mouv_file, gt = self.mouvs[idx]
        full_file = np.load(mouv_file)
        gender = full_file["gender"]
        if gender == "male":
            bm_path = self.bm_paths[0]
            dmpl_path = self.dmpl_paths[0]
        elif gender == "female":
            bm_path = self.bm_paths[1]
            dmpl_path = self.dmpl_paths[1]
        else:
            bm_path = self.bm_paths[2]
            dmpl_path = self.dmpl_paths[2]
        with torch.no_grad():
            time_length = full_file["poses"].shape[0]
            body_parms = {
                #'root_orient': torch.Tensor(full_file['poses'][:, :3]).to(self.device),
                # controls the global root orientation
                'pose_body': torch.Tensor(full_file['poses'][:, 3:66]).to(self.device),  # controls the body
                'pose_hand': torch.Tensor(full_file['poses'][:, 66:]).to(self.device),
                # controls the finger articulation
                #'trans': torch.Tensor(full_file['trans']).to(self.device),  # controls the global body position
                'betas': torch.Tensor(
                    np.repeat(full_file['betas'][:self.num_betas][np.newaxis], repeats=time_length, axis=0)).to(self.device),
                # controls the body shape. Body shape is static
                'dmpls': torch.Tensor(full_file['dmpls'][:, :self.num_dmpls]).to(self.device)
                # controls soft tissue dynamics
            }
            #print(l)
            bm = BodyModel(bm_fname=bm_path, num_betas=self.num_betas,
                           dmpl_fname=dmpl_path, num_dmpls=self.num_dmpls).to(self.device)
            body_v = bm.forward(**body_parms).v
            return body_v, self.faces


bml_names = [
    'vertical_jumping',
    'jumping_jack',
    'taking_photo',
    'running_in_spot',
    'walking',
    'scratching_head',
    'throw/catch',
    'hand_clapping',
    'crossarms',
    'jogging',
    'sideways',
    'hand_waving',
    'phone_talking',
    'stretching',
    'pointing',
    'kicking',
    'crawling',
    'dancing_rm',
    'checking_watch',
    'cross_legged_sitting',
    'sitting_down'
]
def to_class_bml(name):
    if name in bml_names:
        return bml_names.index(name)
    else:
        return len(bml_names)

from tasp.projects.IEVAE.GDVAE import GDVAE
from tasp.projects.IEVAE import DataHandling as DH
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec, run_training
from tasp.structures.PointCloud import PointCloud
from surfaces import Surface


VIS_SETTINGS = {
        'smal' : {
            'R' : np.array([[1.0,           0.0,              0.0],
                            [0, np.cos(np.pi/2), -np.sin(np.pi/2)],
                            [0, np.sin(np.pi/2),  np.cos(np.pi/2)]]),
            'sampling_hpad' : 0.25
        },
        'mnist' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        },
        'smpl' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        }
}


def load_models(AE_model, VAE_model):
    print('Loading AE model', AE_model)
    AE_model = Autoencoder_IEVAE_fixedDec.load(AE_model, mode='eval')
    print('Loading VAE model', VAE_model)
    VAE_model = GDVAE.load(VAE_model, mode='eval')
    print('Converting models to cpu')
    AE_model = AE_model.to("cuda:1")
    VAE_model = VAE_model.to("cuda:1")
    return AE_model, VAE_model


def latent_comp(AE_model, VAE_model, point_clouds):
    NUM_INTERPS_TO_VIS = 3
    NR, NC = 4, 5
    use_zero_for_zr_in_interps = True
    vsettings = VIS_SETTINGS["smpl"]
    rot_dim = VAE_model.rotation_dim
    ext_dim = VAE_model.extrinsic_dim
    int_dim = VAE_model.intrinsic_dim
    RE_dim = rot_dim + ext_dim
    n_sample_points_per_shape = 6890
    only_rotate_z_axis = True
    fixed_R = vsettings['R']
    subsample_pre_display = False # whether to subsample
    subsample_pre_disp_n  = 1100 # Number of points per model to subsample to
    interp_h_padding = 0.05
    interp_v_padding = 0.05
    from torch.autograd import Variable
    #print(point_clouds.shape)
    n_ts = point_clouds.shape[0]
    all_Ls = []
    for i in range(0, n_ts, 10):
        L_temp = VAE_model.encode( AE_model.encode( Variable(
                        torch.FloatTensor(point_clouds[i:i+10, :, :]).to("cuda:1")#.unsqueeze(0)
                    ) ), return_mu_only = True )
        all_Ls.append(L_temp.detach().cpu())
        del L_temp
    L = torch.cat(all_Ls, 0)
    if use_zero_for_zr_in_interps:
       L[:, 0 : rot_dim ] = 0.0
        # Interpolate zEs and zIs
    zE= L[:, 0:RE_dim]
    zI = L[:, RE_dim:]
    return zE, zI
def amass_compute(folder, data_file, dset, name, models):
    data_path = os.path.join(output, "dataset_gdvae_{0}.npy".format(name))
    AE_model, VAE_model = models
    if not os.path.exists(data_path):
        dataset_p = AMASS_DS(data_file, folder, bm_paths + dmpl_path, dset=dset)
        dataset_ts = []
        for batch_vertices, faces in tqdm(dataset_p, "Computing GDVAE"):
            pose_ts, _ = latent_comp(AE_model, VAE_model, batch_vertices)
            ts = pose_ts.detach().cpu().squeeze().numpy()
            dataset_ts.append(ts)
        np.save(data_path, dataset_ts)

body_model_folder = "/run/media/d2/LaCie/bases_de_donnees/smpl/smplh_1"
body_model_folder = "/home/human4d/Nextcloud3/smpl/smplh_1"
bm_paths = [os.path.join(body_model_folder, "male", "model.npz"),
            os.path.join(body_model_folder, "female", "model.npz"),
            os.path.join(body_model_folder, "neutral", "model.npz")]
print(bm_paths)
dmpl_folder = "/run/media/d2/LaCie/bases_de_donnees/smpl/dmpls"
dmpl_folder = "/home/human4d/Nextcloud3/smpl/dmpls"
dmpl_path =  [os.path.join(dmpl_folder, "male", "model.npz"),
              os.path.join(dmpl_folder, "female", "model.npz"),
              os.path.join(dmpl_folder, "neutral", "model.npz")]

output = "latent"

#folder = "/home/human4d/Documents/bases_de_donnees/AMASS/BMLmovi/BMLmovi"
folder = "/home/human4d/Documents/bases_de_donnees/AMASS/DFaust67/DFaust_67"
#file = os.path.join(folder, "01/01_01_poses.npz")
#data_file = "CMU_walk_run_jump.json"
data_file = "/home/human4d/PycharmProjects/varifolds/DFAUST.json"
AE_model_path = "models/sursmpl2-031919-AE.pt"
VAE_model_path = "models/sursmpl-GDVAE-032019-T8.pt"
models = load_models(AE_model_path, VAE_model_path)
amass_compute(folder, data_file, "dfaust", "dfaust", models)