import numpy as np
import surfaces
from tqdm import tqdm
import sys, os
from multiprocessing import Pool
import torch

import h5py
def name_to_id(name, case=None):
    if case is None:
        return int("".join(filter(str.isdigit, name)))
    elif case == "Hasler_Stoll":
        with open("hasler_stoll.json", "r") as f:
            index = json.load(f)
        id, pose = id_plus_pose(name)
        return index[str(40*id+pose)]


VIS_SETTINGS = {
        'smal' : {
            'R' : np.array([[1.0,           0.0,              0.0],
                            [0, np.cos(np.pi/2), -np.sin(np.pi/2)],
                            [0, np.sin(np.pi/2),  np.cos(np.pi/2)]]),
            'sampling_hpad' : 0.25
        },
        'mnist' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        },
        'smpl' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        }
}

from tasp.projects.IEVAE.GDVAE import GDVAE
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec

def load_models(AE_model, VAE_model):
    print('Loading AE model', AE_model)
    AE_model = Autoencoder_IEVAE_fixedDec.load(AE_model, mode='eval')
    print('Loading VAE model', VAE_model)
    VAE_model = GDVAE.load(VAE_model, mode='eval')
    print('Converting models to cpu')
    AE_model = AE_model.to("cuda:0")
    VAE_model = VAE_model.to("cuda:0")
    return AE_model, VAE_model

AE_model_path = "models/sursmpl2-031919-AE.pt"
VAE_model_path = "models/sursmpl-GDVAE-032019-T8.pt"
models = load_models(AE_model_path, VAE_model_path)

def latent_comp(AE_model, VAE_model, point_clouds):
    use_zero_for_zr_in_interps = True
    vsettings = VIS_SETTINGS["smpl"]
    rot_dim = VAE_model.rotation_dim
    ext_dim = VAE_model.extrinsic_dim
    int_dim = VAE_model.intrinsic_dim
    RE_dim = rot_dim + ext_dim
    from torch.autograd import Variable
    #print(point_clouds.shape)
    L = VAE_model.encode( AE_model.encode( Variable(
                    torch.FloatTensor(point_clouds).to("cuda:0")#.unsqueeze(0)
                ) ), return_mu_only = True )
    if use_zero_for_zr_in_interps:
       L[:, 0 : rot_dim ] = 0.0
        # Interpolate zEs and zIs
    zE= L[:, 0:RE_dim].squeeze().detach().cpu().numpy()
    zI = L[:, RE_dim:].squeeze().detach().cpu().numpy()
    return zE, zI

def representation(vertices, faces):
    #print(res_len.shape, res.shape)
    point_cloud = torch.from_numpy(vertices).float().unsqueeze(0)
    zE, _ = latent_comp(models[0], models[1], point_cloud)
    return zE


def doing_things(dat):
    i, file = dat
    try:
        return representation(*dat)
    except:
        return None


def experience(surface):
    # print(res_len.shape, res.shape)
    point_cloud = torch.from_numpy(surface.vertices).float().unsqueeze(0)
    zE, _ = latent_comp(models[0], models[1], point_cloud)
    return zE

def opening_things(dat):
    i, file = dat
    surface = surfaces.Surface(surf=None, filename=os.path.join(path_mouv, file), FV=None)
    res = experience(surface)
    return res #np.hstack((res, res_len))



def similarity_GT(files):
    global path_mouv
    surface = surfaces.Surface(filename=os.path.join(path_mouv, files[0]))
    verts = []
    for i, file in enumerate(files):
        try:
            surface = surfaces.Surface(filename=os.path.join(path_mouv, file))
        except:
            print("Problem with file: {0}".format(file))
        verts.append(surface.vertices)
    verts = np.array(verts) # N_samples, N_verts, 3
    CP = np.linalg.norm(verts[:, np.newaxis, :, :] - verts[np.newaxis, :, :, :], axis=3).mean(axis=2)
    V = verts[1:, :, :] - verts[:-1, :, :]
    CV_temp = np.linalg.norm(V[:, np.newaxis, :, :] - V[np.newaxis, :, :, :], axis=3).mean(axis=2)
    CV = np.zeros((len(files), len(files)))
    CV[:-1, :-1] = CV_temp
    return CP, CV


def from_data(dat):
    if dat is not None:
        return representation(dat[0], dat[1])


def run_imap_multiprocessing(func, argument_list, num_processes):

    pool = Pool(processes=num_processes)

    result_list_tqdm = []
    for result in tqdm(pool.imap(func=func, iterable=argument_list), total=len(argument_list)):
        if result is not None:
            result_list_tqdm.append(result)

    return result_list_tqdm


def run_linear(func, argument_list):
    result_list_tqdm = []
    for data in tqdm(argument_list):
        result = func(data)
        if result is not None:
            result_list_tqdm.append(result)

    return result_list_tqdm



def compute_database(data_path, res_file, datype=True, multi=False):
    global path_mouv
    persons = [f for f in os.listdir(data_path) if os.path.isdir(os.path.join(data_path, f))]
    with h5py.File(res_file, 'w') as f:
        for person in persons:
            print(person)
            person_path = os.path.join(data_path, person)
            mouvs = [f for f in os.listdir(person_path) if os.path.isdir(os.path.join(data_path, person, f))]
            for mouv in mouvs:
                the_func = None
                print("Person: {0}, Mouvement {1}".format(person, mouv))
                if datype:
                    path_mouv = os.path.join(person_path, mouv)
                    mouv_files = list_mouv_files(path_mouv)
                    the_func = opening_things
                    mouv_files = list(zip(range(len(mouv_files)), mouv_files))
                else:
                    path_mouv = os.path.join(person_path, "{0}.npy".format(mouv))
                    mouv_files = np.load(path_mouv, allow_pickle=True)
                if multi:
                    res_fin = run_imap_multiprocessing(the_func, mouv_files, 10)
                else:
                    res_fin = run_linear(the_func, mouv_files)
                f.create_dataset(person + "_" + mouv, data=res_fin)
                # f.create_dataset(person + "_" + mouv + '_rot', data=rotate)


def list_mouv_files(mouv_path):
    files = [f for f in os.listdir(mouv_path) if ".tri" in f or ".ntri" in f]
    return sorted(files, key=lambda x: int(''.join(filter(str.isdigit, x))))


if __name__ == '__main__':
    data_disk = "/home/human4d/Documents/bases_de_donnees/"
    data_disk = "/media/data/these_datasets/cvssp3D"
    real_path = os.path.join(data_disk, "real_for_animation/Man1/Model/")
    synt_path = os.path.join(data_disk, "synthetic")
    file_res = "/run/media/d2/Disque_de_Zonze/emeru/these/cvssp3d/datas_cvssp_real_gdvae.h5py"
    file_synth = "/run/media/d2/Disque_de_Zonze/emeru/these/cvssp3d/datas_cvssp_artificial_gdvae.h5py"
    path_mouv = None
    compute_database(real_path, file_res)
    compute_database(synt_path, file_synth)
