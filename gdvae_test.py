import os, sys, numpy as np, gc, time, random, math, pickle, json, pathlib
from collections import Counter

from tasp import FullTriangleMesh
from tasp.structures.PointCloud import PointCloud
from tasp.projects.IEVAE import DataHandling as DH
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec
from tasp.projects.IEVAE.GDVAE import GDVAE
from tasp.learning.Utils import full_chamfer_over_sets
from tasp.viewing.Color import Color

import torch
from torch.autograd import Variable
from torch.distributions import MultivariateNormal as MVN
import numpy.random as npr
import matplotlib.pyplot as plt

### DATASET SETTINGS ###
DATASETS_LOCATION = 'data'
DATASETS = { 'mnist'      : { 'train' : 'mnist-combined-training-tasp_meshes.pickle',
                              'test'  : 'mnist-combined-test-tasp_meshes.pickle' },
           }

SETTINGS = {
    'mnist' : {
        'AE_dim'      : 250,
        'DIM_EXT'     : 5,
        'DIM_INT'     : 5,
        'EncLSs'      : (1000, 750, 500),
        'MuS'         : 300,
        'SigmaS'      : 300,
        'DecLSs'      : (500, 750, 1000),
        'RotLSs'      : (300, 200),
        'LboLSs'      : (500, 400),
        'lbo_dim'     : 40,
        'BETA_4'      : 50,
        'SpecLoss'    : 1000,
        'B'           : 200,
        'WD'          : 5e-6,
        'N_AE_samps'  : 1000,
        'LR'          : 0.0001,
        'rot_axis'    : 'z',
        'N_EPOCHs'    : 150,
        'GAMMA'       : 1.0,
        'JLW'         : 1.0,
        'LLW'         : 250.0, # in [10, 300]
        'useJac'      : True,
        'max_rot_ang' : np.pi / 5,
        'name_tag'    : '-test'
    },
    'smal' : {
        'AE_dim'      : 400,
        'DIM_EXT'     : 8,
        'DIM_INT'     : 5,
        'EncLSs'      : (1000, 750, 500),
        'MuS'         : 300,
        'SigmaS'      : 300,
        'DecLSs'      : (500, 750, 1000),
        'RotLSs'      : (300, 200),
        'LboLSs'      : (300, 300),
        'lbo_dim'     : 50,
        'BETA_4'      : 50,
        'SpecLoss'    : 1000, 
        'B'           : 250,
        'N_AE_samps'  : 1500,
        'LR'          : 0.0001,
        'rot_axis'    : 'z',
        'N_EPOCHs'    : 200, # 
        'GAMMA'       : 100.0,
        'JLW'         : 10.0, #
        'LLW'         : 1.0,
        'useJac'      : True,
        'max_rot_ang' : np.pi / 12,
        'name_tag'    : '-test',
    },
    'sursmpl' : {
        'AE_dim'      : 300, 
        'DIM_EXT'     : 12,
        'DIM_INT'     : 5,
        'EncLSs'      : (1000, 750, 500),
        'MuS'         : 300,
        'SigmaS'      : 300,
        'DecLSs'      : (500, 750, 1000),
        'RotLSs'      : (300, 200),
        'LboLSs'      : (300, 400),
        'lbo_dim'     : 40,
        'BETA_4'      : 50,
        'SpecLoss'    : 500,
        'B'           : 250,
        'N_AE_samps'  : 2000,
        'LR'          : 0.0001,
        'rot_axis'    : 'z',
        'N_EPOCHs'    : 100,
        'GAMMA'       : 10.0, # 
        'JLW'         : 10.0, # 
        #'WD'          : 1e-3,
        'LLW'         : 200.0,
        'useJac'      : True,
        'max_rot_ang' : np.pi / 12,
        'name_tag'    : '-test'
    },
    'dyna' : {
        'AE_dim'      : 250,
        'DIM_EXT'     : 10,
        'DIM_INT'     : 10,
        'EncLSs'      : (1000, 750, 500),
        'MuS'         : 250,
        'SigmaS'      : 250,
        'DecLSs'      : (500, 750, 1000),
        'RotLSs'      : (300, 200),
        'LboLSs'      : (500, 400),
        'lbo_dim'     : 100,
        'BETA_4'      : 25,
        'SpecLoss'    : 1000,
        'B'           : 200,
        'N_AE_samps'  : 2000,
        'LR'          : 0.0001,
        'rot_axis'    : 'z',
        'N_EPOCHs'    : 150,
        'GAMMA'       : 5.0,
        'JLW'         : 5.0,
        'WD'          : 5e-6,
        'LLW'         : 250.0,
        'useJac'      : True,
        'max_rot_ang' : np.pi / 5,
        'name_tag'    : '-r'
    }
}

def main_train(AE_model_path, key, gpuid=0, tag=None):

    # Use to specify spectral noise
    # ! Change whether to use PCLBOs in the DataHandling class !
    # Note: zeroify actually replaces with Gaussian noise

    #----# Spectral Noise Options #----#
    S_NOISE_TYPE = None #'zeroify' # 'mgaussian' #None 'mgaussian' # interp
    INTERP_NOISE_ALPHA = None # 0.1
    MG_NOISE_SIGMA = 0.0 # 0.01 # 0.9
    MONOTONE_CORRECTION = True # sorting
    #----------------------------------#

    dataset = key
    gpuid_input = int(gpuid)
    assert dataset in SETTINGS.keys()
    OPTS = SETTINGS[dataset]
    OPTS['AE'] = AE_model_path
    if not tag is None:
        OPTS['name_tag'] = tag

    #################################################################
    ### AE Settings ###
    DATA_DIM = OPTS['AE_dim'] # This is the LATENT DIM of the AE model
    USE_GPU = True
    AE_MODEL_PATH = OPTS['AE']
    ### VAE Settings ###
    # Dimensions
    ROTATION_DIMENSIONALITY = 3 # Fixed
    #if USING_DYNA:
    EXTRINSIC_DIMENSIONALITY = OPTS['DIM_EXT']
    INTRINSIC_DIMENSIONALITY = OPTS['DIM_INT']
    # IE Encoder layers
    ENCODER_LAYER_SIZES = OPTS['EncLSs'] #(1000, 750, 500) # Ends with batchnorm and relu
    MU_SIZES = OPTS['MuS'] # (250)
    SIGMA_SIZES = OPTS['SigmaS'] # (250)
    # IE Decoder layers
    DECODER_LAYER_SIZES = OPTS['DecLSs'] # (500, 750, 1000)
    # Rotation layer sizes
    ROTATION_AUTOENC_SIZES = OPTS['RotLSs'] # (300, 200) # 4 -> ~3
    # Spectral predictor layer sizes
    LBO_LAYER_SIZES = OPTS['LboLSs'] # (500, 400)
    ### Data Settings ###
    LBO_DIMENSIONALITY = OPTS['lbo_dim'] #_MNIST = 40
    #LBO_DIMENSIONALITY_DYNA  = 100
    ### Training Settings ###
    #-- Constants --#
    BETA = (  ## Collection of KL divergence loss weights ##
        1.000,          # Weight on (i), the sum of intra-group TC
        1.000,          # Weight on (ii), the sum of dimension-wise KL divergences
        1.000,          # Weight on (2), the mutual information between x & z
        OPTS['BETA_4'], # Weight on (A), the inter-group TC
        0.000           # Weight on supervised error term
    )
    # Log likelihood
    # Weight on the reconstruction error (W_LL) [note quat err weight as well]
    LL_WEIGHT = OPTS['LLW'] # 
    REL_QUAT_WEIGHT = 10 # 
    # Covariance penalty coefficients
    GAMMA_INTER = OPTS['GAMMA'] # Inter-group covariance
    LAMBDA_INTRA_GROUP_ON_DIAG = 0.00 # Note these two are present in the prior matcher
    LAMBDA_INTRA_GROUP_OFF_DIAG = 0.00
    # Jacobian norm loss
    JACOBIAN_LOSS_WEIGHT = OPTS['JLW'] 
    # Spectral loss coefficient
    ZETA_LBO = OPTS['SpecLoss'] 
    #-- Algo Settings --#
    # Dataset info
    dataset_loc = DATASETS_LOCATION
    dataset_suffix = 'pickle'
    # Training algo parameters
    N_EPOCHS = OPTS['N_EPOCHs'] 
    BATCH_SIZE = OPTS['B']
    LEARNING_RATE = OPTS['LR'] 
    DATAFILE_COVERINGS = 1
    N_SAMPLES_FOR_AE_ENCODING = OPTS['N_AE_samps']
    if not 'WD' in OPTS.keys(): OPTS['WD'] = 0.0
    WEIGHT_DECAY_CONST = OPTS['WD'] #5e-6 # 1e-7 # 5e-7
    #-- Meta-params --#
    PRINT_EVERY = 50
    SAVE_EVERY = 100
    ALLOW_MODEL_OVERWRITE = False # Whether to train from scratch
    EXIT_EARLY_NB = None
    GPUID = gpuid_input
    USE_GPU = True
    #-- Data Control and Augmentation --#
    ROTATE_DATA = True
    MAX_ROT_ANGLE = OPTS['max_rot_ang'] # in [0, pi]
    PRELOAD_FILES = True
    #################################################################

    dataset_prefix = DATASETS[dataset]['train']

    ### Prepare Data ###
    is_data_file = lambda s: (s.startswith(dataset_prefix) and
                              s.endswith(dataset_suffix))
    dataset_files = [ f for f in os.listdir(dataset_loc) if is_data_file(f) ]
    train_files = [ os.path.join(dataset_loc, f) for f in dataset_files ]
    print('Found data files:', dataset_files)
    assert len(dataset_files) > 0, "No data file found (prefix %s)" % dataset_prefix

    if   OPTS['rot_axis'] == 'y':
        ROTATE_Y_ONLY = True
        ROTATE_Z_ONLY = False
    elif OPTS['rot_axis'] == 'z':
        ROTATE_Z_ONLY = True
        ROTATE_Y_ONLY = False
    else:
        print('Unknown rot axis')
        sys.exit(0)

    AE_MODEL_PATH = OPTS['AE']

    ### Save name ###
    import datetime as dt
    cdt = dt.datetime.today().strftime('%m%d%y')
    # GDVAE-DATE-DATA-TAG
    SAVE_NAME_TAG = OPTS['name_tag']
    SAVE_NAME = dataset + '-GDVAE-' + cdt # + '-' + dataset # + '-'
    SAVE_NAME += SAVE_NAME_TAG + '.pt'

    ### Create Model ###
    print('-- Creating Model --')
    vae_model = GDVAE(
        ## Dimensionalities ##
        data_dim = DATA_DIM,
        rotation_dim = ROTATION_DIMENSIONALITY,
        intrinsic_dim = INTRINSIC_DIMENSIONALITY,
        extrinsic_dim = EXTRINSIC_DIMENSIONALITY,
        ## Layer Sizes ##
        encoder_sizes = ENCODER_LAYER_SIZES,
        mu_sizes = MU_SIZES,
        sigma_sizes = SIGMA_SIZES,
        decoder_sizes = DECODER_LAYER_SIZES,
        rotation_sizes = ROTATION_AUTOENC_SIZES,
        ## Spectral Information ##
        lbo_dim = LBO_DIMENSIONALITY,
        lbo_sizes = LBO_LAYER_SIZES,
        ## AHD layer sizes ##
        use_jacobian = OPTS['useJac']
    )

    vae_model.OPTS = OPTS

    if GPUID == -1: 
        print('Using CPU')
        USE_GPU = False
    DEVICE = torch.device("cuda:%d" % GPUID) if USE_GPU else torch.device('cpu')

    print('Architecture')
    print(vae_model)
    print('Dimensionalities')
    vae_model.print_dims()

    ### Run Training ###
    print('-- Starting Training --')
    vae_model.run_training(
        ### Meta-parameters ###
        dataset_files = train_files,
        path_to_save_model = SAVE_NAME,
        ## Point Cloud Autoencoder ##
        path_to_AE = AE_MODEL_PATH,
        ### Training parameters ###
        num_epochs = N_EPOCHS,
        batch_size = BATCH_SIZE,
        learning_rate = LEARNING_RATE,
        dataset_coverings = DATAFILE_COVERINGS,
        input_shape_sample_size = N_SAMPLES_FOR_AE_ENCODING, # 
        ### Meta-parameters ###
        print_every = PRINT_EVERY,
        save_every = SAVE_EVERY,
        allow_initial_overwrites = ALLOW_MODEL_OVERWRITE,
        device = DEVICE,
        ## Loss Parameters ##
        BETA = BETA, # beta in beta-VAE
        W_LL = LL_WEIGHT,
        GAMMA = GAMMA_INTER,
        LAMBDA_OFF = LAMBDA_INTRA_GROUP_OFF_DIAG,
        LAMBDA_DIA = LAMBDA_INTRA_GROUP_ON_DIAG,
        use_covar_p = True,
        ZETA_lbo = ZETA_LBO,
        Jacobian_coef = JACOBIAN_LOSS_WEIGHT,
        # Weight decay
        WEIGHT_DECAY_CONST = WEIGHT_DECAY_CONST,
        ### Training data params ###
        rotate_training_data = ROTATE_DATA,
        only_rotate_z_axis = ROTATE_Z_ONLY,
        only_rotate_y_axis = ROTATE_Y_ONLY,
        max_rotation_angle = MAX_ROT_ANGLE,
        preload_files = PRELOAD_FILES,
        RELATIVE_QUAT_WEIGHT = REL_QUAT_WEIGHT,
        #
        noise_type = S_NOISE_TYPE,
        interp_noise_alpha = INTERP_NOISE_ALPHA,
        mg_noise_sigma = MG_NOISE_SIGMA,
        monotone_correction = MONOTONE_CORRECTION
        )

#################################################################
if __name__ == '__main__':
    main()
#################################################################



#
