# TTAA

import os, sys


OFF_EXT = ".off"
OBJ_EXT = ".obj"


def convertOffToObj(fileIn, fileOut=None, verbose=False):
    assert os.path.exists(fileIn), fileIn + ' not found!'
    # for file in os.listdir("."):
    #     if file.endswith(targext):
    with open(fileIn,"r") as r:
        lines = r.readlines()
        newFile = fileIn.replace(OFF_EXT, OBJ_EXT) if fileOut is None else fileOut
        assert not os.path.exists(newFile), fileOut + ' already exists!'
        if verbose: print("Processing "+str(fileIn))
        nv, nf = [int(k) for k in lines[1].split()][0:2]
        startLine = 2
        vertLines = [ a for a in lines[startLine:nv+startLine] if not a == '' ]
        faceLines = [ a for a in lines[nv+startLine:] if not a == '' ]
        assert len(vertLines) == nv, "Vertex number length mismatch "+str(len(vertLines))+" vs "+str(nv)
        assert len(faceLines) == nf, "Face number length mismatch "+str(len(faceLines))+" vs "+str(nf)

    with open(newFile,"w") as w:
        w.write("# File type: ASCII OBJ\n")
        for vl in vertLines:
            w.write("v " + vl)
        for fl in faceLines:
            q = [ str(int(s)+1) for s in fl[2:].split() ]
            w.write("f " + " ".join(q) + "\n")
        if verbose: print("\tGenerated " + str(newFile))


















