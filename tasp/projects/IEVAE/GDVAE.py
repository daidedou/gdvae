import torch, time, os, sys, numpy as np
from torch import nn, optim
from torch.autograd.gradcheck import zero_gradients
from torch.nn import functional as F
from torch.autograd import Variable
from .DataHandling import IEVAE_Dataset
from ...learning.Utils import partial_chamfer_and_Hausdorff_over_sets, load_mnist, count_parameters as n_parms
from ...learning.Utils import StandardSequentialBNRL
import probtorch
from .MarginalObjectives import elbo
from .gdvae_autoencoder import Autoencoder_IEVAE_fixedDec

class GDVAE(nn.Module):
    '''
    A Vector2Vector Variational Autoencoder

    The DIP-VAE-I is a special case when the intergroup weight equals the
    off-diagonal intra-group weight.
    The beta-VAE is a special case when the covariance penalties are zero.
    '''

    def __init__(self,
                 ## Dimensionalities ##
                 data_dim,
                 rotation_dim,
                 intrinsic_dim,
                 extrinsic_dim,
                 ## Layer Sizes ##
                 encoder_sizes,
                 mu_sizes,
                 sigma_sizes,
                 decoder_sizes,
                 rotation_sizes,
                 ## Spectral Information ##
                 lbo_dim,
                 lbo_sizes, # Set to None to get a single linear transform (no nonlin)
                 ## AHD layer sizes ##
                 #intToExt_sizes,
                 #extToInt_sizes
                 use_jacobian=False
                 ):

        ############ Super-constructor ############
        super(GDVAE, self).__init__()

        ############ Dimensionalities ############
        self.DD = data_dim      # Input/output size
        self.lbo_dim = lbo_dim  # Spectrum length
        self.AEDIM_R = 4 # rotation quat representation dimensionality
        self.DD_NON_R = self.DD - self.AEDIM_R # Data dim without rotation quat
        # Latent rotation state
        self.rotation_dim  = rotation_dim
        # Latent non-rotational extrinsic state
        self.extrinsic_dim = extrinsic_dim
        # Latent intrinsic state
        self.intrinsic_dim = intrinsic_dim
        # Latent bottleneck dimensionality
        self.LD = rotation_dim + extrinsic_dim + intrinsic_dim
        self.IELD = extrinsic_dim + intrinsic_dim # latent dimensionality without rotation

        ############ Layer Sizes & Settings ############
        # Sizes of the encoder BEFORE applying the learned mu and sigma
        self.enc_sizes = encoder_sizes
        # Layer sizes of encoding mu and sigma
        self.mu_sizes = mu_sizes
        self.sigma_sizes = sigma_sizes
        # Decoder layer sizes
        self.dec_sizes = decoder_sizes # e.g. [300, 300]
        # rotation encoder and decoder are the same sizes (in reverse)
        self.enc_rotation_sizes = rotation_sizes
        self.dec_rotation_sizes = rotation_sizes[::-1] # Reverse layers
        # Direct spectral learner
        self.lbo_sizes = lbo_sizes
        # Sizes of intrinsic and extrinsic adversarial disentanglers
        #self.intToExt_sizes = intToExt_sizes
        #self.extToInt_sizes = extToInt_sizes
        # n_samples used to Monte Carlo estimate the ELBO
        self.num_mc_samples = 1
        self.use_jacobian = use_jacobian

        ############ Indices ############
        # Latent subgroups
        # z_R = z[:, 0 : self.b1]
        # z_E = z[:, self.b1 : self.b2]
        # z_I = z[:, self.b2 : ]
        self.b1 = self.rotation_dim
        self.b2 = self.extrinsic_dim + self.rotation_dim

        ############ Encoder ############
        self.encoder_L1 = StandardSequentialBNRL(self.DD_NON_R, self.enc_sizes[-1],
                                                 sizes = self.enc_sizes[ : -1], append_BN_NL=True)
        self.encoder_mu = StandardSequentialBNRL(self.enc_sizes[-1], self.IELD,
                                                 sizes = self.mu_sizes)
        self.encoder_logvar = StandardSequentialBNRL(self.enc_sizes[-1], self.IELD,
                                                     sizes = self.sigma_sizes)
        #> Rotation encoder
        # Map 4d quat to another representation
        self.rotation_encoder = StandardSequentialBNRL(self.AEDIM_R, self.enc_rotation_sizes[-1],
                                                 sizes = self.enc_rotation_sizes[ : -1], append_BN_NL=True)
        self.R_enc_mu = nn.Linear(self.enc_rotation_sizes[-1], self.rotation_dim)
        self.R_enc_logvar = nn.Linear(self.enc_rotation_sizes[-1], self.rotation_dim)

        ############ Decoder ############
        #> Maps intrinsic and extrinsic latent vectors to the non-rotation data dimensionality
        self.decoder = StandardSequentialBNRL(self.IELD, self.DD_NON_R, sizes = self.dec_sizes)
        #> Rotation decoder: Map inner rotation representation to a 4d quat
        self.rotation_decoder = StandardSequentialBNRL(self.rotation_dim, self.AEDIM_R,
                                                       sizes=self.dec_rotation_sizes)

        ############ Spectral Learner ############
        #> LBO spectrum learner
        self.lbo_predictor = StandardSequentialBNRL(self.intrinsic_dim, self.lbo_dim, sizes = self.lbo_sizes)

        ############ Adversarial Hierarchical Disentanglement predictors############
        #self.extrinsic_from_intrinsic = None
        #StandardSequentialBNRL(self.intrinsic_dim, self.extrinsic_dim,
                                       #                       sizes = self.intToExt_sizes)
        #self.intrinsic_from_extrinsic = None
        #StandardSequentialBNRL(self.extrinsic_dim, self.intrinsic_dim,
        #                                                       sizes = self.extToInt_sizes)

    #</ ----------------- End of constructor ----------------- />#

    def encode(self, x, return_mu_only=False, return_mu_with_q=False):
        '''
        Returns a probtorch trace object q that contains values for
        z = (z_R, z_E, z_I), after probabilistic sampling.

        If return_mu_only, we return the mu values alone instead
        '''
        q = probtorch.Trace()
        # Unpack input vector
        DIM_ROT_AE_X = self.AEDIM_R
        R = x[:, 0:DIM_ROT_AE_X ] # since x = (quat, non-quat), segregate R out
        x = x[:, DIM_ROT_AE_X:  ]
        x = self.encoder_L1(x) # initial x, before mu and logvar
        R = self.rotation_encoder(R) # initial R, before mu and logvar
        # The mu and sigma for R and z are computed completely separately,
        # then concatenated at the end. Note that R is always at the front.
        full_mu = torch.cat( (self.R_enc_mu(R), self.encoder_mu(x)), dim=1)
        mu_R = full_mu[:, 0:self.b1] #.unsqueeze(0)
        mu_E = full_mu[:, self.b1:self.b2] #.unsqueeze(0)
        mu_I = full_mu[:, self.b2 : ] #.unsqueeze(0)
        # Ensure the means compute and retain the gradients
        mu_R.requires_grad_(True)
        mu_R.retain_grad()
        mu_E.requires_grad_(True)
        mu_E.retain_grad()
        mu_I.requires_grad_(True)
        mu_I.retain_grad()
        # If we only care about the mean encoding value, return early
        if return_mu_only:
            return full_mu
        else:
            # Get the log variance otherwise (rather, log stddev)
            # Don't forget to exponentiate it
            full_logvar = torch.cat(
                (self.R_enc_logvar(R), self.encoder_logvar(x)), dim=1).exp()
            # Compute encoded z values and store in trace
            # Unsqueeze as sample dimension
            # Rotation group z_R
            z_R = q.normal(
                     loc = mu_R.unsqueeze(0),
                     scale = full_logvar[:, 0:self.b1].unsqueeze(0),
                     name = 'z_R'
                  )
            # Extrinsic group z_E
            z_E = q.normal(
                     loc = mu_E.unsqueeze(0),
                     scale = full_logvar[:, self.b1:self.b2].unsqueeze(0),
                     name = 'z_E'
                  )
            # Intrinsic group z_I
            z_I = q.normal(
                     loc = mu_I.unsqueeze(0),
                     scale = full_logvar[:, self.b2 : ].unsqueeze(0),
                     name = 'z_I'
                  )
            # Note that the values of each variable group are stored within q
            # Hence we don't need to return anything else
            if return_mu_with_q:
                return q, (mu_R, mu_E, mu_I)
            return q

    def decode(self, q=None, z=None): #, device=None):
        '''
        Decodes the latent VAE vector into the AE latent vector.

        If q is given, we return a probtorch trace p, which can be used to compute
        a probabilistic marginal ELBO loss, as well as the decoded X_hat, as (p, X_hat)

        If z is given, return X_hat alone.
        Use this when you simply want to decode as a deterministic autoencoder.
        '''

        # Only prohibited case is if both q and z are given or neither are
        if (q is None) and (z is None):
            pex('At least one of q or z must be given to decode')
        if (not q is None) and (not z is None):
            pex('Only one of q or z can be given to decode')
        if (not type(q) is probtorch.stochastic.Trace) and (not q is None):
            pex("q must be a probtorch trace")

        ### Case 1: q is given ###
        # Note the priors are defined in the training loop
        if z is None:
            # Dims
            nmcs, B, dim_zR = q['z_R'].dist.mean.shape
            # Decoder distribution
            p = probtorch.Trace()
            # Latent rotation sample
            z_R = p.normal(
                    loc = self.latent_prior_zR_mean,
                    scale = self.latent_prior_zR_stddev,
                    value = q['z_R'],
                    name = 'z_R'
                  )
            # Latent extrinsic sample
            z_E = p.normal(
                    loc = self.latent_prior_zE_mean,
                    scale = self.latent_prior_zE_stddev,
                    value = q['z_E'],
                    name = 'z_E'
                  )
            # Latent intrinsic sample
            z_I = p.normal(
                    loc = self.latent_prior_zI_mean,
                    scale = self.latent_prior_zI_stddev,
                    value = q['z_I'],
                    name = 'z_I'
                  )
            z = torch.cat( (z_R.squeeze(0), z_E.squeeze(0), z_I.squeeze(0)), dim=1 )

        ### Case 2: q is not given ###
        # Instead z is given directly, and the decoder is run deterministically
        ### Run decodings (to the AE latent space) ###
        X_hat = torch.cat( ( self.rotation_decoder(z[:, 0:self.rotation_dim]),
                             self.decoder(z[:, self.rotation_dim:]) ), dim=1)

        ### Cases: no spectra
        # Note: z is computed from q if it is not explicitly given
        if (q is None): return X_hat
        return (p, X_hat)

    # Simply calls encode and decode to push the input through the network
    def forward(self, x, mu_only=False, return_mu_with_q=False):
        assert not (mu_only and return_mu_with_q)
        # Deterministic case, using autoencoder directly
        if mu_only:
            z_mu = self.encode(x, return_mu_only=mu_only)
            X_hat = self.decode(z = z_mu)
            return z_mu, X_hat
        else:
            if return_mu_with_q:
                q, muset = self.encode(x, return_mu_with_q=return_mu_with_q)
                p, X_hat = self.decode(q=q)
                return q, p, X_hat, muset
            q = self.encode(x) # Note the latent z values are in q
            p, X_hat = self.decode(q=q)
            return q, p, X_hat

    def sample_from_prior(self, n_samples, AE_model=None, as_numpy=False):
        '''
        Decodes n samples from N(0,I).
        If the AE_model is given, it decodes the result as well.
        '''
        latent_prior_vectors = torch.randn(n_samples, self.LD)
        latent_PCs = self.decode(z=latent_prior_vectors)
        rett = lambda a: a.detach().cpu().numpy() if as_numpy else a
        if not AE_model is None: return rett( AE_model.decode(latent_PCs) )
        else:                    return rett(latent_PCs)

    def _flatten_(l): return [item for sublist in l for item in sublist]

    #def ahd_params(self):
    #    return GDVAE._flatten_([ self.extrinsic_from_intrinsic.parameters(),
    #                             self.intrinsic_from_extrinsic.parameters() ])

    def vae_params(self):
        return GDVAE._flatten_([ self.encoder_L1.parameters(),
                                    self.encoder_mu.parameters(),
                                    self.encoder_logvar.parameters(),
                                    self.rotation_encoder.parameters(),
                                    self.R_enc_mu.parameters(),
                                    self.R_enc_logvar.parameters(),
                                    self.decoder.parameters(),
                                    self.rotation_decoder.parameters(),
                                    self.lbo_predictor.parameters()       ])

################################################################################

    def run_training(self,
              #### Data Files ####
              dataset_files,
              #### Model ####
              path_to_save_model,
              #### Point Cloud Autoencoder ####
              path_to_AE,
              #### Training Parameters ####
              num_epochs,
              #num_ext_to_lbo_iters,
              batch_size,
              learning_rate,
              dataset_coverings,
              input_shape_sample_size, # When sampling PCs, before encoding
              #### Meta-parameters ####
              print_every,
              save_every,
              allow_initial_overwrites, # Whether to overwrite the model
              device,
              #### Loss Parameters ####
              # HFVAE weights
              BETA, # Decomposed KL weight set
              W_LL, # Weight for the reconstruction error (log-likelihood)
              # Covariance penalty weights
              GAMMA,
              LAMBDA_OFF,
              LAMBDA_DIA,
              use_covar_p, # CURRENTLY UNUSED
              # Spectral weight
              ZETA_lbo,
              # Jacobian penalty
              Jacobian_coef,
              # AHD
              #add_ahd_term_to_loss,
              #adv_loss_coef,
              #ahd_batch_size, # Batch size for training adversarial learners
              #apply_ahd_gradient_projection,
              #adv_loss_bound,
              # Weight decay
              WEIGHT_DECAY_CONST,
              #### Data Augmentation ####
              rotate_training_data,
              only_rotate_z_axis,
              only_rotate_y_axis,
              max_rotation_angle,
              preload_files,
              RELATIVE_QUAT_WEIGHT,
              #
              noise_type, # = 'interp',
              interp_noise_alpha, # = 0.2
              mg_noise_sigma,
              monotone_correction
        ):

        assert len(BETA) == 5, "Five weights must be present in BETA"
        assert abs(BETA[4]) < 1e-7, "Supervised weight (beta_5) should be 0"

        #assert apply_ahd_gradient_projection in [True, False], "AHD GP must be true or false"
        #if apply_ahd_gradient_projection or add_ahd_term_to_loss:
        #    assert num_ext_to_lbo_iters > 0, "Need to train ahd adversaries to use them"
        #apply_ahd = num_ext_to_lbo_iters > 0 or apply_ahd_gradient_projection or add_ahd_term_to_loss

        self.training_params = locals()

        # Ascertain AHD is shut off
        #assert apply_ahd == False, "AHD disabled here"

        # Define tensors for the prior
        B = batch_size
        self.latent_prior_zR_mean   = torch.zeros(self.num_mc_samples, B, self.rotation_dim).to(device)
        self.latent_prior_zR_stddev = torch.ones(self.num_mc_samples,  B, self.rotation_dim).to(device)
        self.latent_prior_zE_mean   = torch.zeros(self.num_mc_samples, B, self.extrinsic_dim).to(device)
        self.latent_prior_zE_stddev = torch.ones(self.num_mc_samples,  B, self.extrinsic_dim).to(device)
        self.latent_prior_zI_mean   = torch.zeros(self.num_mc_samples, B, self.intrinsic_dim).to(device)
        self.latent_prior_zI_stddev = torch.ones(self.num_mc_samples,  B, self.intrinsic_dim).to(device)

        ### Preload files, if needed ###
        # Also compute the total number of shapes and training bias
        N = 0
        if preload_files:
            print('Preloading files')
            preloaded_files = {
                f_name : IEVAE_Dataset.read_from_file(f_name,
                                            file_filter=None,
                                            noise_type=noise_type,
                                            interp_noise_alpha=interp_noise_alpha,
                                            mgaussian_noise_sigma=mg_noise_sigma,
                                            monotone_correction=monotone_correction)
                for f_name in dataset_files
            }
            print('\tFinished')
            for f_name in dataset_files: N += preloaded_files[f_name].num_shapes
        else:
            for f_name in dataset_files:
                N += IEVAE_Dataset.read_from_file(f_name, file_filter=None).num_shapes
        n_files = len(dataset_files)
        training_bias = (N - 1) / (batch_size - 1)
        print('Total N_shapes:', N)

        ### Load in Deterministic Autoencoder Model ###
        print('Loading AE', path_to_AE)
        AEM = Autoencoder_IEVAE_fixedDec.load(path_to_AE, mode='eval')

        ### Move models to gpu ###
        AEM = AEM.to(device)
        AEM.encoder.move_eye(device)
        self.to(device)

        assert not (only_rotate_y_axis and
                    only_rotate_z_axis), "Only rotate one dimension"

        # Freeze the autoencoder
        for param in AEM.parameters(): param.requires_grad = False
        assert AEM.latent_dimensionality == self.DD, "Latent dim mismatch!"

        ### Setup ###
        print(self.training_params)
        optimizer = optim.Adam(self.vae_params(),
                               lr=learning_rate,
                               weight_decay=WEIGHT_DECAY_CONST)

        ######################################
        ### Define the total loss function ###
        ######################################
        def loss_function(
                    model,
                    ### Input AE latent vector and predicted output ###
                    X,     # Deterministically encoded latent point cloud
                    X_hat, # Predicted deterministically encoded latent point cloud
                    ### Encoder and decoder distributions ###
                    q,    # Encoder distribution
                    p,    # Decoder distribution
                    ### Spectral loss arguments ###
                    true_lbo,      # True LBO spectrum
                    lbo_hat,       # Predicted LBO spectrum
                    muset
                ):
            BD, AELD = X.shape
            LBOSD = lbo_hat.shape[1]
            weyl_weights = torch.arange(1, LBOSD + 1).type(torch.FloatTensor).to(device)

            #### Information-theoretic hierarchically factorized disentanglement ELBO ####
            #----------------------------------------------------------------------------#
            def likelihood_loss(X_hat, X):
                relative_quat_weight = RELATIVE_QUAT_WEIGHT
                # Extract quaternion-valued portion
                # Note we actually compute the distance between their unitized forms
                # See Huynh, Metrics for 3D Rotations: Comparison and Analysis
                uq_x  = F.normalize(     X[:, 0:self.AEDIM_R], p=2, dim=1) # unit quat from x
                uq_rx = F.normalize( X_hat[:, 0:self.AEDIM_R], p=2, dim=1) # " " from recon_x
                # Distance between rotations, only pseudo-metric between quaternions
                # But still a metric on SO(3)
                qerr = (1 - torch.bmm(  uq_x.view(BD, 1, self.AEDIM_R),
                                       uq_rx.view(BD, self.AEDIM_R, 1)  ).abs() ) #.mean(dim=-1)
                # Non-rotational reconstruction error
                euc_err = (X_hat[:, self.AEDIM_R:] - X[:, self.AEDIM_R:]).pow(2).mean(dim=-1)
                # Combined log-likelihood
                return ( (euc_err + relative_quat_weight*qerr) * W_LL ).unsqueeze(0)
            # Assign the loss to the decoder distribution
            # The MSE loss corresponds to a Gaussian continuous output under log-likelihood
            p.loss(likelihood_loss, X_hat, X, name='x')
            # Hierarchically factorized ELBO function
            hfelbo = lambda q, p: elbo(q, p, sample_dim=0, batch_dim=1, alpha=0.0, beta=BETA,
                                       bias=training_bias, size_average=True, reduce=True)
            L_hfvae, indiv_terms = hfelbo(q, p) # Minimize the negative elbo as a loss
            L_hfvae = -L_hfvae

            #### Covariance penalty for disentanglement ####
            #----------------------------------------------#
            # Extract the means from the z group distirbutions
            # DIP-VAE-I covariance penalty
            mu_R = q['z_R'].dist.mean # Same as loc
            mu_E = q['z_E'].dist.mean
            mu_I = q['z_I'].dist.mean
            mu = torch.cat( (mu_R, mu_E, mu_I), dim=-1 ).squeeze(0) # Remove mc n_samples dim
            # Estimate the covariance matrix of mu
            M_tilde = mu - mu.mean(dim=0) # B x L
            C_hat = (1 / (B - 1)) * M_tilde.t().mm(M_tilde)
            # For the intra-group penalties, get the intra-group covariance matrices
            C_hat_R = C_hat[0:self.b1, 0:self.b1]
            C_hat_E = C_hat[self.b1:self.b2, self.b1:self.b2]
            C_hat_I = C_hat[self.b2:, self.b2:]
            # For the inter-group penalties, get the inter-group covariance matrices
            C_hat_RE = C_hat[0:self.b1,       self.b1:self.b2] # cov(z_R, z_E)
            C_hat_EI = C_hat[self.b1:self.b2, self.b2:] # cov(z_E, z_I)
            C_hat_RI = C_hat[0:self.b1,       self.b2:] # cov(z_R, z_I)
            # Local methods for computing penalties via the L_1,1 norm
            def onDiagTerm(M):  return (M.diag() - 1).abs().sum()
            def offDiagTerm(M): return M.triu(diagonal=1).abs().sum()
            def interTerm(M):   return M.abs().sum()
            # Compute the on-diagonal intra-group penalty
            onDiagPen  = onDiagTerm(C_hat_R)  + onDiagTerm(C_hat_E)  + onDiagTerm(C_hat_I)
            # Compute the off-diagonal intra-group penalty
            offDiagPen = offDiagTerm(C_hat_R) + offDiagTerm(C_hat_E) + offDiagTerm(C_hat_I)
            # Compute the inter-group penalty
            interPen   = interTerm(C_hat_RI)  + interTerm(C_hat_EI)  + interTerm(C_hat_RE)
            # Final weighted penalty
            L_covarpen = GAMMA*interPen + LAMBDA_OFF*offDiagPen + LAMBDA_DIA*onDiagPen

            #### Spectral loss ####
            #---------------------#
            # Using weyl weights has the benefit of not over-emphasizing
            # higher frequency shape details, which might be more likely
            # to include pose information (as the deformations are only
            # nearly isometric, not exactly so).
            L_spectral = ( (true_lbo - lbo_hat).abs() / weyl_weights ).mean()

            #### Jacobian loss ####
            # P -> X -> mu -> z -> X_hat -> mu_hat
            # Compute Jacobians: dmu_hat_E/d_mu_I and dmu_hat_I/d_mu_E

            #--- Define a function to compute the Jacobian ---#
            def jacobian(inputs, output):
                assert inputs.requires_grad
                B, n = output.shape
                J = torch.zeros(n, *inputs.size()).to(device) # n x B x m
                grad_output = torch.zeros(*output.size()).to(device) # B x n
                for i in range(n):
                    zero_gradients(inputs)
                    grad_output.zero_()
                    grad_output[:, i] = 1 # Across the batch, for one output
                    output.backward(grad_output, create_graph=True)
                    J[i] = inputs.grad
                # Cleanup
                zero_gradients(inputs)
                grad_output.zero_()
                return torch.transpose(J, dim0=0, dim1=1) # B x n x m
            #--- End Jacobian calculation function ---#

            # Grab the variables wrt which we will compute the gradient
            # Set retains_grad to true so that it actually saves the gradient
            # Note that it is different than requires_grad, which was on
            # As opposed to doing nothing, returning None, and giving me an error
            # with no helpful messages whatsoever -_-'
            # Note that retain_grad() had to be called earlier (in the encode function)
            #mu_E = mu_E.squeeze(0) # q['z_E'].value.squeeze(0)  #mu_E.squeeze(0)
            #mu_E.retain_grad()
            #mu_I = mu_I.squeeze(0) # q['z_I'].value.squeeze(0)
            #mu_I.retain_grad()

            mu_E = muset[1]
            mu_I = muset[2]

            # This is the funtion passed as an argument to the differentiation operator
            mu_2nd = model.encode(X_hat, return_mu_only = True) # This is mu_hat
            mu_hat_E = mu_2nd[:, self.b1 : self.b2]
            mu_hat_I = mu_2nd[:, self.b2 :        ]

            # We need to keep the graph around since we will use it in the loss function
            # -- Jacobian dhatzE/dzI -- #
            jacobian_E_by_I = jacobian(inputs = mu_I, output = mu_hat_E)
            # -- Jacobian dhatzI/dzE -- #
            jacobian_I_by_E = jacobian(inputs = mu_E, output = mu_hat_I)
            # Compute the squared Frobenius norms of the Jacobians
            jacobian_dEdI = jacobian_E_by_I.pow(2).mean(dim=0).sum()
            jacobian_dIdE = jacobian_I_by_E.pow(2).mean(dim=0).sum()
            # Final penalty 
            JacobianPenalty = torch.max( jacobian_dEdI, jacobian_dIdE )

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
            #--- Finally compute the total loss sum ---#
            if self.use_jacobian:
                total_loss = (   L_hfvae
                           + L_covarpen
                           + L_spectral * ZETA_lbo
                           + JacobianPenalty * Jacobian_coef  )
            else:
                total_loss = (   L_hfvae
                           + L_covarpen
                           + L_spectral * ZETA_lbo )

            #### Indiv Loss terms ####
            #------------------------#
            term_set = [
                    # HFVAE terms from the KL divergence
                    # ELBO_TERMS =
                    #     log_likelihood:  (1) + (3),
                    #     intra-group TCs: (i),
                    #     dimwise KL-div:  (ii),
                    #     MI(x, z):        (2),
                    #     TC(z):           (A),
                    #     T_5: semi-sup term 1
                    #     T_6: semi-sup term 2
                    indiv_terms[0], indiv_terms[1], indiv_terms[2], indiv_terms[3], indiv_terms[4],
                    onDiagPen, offDiagPen, interPen, # Covariance penalty terms
                    L_spectral, # Spectral loss term
                    jacobian_dEdI, jacobian_dIdE
            ]
            term_names = [
                'log-likelihood', 'Intra-TC', 'DimWise-KL', 'MI(x,z)', 'Inter-TC',
                'ON-DIAG', 'OFF-DIAG', 'INTER-COVAR',
                'LBO-ERR',
                'EwrtI_jacobian', 'IwrtE_jacobian'
            ]

            return total_loss, term_set, term_names
        #</ End of loss function routine />#

        ### Single Batch Processing Function ###
        # P -> X -> Z -> X_hat -> P_hat
        def process_batch(dataset_object):
            Ps, dataset_indices, R = dataset_object.sample_batch(
                                    batch_size,
                                    input_shape_sample_size,
                                    rotate=rotate_training_data,
                                    max_rotation_angle=max_rotation_angle,
                                    y_axis_only=only_rotate_y_axis,
                                    z_axis_only=only_rotate_z_axis)
            # Encode the point clouds with the deterministic autoencoder
            CPT = lambda qw: Variable(torch.FloatTensor(qw), requires_grad=False).to(device)
            X = AEM.encode(CPT(Ps))
            # Run this VAE on the vector outputs
            # Outputs: p = decoder dist, q = encoder dist, X_hat = decoded latent AE vec
            q, p, X_hat, muset = self.forward(X, mu_only=False, return_mu_with_q=True)
            # Spectral Data and prediction
            true_lbo = CPT( dataset_object.gather_lbo_spectra( dataset_indices ) )
            lbo_hat = self.lbo_predictor.forward( q['z_I'].value.squeeze(0) )
            ### Compute loss ###
            loss_func_output = loss_function(
                                self,
                                ### Input AE latent vector and predicted output ###
                                X,     # Deterministically encoded latent point cloud
                                X_hat, # Predicted deterministically encoded latent point cloud
                                ### Encoder and decoder distributions ###
                                q,    # Encoder distribution
                                p,    # Decoder distribution
                                ### Spectral loss arguments ###
                                true_lbo, # True LBO spectrum
                                lbo_hat,   # Predicted LBO spectrum
                                muset
            )
            loss_total, sublosses, subloss_names = loss_func_output
            optimizer.zero_grad()
            loss_total.backward()
            optimizer.step()
            # Return (1) the total loss, (2) the subloss terms, (3) the names of the sublosses
            return loss_total, sublosses, subloss_names
        #</ End of single batch processing routine />#

        #-------------------------#
        ### Training Iterations ###
        #-------------------------#
        self.train()

        record_losses = True
        self.EPOCHAL_LOSS_VALUES = []
        self.EPOCHAL_LOSS_NAMES = None
        assert len(dataset_files) == 1, "Current record-keeping assumes num_files == 1"

        tot_count = 0 # Number of processed batches
        to_pt = lambda rt: torch.FloatTensor( rt ).to(device)
        for i in range(num_epochs):
            __current_record_losses = []
            epoch_start_time = time.time()
            print('-' * 30, '\nEpoch', i, '\n' + '-' * 30)
            for fi, file in enumerate(dataset_files):
                print('Loading file %d:' % fi, file)
                if preload_files:
                    dataset_object = preloaded_files[file]
                else:
                    dataset_object = IEVAE_Dataset.read_from_file(
                                            file, file_filter=None)
                cov_nb = dataset_object.num_samples_to_cover_set(batch_size)
                n_batches = dataset_coverings * cov_nb
                print('\tObtained', dataset_object.num_shapes,
                      'samples. Using NB =', n_batches)
                list_of_times = []

                ### Loop over batches ###
                for j in range(n_batches):
                    batch_start_time = time.time()

                    ### Generate and run through a single batch for the rest of the hfievae ###
                    #>> Process the batch for the main VAE learner <<#
                    loss, sublosses, subloss_names = process_batch(dataset_object)
                    # Record the time taken
                    list_of_times.append( time.time() - batch_start_time )
                    tot_count += 1
                    # Record subloss vals
                    if record_losses:
                        curr_sublosses = np.array([float(val) for val in sublosses])
                        __current_record_losses.append( curr_sublosses )
                    # Record epochal average if needed
                    if record_losses and j == n_batches - 1:
                        avg_epoch_losses = np.mean( __current_record_losses, axis=0 )
                        self.EPOCHAL_LOSS_VALUES.append( avg_epoch_losses )
                        self.EPOCHAL_LOSS_NAMES = subloss_names
                        print( 'Current Epochal loss set =',
                                list(zip(subloss_names, avg_epoch_losses)) )
                    # Print progress so far
                    if tot_count % print_every == 0:
                        # Unpack the KL loss terms
                        L_vals = sublosses # + ([ahd_loss] if apply_ahd else [])
                        errNames = subloss_names # + (['AHD_enemy_loss'] if apply_ahd else [])
                        # Print
                        partial_pos, n_splits = len(errNames) // 3, 2
                        split_names = [ errNames[i] for i in [partial_pos, partial_pos*2] ]
                        print("C%d.E%d.B%d.F%d. <Loss: %f>." %
                              (tot_count, i, j, fi, loss), "(%.2fs/b)" % np.mean(list_of_times))
                        print( " ".join([ e + ' = ' + '%.3f,' % s + ('\n  ' if e in split_names else '')
                                           for e, s in zip(errNames, L_vals) ]) )
                    # Save the model, if needed
                    if tot_count % save_every == 0:
                        print('Saving to', path_to_save_model)
                        self.save(path_to_save_model)

            if (i % 100 == 0) and False:
                sss = 'e' + str(i) + '.pt'
                curr_save_path = path_to_save_model.replace(".pt", sss)
                print('Epochal Save (%d) to' % i, curr_save_path)
                self.save(curr_save_path)

    ### Model Modes ###

    def to_eval_mode(self):
        self.eval()
        return self

    def to_train_mode(self):
        self.train()
        return self

    ### Saving and Loading Methods ###

    def save(self, path_to_save):
        torch.save(self, path_to_save)

    def load(path_to_load, gpu_to_cpu=True, mode='eval'): # Static
        assert mode in ['eval', 'train'], 'Mode must be eval or train'
        mc = lambda m: m.to_eval_mode() if mode == 'eval' else m.to_train_mode()
        if gpu_to_cpu:
            return mc(torch.load(path_to_load,
                      map_location=lambda storage, location: storage))
        return mc(torch.load( path_to_load ))


    ### Misc helpers ###
    def print_dims(self):
        for w, v in [
                ("Latent dim", self.LD),
                ("Data dim", self.DD),
                ("Data Rotation dim", self.AEDIM_R),
                ("ENC sizes", self.enc_sizes),
                ("ENC_MU sizes", self.mu_sizes),
                ("ENC_SIGMA sizes", self.sigma_sizes),
                ("DEC sizes", self.dec_sizes),
                ("ENC_ROT sizes", self.enc_rotation_sizes),
                ("DEC_ROT sizes", self.dec_rotation_sizes),
#                ("I -> E sizes", self.intToExt_sizes),
#                ("E -> I sizes", self.extToInt_sizes),
                ("|z_R|", self.rotation_dim),
                ("|z_E|", self.extrinsic_dim),
                ("|z_I|", self.intrinsic_dim)
            ]:
            print(str(w) + ":", v)

def pex(s,err=1):
    print(s)
    sys.exit(err)





#
