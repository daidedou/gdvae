from typing import Iterable, Type
import numpy as np, os, sys, pickle, math, json
import numpy.random as npr
from sklearn.neighbors import KDTree
from ...structures.TriangleMesh import TriangleMesh, MinimalTriangleMesh, FullTriangleMesh
from ...utils.TransformUtils import random3DRotationMatrix_unitary
from ...viewing.DataPlotting import plotMultiArrays
from ...structures.PointCloud import PointCloud

class IEVAE_Datum(object):

    def __init__(self, mesh_input               : FullTriangleMesh = None,
                       chosen_points            : list  = None,
                       num_sampled_points       : int   = 2500,
                       precomputed_lbo_spec     : list  = None,
                       lbo_spectrum_size        : int   = None,
                       rel_dirac_spectrum_size  : int   = None,
                       precomputed_relDirac     : list  = None,
                       center                   : bool  = True,
                       extra_data               : dict  = None,
                       verbose                  : bool  = True,
                       marq_lev_coef            : float = 0.0,
                       node_areas_pert          : float = 0.0,
                       allow_mat_perturbations  : bool  = False,
                       lbo_spec_tol             : float = 0,
                       lbo_spec_max_iters       : int   = None
        ) -> None:

        # Vertex positions and associated surface normals
        if not chosen_points is None:
            self.points = chosen_points
            self.normals = None
        else:
            self.points, self.normals = mesh_input.generateAreaWeightedRandomPointCloudWithNormals(
                                                    num_sampled_points,
                                                    verbose=verbose)
        # Centering the point cloud
        if center:
            # First, mean-center the point set at the origin
            # Note this somewhat implicitly assumes uniform surface sampling
            mean_val = np.mean(self.points, axis=0)
            self.points = self.points - mean_val
            # We don't do scaling
            # Note that this could have some undesirable effects if we are not careful
            # E.g. consider a standing person and an ~isometric transformation to crouching
            #      Then this scaling will remove the isometric connection between them

        # Intrinsic and Extrinsic Spectra
        if   not precomputed_lbo_spec is None:
            self.LBO_spectrum = precomputed_lbo_spec
        elif not lbo_spectrum_size is None:
            self.LBO_spectrum, _ = mesh_input.computeCotanLboSpectrum(
                                    spectrum_size=lbo_spectrum_size,
                                    marq_lev_coef=marq_lev_coef,
                                    node_areas_pert=node_areas_pert,
                                    allow_mat_perturbations=allow_mat_perturbations,
                                    max_iters=lbo_spec_max_iters,
                                    tol=lbo_spec_tol,
                                    verbose=verbose)
        if not precomputed_relDirac is None:
            if verbose: print('Using precomputed relative Dirac spectrum')
            self.rdirac_spectrum = precomputed_relDirac

        # View LBO/Dirac spectrum plotted
        # plotMultiArrays([self.LBO_spectrum, self.rdirac_spectrum], form_list=['ro','g^'])

        # Metadata
        self.num_points = len(self.points)
        #self.name = mesh_input.name
        self.extra_data = extra_data # Labels, categories, etc...

    def sample_points(self, npoints):
        '''
        These points serve as the input to the encoder, and are a (sampled) subset
        of the sampled points from the mesh
        '''
        inds = npr.choice(self.num_points, size=npoints, replace=False)
        # uniform discrete sample, no replacement
        return self.points[inds]


class IEVAE_Dataset(object):

    def apply_artificial_spectral_noise(self,
                                        noise_type,
                                        interp_noise_alpha,
                                        mgaussian_noise_sigma,
                                        correct_monotone):
        # Only applied once, not as data augmentation
        if not noise_type is None:
            print('WARNING: ADDING ARTIFICIAL SPECTRAL NOISE')
            assert noise_type in ['interp', 'mgaussian', 'zeroify'], "Unrecognized noise type"
            # Original LBOs
            all_lbos = np.array([ datum.LBO_spectrum
                                  for datum in self.data ])
            N = len(all_lbos)
            slen = len(all_lbos[0])
            #-- Zeroing Spectra and replacing with Gaussian noise --#
            if noise_type == 'zeroify':
                print('WARNING: ZEROING LBO SPECTRA & REPLACING WITH GAUSSIAN NOISE')
                # Zeros out spectrum, and replace with Gaussian noise
                _SIGMA_replacement = 50
                new_lbos = all_lbos * 0.0 + npr.randn(N,slen) * _SIGMA_replacement
                prrr = [7, 9, 13]
                for i, datum in enumerate(self.data):
                    if i == len(self.data) // 2:
                        print('\t50% Complete')
                    if i in prrr:
                        print(i,'Old',self.data[i].LBO_spectrum[0:25])
                    datum.LBO_spectrum = new_lbos[i,:]
                    if i in prrr:
                        print(i,'New',self.data[i].LBO_spectrum[0:25])
            #-- Multiplicative Gaussian noise --#
            if noise_type == 'mgaussian':
                print('WARNING: USING MG NOISE (sigma = %.3f)' % mgaussian_noise_sigma)
                if correct_monotone: print('WARNING: USING MONOTONE CORRECTION')
                assert interp_noise_alpha is None
                mg_noise = (mgaussian_noise_sigma * npr.randn(N,slen) + 1).clip(min=0)
                new_lbos = all_lbos * mg_noise
                prrr = [7, 9, 13]
                for i, datum in enumerate(self.data):
                    if i == len(self.data) // 2:
                        print('\t50% Complete')
                    if i in prrr:
                        print(i,'Old',self.data[i].LBO_spectrum[0:25])
                    datum.LBO_spectrum = new_lbos[i,:]
                    if correct_monotone:
                        # Don't do this VV. The values (noise) are no longer independent then!
                        # datum.LBO_spectrum = np.maximum.accumulate(datum.LBO_spectrum)
                        # Instead, we resort the eigenvalues.
                        datum.LBO_spectrum = np.sort( datum.LBO_spectrum )
                    if i in prrr:
                        print(i,'Noise',mg_noise[i])
                        print(i,'New',self.data[i].LBO_spectrum[0:25])
            #-- Interpolation noise --#
            if noise_type == 'interp':
                #
                if correct_monotone: assert False # don't come here
                #
                assert (type(interp_noise_alpha) is float and
                        interp_noise_alpha <= 1.00001 and
                        interp_noise_alpha >= 0.0), "Untenable noise level"
                print('WARNING: USING INTERP NOISE (alpha = %.3f)' % interp_noise_alpha)
                if correct_monotone: print('WARNING: USING MONOTONE CORRECTION')
                # Scrambled LBOs
                lbos_scrambled = np.copy(all_lbos)
                np.random.shuffle(lbos_scrambled)
                print('\tall_lbos.size:', all_lbos.shape)
                print('\tFirst few values')
                targg = 9
                print('\torig:', all_lbos[targg,0:15])
                print('\tscrambled', lbos_scrambled[targg,0:15])
                # Interpolations
                alphas = np.random.uniform(low=1e-7,
                                           high=interp_noise_alpha,
                                           size=(N,1))
                print('\ttargg alpha', alphas[targg])
                interped_lbos = (1 - alphas) * all_lbos + alphas * lbos_scrambled
                print('\tinterps', interped_lbos[targg,0:15])
                # Overwrite
                print('Overwriting LBOs')
                for i, datum in enumerate(self.data):
                    if i == len(self.data) // 2:
                        print('\t50% Complete')
                    datum.LBO_spectrum = interped_lbos[i,:]
                    if correct_monotone:
                        datum.LBO_spectrum = np.maximum.accumulate(datum.LBO_spectrum)
                print('\tCorrected', self.data[targg].LBO_spectrum[0:15])

    def __init__(self,
                 data_samples : Iterable[IEVAE_Datum],
                 noise_type = None,
                 interp_noise_alpha = None
                 ) -> None:
        # Data fields
        self.data = data_samples
        self.num_shapes = len(data_samples)
        # Fields for non-overlapping batch sampling
        # Unnorm probs is only ever ~0 or 1
        self.unnorm_probs = np.ones(self.num_shapes)
        self.unnorm_seen_prob = 1e-7
        # Artificial spectral noise
        self.apply_artificial_spectral_noise(noise_type, interp_noise_alpha)

    def compute_trees(self): self.trees = [ KDTree(dm.points) for dm in self.data ]

    def sample_batch(self, batch_size, shape_sample_size, reset_probs_if_full=True,
                     rotate=True, max_rotation_angle=None, z_axis_only=False,
                     y_axis_only=False, prechosen_indices=None):
        '''
        Prevent seeing the same targets more than once (or missing points).
        Batches are still assembled randomly.
        For the final batch, select uniformly randomly from the whole set if
        need to fill remaining spots.
        Note that max_rotation_angle has no effect unless z_axis_only is true.
        '''
        # Choose indices from this file, by uniform sampling (no replacement)
        # Note: as long as there remain unssen samples, the chance of choosing
        # a seen sample is very low. However, once all the samples are seen, it
        # chooses from the remainder uniformly randomly.
        cp = self.unnorm_probs / self.unnorm_probs.sum()
        if prechosen_indices is None:
            Y = npr.choice(self.num_shapes, size=batch_size, replace=False, p=cp)
        else:
            assert len(prechosen_indices) == batch_size, "Inconsistent specs"
            Y = prechosen_indices
        X = np.array([ (self.data[q]).sample_points(shape_sample_size)
                        for q in Y ])
        # Update the choice probabilities
        self.unnorm_probs[Y] = self.unnorm_seen_prob
        if reset_probs_if_full:
            if (self.unnorm_probs <= 2 * self.unnorm_seen_prob).all():
                self.reset_probs()
        # Perform rotation if desired
        if rotate:
            R = IEVAE_Dataset._randomly_rotate(X, max_rotation_angle,
                    z_axis_only, y_axis_only, batch_size)
            return X, Y, R
        # Return data with indices
        return X, Y

    def sample_batch_unifrand(self, batch_size, shape_sample_size,
                              rotate=True, max_rotation_angle=None,
                              z_axis_only=False, y_axis_only=False):
        '''
        Generate a batch simply by random sampling without replacement.
        Does not keep global track of what has been sampled.
        '''
        # Choose indices from the dataset
        Y = npr.choice(self.num_shapes, size=batch_size, replace=False)
        # uniform discrete sample, no replacement
        # Use samples from Y to get shapes from the dataset
        X = np.array([ (self.data[q]).sample_points(shape_sample_size) for q in Y ])
        if rotate:
            R = IEVAE_Dataset._randomly_rotate(X, max_rotation_angle,
                    z_axis_only, y_axis_only, batch_size)
            return X, Y, R
        # X = subsample of the random shapes in batch_size x shape_sample_size x 3
        # Y = indices of shapes in the dataset
        return X, Y

    def _randomly_rotate(X,
                         max_rotation_angle,
                         z_axis_only,
                         y_axis_only,
                         batch_size):

        R_all = np.zeros((batch_size, 3, 3))
        for i in range(batch_size):
            # Grab a point cloud
            pc = X[i, :, :]
            # Random rotation matrix
            if y_axis_only: # y_axis_only given precedence over z
                if max_rotation_angle is None:
                    max_rotation_angle = np.pi
                theta = npr.uniform(-max_rotation_angle, max_rotation_angle)
                c, s = np.cos(theta), np.sin(theta)
                R = np.array([
                    [ c,   0.0, s   ],
                    [ 0.0, 1.0, 0.0 ],
                    [ -s,  0.0, c   ]
                ])
            elif z_axis_only:
                if max_rotation_angle is None:
                    max_rotation_angle = np.pi
                theta = npr.uniform(-max_rotation_angle, max_rotation_angle)
                c, s = np.cos(theta), np.sin(theta)
                R = np.array([
                    [ c,   -s,  0.0 ],
                    [ s,   c,   0.0 ],
                    [ 0.0, 0.0, 1.0 ]
                ])
            else:
                R = random3DRotationMatrix_unitary() #None # TODO
            # Rotate current point cloud
            X[i, :, :] = pc.dot( R )
            R_all[i, :, :] = R
        return R_all

    def num_samples_to_cover_set(self, batch_size):
        return int(math.ceil(self.num_shapes / batch_size))

    def reset_probs(self):
        self.unnorm_probs = np.ones(self.num_shapes)

    def read_from_file(file_path : str,
                       file_filter = None,
                       compute_trees = True,
                       # Spectral Noise #
                       noise_type = None,
                       interp_noise_alpha = None,
                       mgaussian_noise_sigma = None,
                       monotone_correction = None,
                       ####
                       use_pc_lbos = False,
                       ####
                       pc_lbo_subsampling_max = 4000,
                       replacement_filepath = None
                       ) -> 'IEVAE_Dataset':
        with open(file_path, "rb") as target: dataset = pickle.load(target)
        if not file_filter is None: file_filter( dataset )
        if compute_trees: dataset.compute_trees() # avoid storing them

        # Replace the mesh spectra with PC spectra
        if use_pc_lbos:
            print('Using PC LBOs')
            if replacement_filepath is None: # Default
                pss = str(pc_lbo_subsampling_max) + "p"
                replacement_filepath = file_path + ".PCLBOs." + pss + ".json"
            print('PCLBO filepath:', replacement_filepath)
            #---------------------#
            # Case 1: PC LBOs exist
            #---------------------#
            if os.path.exists(replacement_filepath):
                print('\tFile already exists! Loading.')
                # Read in PC LBOs
                with open(replacement_filepath, 'r') as fp:
                    new_lbos = json.load(fp)
                fail_cases = new_lbos['fail_cases']
            #-----------------------------------#
            # Case 2: PC LBOs need to be computed
            #-----------------------------------#
            else:
                print('Generating new PCLBOs')
                # Compute PCLBOs for each PC
                N_lambda = len( dataset.data[0].LBO_spectrum )
                N_shapes = len( dataset.data )
                N_points = len( dataset.data[0].points )
                print('len_lambda = %d, N_shapes = %d, NP_per_shape = %d' %
                        (N_lambda, N_shapes, N_points) )
                if N_points > pc_lbo_subsampling_max:
                    print('Subsampling shapes to', pc_lbo_subsampling_max,
                            'for lbo calculations')
                fail_cases = []
                new_lbos = {}
                for i, datum in enumerate(dataset.data):
                    if N_points > pc_lbo_subsampling_max:
                        indices = np.random.choice(N_points, #datum.points,
                                                  pc_lbo_subsampling_max,
                                                  replace=False)
                        points = datum.points[indices]
                    else:
                        points = datum.points
                    curr_pc = PointCloud(points)
                    try:
                        print('Computing spectrum', i, '/', N_shapes)
                        curr_lbo = curr_pc.compute_lbo_spectrum(N_lambda,
                                                     use_pseudo_areas=True, # in generalized eigenproblem
                                                     lap_type='hk',
                                                     normalize_L=False,
                                                     hk_sigma_bandwidth='default')
                    except:
                        print('PCLBO computation at index', i, 'failed!')
                        fail_cases.append(i)
                        continue
                    # Store into dict
                    new_lbos[i] = curr_lbo[0].tolist()
                # Done computing LBOs. Store fail cases too
                new_lbos['fail_cases'] = fail_cases
                # Save outputs
                with open(replacement_filepath, "w") as rfp:
                    json.dump(new_lbos, rfp, indent=3)
                print('\tWrote JSON output file', replacement_filepath)
            #-------#
            # Note will fail after running to generate pclbo json
            # Need to rerun afterwards
            # Assign LBOs to targets (remove targets with no LBO)
            print('Assigning new LBO spectra')
            print(len(fail_cases), 'fail cases present:', fail_cases)
            #print('keys', new_lbos.keys() )
            for i, datum in enumerate(dataset.data):
                si = str(i) # All JSON keys are strings, even if saved as ints
                if i == len(dataset.data) // 2: print('50% Done')
                if not i in fail_cases:
                    datum.LBO_spectrum = np.array(new_lbos[ si ])
                else: # consistency check (i is in fail_cases)
                    if not (new_lbos.get(si, None) is None):
                        print('Inconsistency: key', i, 'failed but is valid.')
            # </ Finished reassigning spectra />
        # Add noise to the spectra
        if not noise_type is None:
            print('Warning: extra spectral noise injection running')
            dataset.apply_artificial_spectral_noise(noise_type,
                    interp_noise_alpha=interp_noise_alpha,
                    mgaussian_noise_sigma=mgaussian_noise_sigma,
                    correct_monotone=monotone_correction)
        #~~~~#
        ### temporary, for backwards compatibility
        dataset.unnorm_probs = np.ones(dataset.num_shapes)
        dataset.unnorm_seen_prob = 1e-7
        ###

        return dataset

    def save_to_file(self, file_path : str):
        self.trees = None # Don't store kd-trees by default
        with open(file_path, "wb") as target: dataset = pickle.dump(self, target)

    def sample_points_from_target_shapes(self, indices, num_points):
        return np.array([self.data[i].sample_points(num_points) for i in indices])

    def retrieve_spectra_from_targets(self, indices):
        '''
        Input: a list of dataset indices.
        returns (dirac_spectra, lbo_spectra) as a tuple pair of matrices
        i.e. if len(indices) = B, D = len per dirac spectra, L = num lbo evals,
        then we return a matrix tuple with sizes (B x D, B x L).
        '''
        DS = np.array([self.data[i].rdirac_spectrum for i in indices])
        LS = np.array([self.data[i].LBO_spectrum    for i in indices ])
        return DS, LS

    def get_closest_points_with_normals(self, indices, input_points):
        '''
        indices = which shapes to which each point set belongs.
        Assuming input_points in batch_size x n_samples_per_shape x 3.
        This means the output points has to have the same shape.
        '''
        # Outputs
        B, NS, d = input_points.shape
        true_points = np.zeros((B, NS, d))
        true_normals = np.zeros((B, NS, d))
        # Get the indices of the closest points, for each shape
        for i, shape_index in enumerate(indices):
            tree = self.trees[shape_index]
            _, inds = tree.query(input_points[i,:,:], k=1)
            inds = inds.flatten() # Since k = 1, each output is a list of len 1
            true_points[i,:,:] = self.data[shape_index].points[inds,:]
            true_normals[i,:,:] = self.data[shape_index].normals[inds,:]
        return true_points, true_normals


    def get_closest_points(self, indices, input_points):
        '''
        indices = which shapes to which each point set belongs.
        Assuming input_points in batch_size x n_samples_per_shape x 3.
        This means the output points has to have the same shape.
        '''
        # Outputs
        B, NS, d = input_points.shape
        true_points = np.zeros((B, NS, d))
        # Get the indices of the closest points, for each shape
        for i, shape_index in enumerate(indices):
            tree = self.trees[shape_index]
            _, inds = tree.query(input_points[i,:,:], k=1)
            inds = inds.flatten() # Since k = 1, each output is a list of len 1
            true_points[i,:,:] = self.data[shape_index].points[inds,:]
        return true_points

    def gather_lbo_spectra(self, indices):
        return np.array([self.data[i].LBO_spectrum for i in indices])

    def gather_dirac_spectra(self, indices):
        return np.array([self.data[i].rdirac_spectrum for i in indices])

    def random_split_dataset(self, size1, size2, name1, name2):
        assert self.num_shapes == len(self.data)
        assert self.num_shapes == (size1 + size2)
        assert not os.path.isfile(name1)
        assert not os.path.isfile(name2)
        print('Generating random split (orig size: %d)' % self.num_shapes)

        # In-place shuffle. Note that we do not resave self
        # so this does not affect the original dataset.
        from random import shuffle
        shuffle(self.data)
        shuffled_data = self.data

        data_1 = shuffled_data[0 : size1]
        data_2 = shuffled_data[size1 : ]
        assert len(data_1) == size1 and len(data_2) == size2

        set1 = IEVAE_Dataset(data_1)
        set2 = IEVAE_Dataset(data_2)

        set1.save_to_file(name1)
        print('Saved DS1 to', name1)
        set2.save_to_file(name2)
        print('Saved DS2 to', name2)
