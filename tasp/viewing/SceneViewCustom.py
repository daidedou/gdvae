###############################################################
# Based on the trimesh library's viewing code
###############################################################

import pyglet, os, sys
import pyglet.gl as gl
import numpy as np
import platform
from collections import deque
from io import BytesIO, StringIO
from .Color import Color

#from .. import util
#from trimesh import util
from .Transformations import Arcball

# smooth only when fewer faces than this
_SMOOTH_MAX_FACES = 100000
basestring = str

class SceneViewer(pyglet.window.Window):

    def __init__(self,
                 scene,
                 smooth=True,
                 save_image=None,
                 flags=None,
                 resolution=(640, 480),
                 extraTrajs=[],
                 tasp_mesh=None,
                 marked_points=None,
                 marked_point_colors=None, # list of colors (i.e. 3d vectors), or a list with one color
                 marked_point_size=None,
                 normals_length=None,
                 cols=[],
                 additional_meshes=None,
                 on_retina_screen=False,
                 verbose=True
                 ):

        cont = gl.current_context
        #print('Context:', cont)

        self.scene = scene
        self.scene._redraw = self._redraw
        self.tasp_mesh = tasp_mesh
        self.plot_normals = False
        self.extra_trajs = extraTrajs
        self.cols = cols
        self.on_retina_screen = on_retina_screen
        #print('FLAGS', flags)
        self.reset_view(flags=flags)
        self.batch = pyglet.graphics.Batch()

        #########################################################################################################
        self.marked_points = None if (marked_points is None) else []

        if (not self.marked_points is None) and (not tasp_mesh is None):
            for i in range(len(marked_points)):
                #if type(self.marked_points[i]) is int or type(self.marked_points[i]) is np.int32:
                self.marked_points.append( self.tasp_mesh.vertices[ marked_points[i] ] )
            self.marked_points = np.array(self.marked_points)

        if not self.marked_points is None:
            default_marked_point_size = 10
            if marked_point_size is None: self.marked_point_size = default_marked_point_size
            else: self.marked_point_size = marked_point_size

        if not self.marked_points is None:
            default_color = [0.0, 0.0, 0.0]
            if marked_point_colors is None:
                self.marked_point_colors = [default_color for _ in range(len(self.marked_points))]
            else:
                if type(marked_point_colors) is np.ndarray:
                    if marked_point_colors.shape[0] == 3:
                        self.marked_point_colors = [marked_point_colors for _ in range(len(self.marked_points))]
                elif type(marked_point_colors) is str:
                    # Assume it is type Color, if the input is a single string
                    c = Color.from_name(marked_point_colors).as_normalized_rgb()
                    self.marked_point_colors = [c for _ in range(len(self.marked_points))]
                elif type(marked_point_colors) is list and type(marked_point_colors[0]) is str:
                    assert len(marked_point_colors) == len(self.marked_points), "Marked points colors insufficient"
                    self.marked_point_colors = [Color.from_name(c).as_normalized_rgb() for c in range(len(self.marked_points))]
                elif len(marked_point_colors) == 1:
                    self.marked_point_colors = [marked_point_colors for _ in range(len(self.marked_points))]   
                else:
                    assert len(marked_point_colors) == len(self.marked_points), "Marked points colors insufficient"
                    self.marked_point_colors = marked_point_colors

        if not normals_length is None:
            self.normals_length = normals_length
        else:
            self.normals_length = 1.0
        #########################################################################################################

        #if not ('camera' in scene.graph.nodes):
        #    print('Camera')
        scene.set_camera()


        visible = (save_image is None) or (platform.system() != 'Linux')
        width, height = resolution

        # if not save_image is None: visible = False

        try:
            conf = gl.Config(sample_buffers=1,
                             samples=4,
                             depth_size=16,
                             double_buffer=True)
            super(SceneViewer, self).__init__(config=conf,
                                              visible=visible,
                                              resizable=True,
                                              width=width,
                                              height=height)
        except pyglet.window.NoSuchConfigException:
            conf = gl.Config(double_buffer=True)
            super(SceneViewer, self).__init__(config=conf,
                                              resizable=True,
                                              visible=visible,
                                              width=width,
                                              height=height)
        
        self._img = save_image
        self._smooth = smooth

        self.vertex_list = {}
        self.vertex_list_md5 = {}
        self.vertex_list_mode = {}

        for name, mesh in scene.geometry.items():
            # print('nn', name)
            self.add_geometry(name=name, geometry=mesh)

        #########################################################
        # if not additional_meshes is None:
        #     for mesh in additional_meshes:
        #         print('nn', name)
        #         self.add_geometry(name=mesh.name, geometry=mesh)
        #########################################################

        # print('nkeys',len(self.vertex_list.keys()))
        # sys.exit(0)

        self.init_gl()
        self.set_size(*resolution)
        self.update_flags()
        try:
            pyglet.app.run()
        except RuntimeError as e:
            if verbose:
                # TODO save_image ALWAYS generates an error
                # Namely "dictionary changed size"
                print('RuntimeError incurred in SceneViewCustom')
                print(e)
            pyglet.app.exit()

    def _redraw(self):
        self.on_draw()

    def _update_meshes(self):
        for name, mesh in self.scene.geometry.items():
            if self.vertex_list_md5[name] != geometry_md5(mesh):
                self.add_geometry(name, mesh)

    def _add_mesh(self, name, mesh):
        if self._smooth and len(mesh.faces) < _SMOOTH_MAX_FACES:
            display = mesh.smoothed()
        else:
            display = mesh.copy()
            display.unmerge_vertices()
        self.vertex_list[name] = self.batch.add_indexed(
            *mesh_to_vertex_list(display))
        self.vertex_list_md5[name] = geometry_md5(mesh)
        self.vertex_list_mode[name] = gl.GL_TRIANGLES

    def _add_path(self, name, path):
        self.vertex_list[name] = self.batch.add_indexed(
            *path_to_vertex_list(path))
        self.vertex_list_md5[name] = geometry_md5(path)
        self.vertex_list_mode[name] = gl.GL_LINES

    def _add_points(self, name, pointcloud):
        self.vertex_list[name] = self.batch.add_indexed(*points_to_vertex_list( pointcloud.vertices,
                                                                                pointcloud.colors ))
#                                                                               pointcloud.vertices_color))
        self.vertex_list_md5[name] = geometry_md5(pointcloud)
        self.vertex_list_mode[name] = gl.GL_POINTS

    def add_geometry(self, name, geometry):
        def is_instance_named(obj, name):
            """
            Given an object, if it is a member of the class 'name',
            or a subclass of 'name', return True.
            Parameters
            ---------
            obj: instance of a class
            name: string
            Returns
            ---------
            bool, whether the object is a member of the named class
            """
            try:
                type_named(obj, name)
                return True
            except ValueError:
                return False
        if is_instance_named(geometry, 'Trimesh'):
            return self._add_mesh(name, geometry)
        elif is_instance_named(geometry, 'Path3D'):
            return self._add_path(name, geometry)
        elif is_instance_named(geometry, 'Path2D'):
            return self._add_path(name, geometry.to_3D())
        elif is_instance_named(geometry, 'PointCloud'):
            return self._add_points(name, geometry)
        else:
            raise ValueError('Geometry passed is not a viewable type!')

    def reset_view(self, flags=None):
        '''
        Set view to base.
        '''
        self.view = {'wireframe': False,
                     'cull': False, #True,
                     'translation': np.zeros(3),
                     'center': self.scene.centroid,
                     'scale': self.scene.scale,
                     'ball': Arcball()}
        if isinstance(flags, dict):
            for k, v in flags.items():
                if k in self.view:
                    self.view[k] = v
            self.update_flags()

    def init_gl(self):
        gl.glClearColor(1, 1, 1, 0)

        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_CULL_FACE)
        gl.glEnable(gl.GL_LIGHTING)
        gl.glEnable(gl.GL_LIGHT0)
        gl.glEnable(gl.GL_LIGHT1)
        gl.glEnable(gl.GL_LIGHT2)

        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, _gl_vector(.5, .5, 1, 0))
#        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, _gl_vector(.5, .5, -1, 0))
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, _gl_vector(.5, .5, 1, 1))
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, _gl_vector(1, 1, 1, 1))

        gl.glLightfv(gl.GL_LIGHT1, gl.GL_POSITION, _gl_vector(1, 0, .5, 0))
        gl.glLightfv(gl.GL_LIGHT1, gl.GL_DIFFUSE, _gl_vector(.5, .5, .5, 1))
        gl.glLightfv(gl.GL_LIGHT1, gl.GL_SPECULAR, _gl_vector(1, 1, 1, 1))

        gl.glLightfv(gl.GL_LIGHT2, gl.GL_POSITION, _gl_vector(.5, .5, -1, 0))
#        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, _gl_vector(.5, .5, -1, 0))
        gl.glLightfv(gl.GL_LIGHT2, gl.GL_SPECULAR, _gl_vector(.5, .5, 1, 1))
        gl.glLightfv(gl.GL_LIGHT2, gl.GL_DIFFUSE, _gl_vector(1, 1, 1, 1))

        gl.glColorMaterial(gl.GL_FRONT_AND_BACK, gl.GL_AMBIENT_AND_DIFFUSE)
        gl.glEnable(gl.GL_COLOR_MATERIAL)
        gl.glShadeModel(gl.GL_SMOOTH)

        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_AMBIENT,
                        _gl_vector(0.192250, 0.192250, 0.192250))
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_DIFFUSE,
                        _gl_vector(0.507540, 0.507540, 0.507540))
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_SPECULAR,
                        _gl_vector(.5082730, .5082730, .5082730))

        gl.glMaterialf(gl.GL_FRONT_AND_BACK, gl.GL_SHININESS, .4 * 128.0)

        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        gl.glEnable(gl.GL_LINE_SMOOTH)
        gl.glHint(gl.GL_LINE_SMOOTH_HINT, gl.GL_NICEST)

        gl.glLineWidth(1.5)
        gl.glPointSize(4)

    def toggle_culling(self):
        self.view['cull'] = not self.view['cull']
        self.update_flags()

    def toggle_wireframe(self):
        self.view['wireframe'] = not self.view['wireframe']
        self.update_flags()

    def update_flags(self):
        if self.view['wireframe']:
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
        else:
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL)

        if self.view['cull']:
            gl.glEnable(gl.GL_CULL_FACE)
        else:
            gl.glDisable(gl.GL_CULL_FACE)

    def on_resize(self, width, height):
        if self.on_retina_screen:
            gl.glViewport(0, 0, width*2, height*2)
        else:
            gl.glViewport(0, 0, width, height)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.gluPerspective(60., width / float(height), .01,
                          self.scene.scale * 5.0)
        gl.glMatrixMode(gl.GL_MODELVIEW)
        self.view['ball'].place([width / 2, height / 2], (width + height) / 2)

    def on_mouse_press(self, x, y, buttons, modifiers):
        self.view['ball'].down([x, -y])

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        delta = np.array([dx, dy], dtype=np.float) / [self.width, self.height]

        # left mouse button, with control key down (pan)
        if ((buttons == pyglet.window.mouse.LEFT) and
                (modifiers & pyglet.window.key.MOD_CTRL)):
            self.view['translation'][0:2] += delta

        # left mouse button, no modifier keys pressed (rotate)
        elif (buttons == pyglet.window.mouse.LEFT):
            self.view['ball'].drag([x, -y])

    def on_mouse_scroll(self, x, y, dx, dy):
        self.view['translation'][2] += float(dy) / self.height

    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.W:
            self.toggle_wireframe()
        elif symbol == pyglet.window.key.Z:
            self.reset_view()
        elif symbol == pyglet.window.key.C:
            self.toggle_culling()
        elif symbol == pyglet.window.key.S:
            print("Saving screenshot ",end='',flush=True)
            ii = 0
            fname = "capture"
            while os.path.exists(fname+str(ii)+".png"): ii += 1
            filename = fname + str(ii) + ".png"
            print("("+filename+")")
            self.save_image(filename)
        elif symbol == pyglet.window.key.N:
            self.plot_normals = not self.plot_normals

    def on_draw(self):
        self._update_meshes()
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glLoadIdentity()

        # pull the new camera transform from the scene
        transform_camera, _junk = self.scene.graph['camera']
        # apply the camera transform to the matrix stack
        gl.glMultMatrixf(_gl_matrix(transform_camera))

        # dragging the mouse moves the view transform (but doesn't alter the
        # scene)
        transform_view = _view_transform(self.view)
        gl.glMultMatrixf(_gl_matrix(transform_view))

        # we want to render fully opaque objects first,
        # followed by objects which have transparency
        node_names = deque(self.scene.graph.nodes_geometry)
        count_original = len(node_names)
        # print('corig', count_original)
        count = -1

        #################################################
        #################################################
 #       gl.glDepthMask(False)
        for col,traj in zip(self.cols[0:len(self.extra_trajs)],self.extra_trajs):
            gl.glLineWidth(1.4)
            lentraj = len(traj)
            gl.glBegin(gl.GL_LINES)
            gl.glColor4f( col[0], col[1], col[2], col[3] )    
            for i,p in enumerate(traj):
                gl.glVertex3f(p[0],p[1],p[2])
                if i < lentraj - 1:
                    gl.glVertex3f(traj[i+1][0],traj[i+1][1],traj[i+1][2])
                if i == lentraj - 2: break
            gl.glEnd()
        #################################################
        plotv_normals = True
        plotf_normals = True
        vn_col = np.array([1.0, 0.0, 0.0, 1.0])
        fn_col = np.array([0.0, 0.0, 1.0, 1.0])
        if self.plot_normals:
            gl.glLineWidth(1.97)
            if self.tasp_mesh.has('vertex_normals') and plotv_normals:
                gl.glBegin(gl.GL_LINES)
                gl.glColor4f( vn_col[0], vn_col[1], vn_col[2], vn_col[3] )    
                for i in range(self.tasp_mesh.num_vertices):
                    n = self.tasp_mesh.vertex_normals[i] * self.normals_length
                    v = self.tasp_mesh.vertices[i]
                    gl.glVertex3f(v[0], v[1], v[2])
                    gl.glVertex3f(v[0] + n[0], v[1] + n[1], v[2] + n[2])
                gl.glEnd()
            if self.tasp_mesh.has('face_normals') and plotf_normals:
                gl.glBegin(gl.GL_LINES)
                gl.glColor4f( fn_col[0], fn_col[1], fn_col[2], fn_col[3] ) 
                for i in range(self.tasp_mesh.num_faces):
                    n = self.tasp_mesh.face_normals[i] * self.normals_length
                    v = self.tasp_mesh.face_centers[i]
                    gl.glVertex3f(v[0], v[1], v[2])
                    gl.glVertex3f(v[0] + n[0], v[1] + n[1], v[2] + n[2])
                gl.glEnd()
        #################################################
        if not self.marked_points is None:
            # Marked points is a list of vectors at which a little sphere will be drawn
            gl.glPointSize(self.marked_point_size)
            gl.glBegin(gl.GL_POINTS)
            for i, p in enumerate(self.marked_points):
                gl.glColor3f(self.marked_point_colors[i][0], 
                             self.marked_point_colors[i][1], 
                             self.marked_point_colors[i][2])
                gl.glVertex3f(p[0], p[1], p[2])
            gl.glEnd()

        #################################################
        #################################################

#        gl.glDepthMask(True)
        while len(node_names) > 0:
            count += 1
            current_node = node_names.popleft()
            
            # if the flag isn't defined, this will be None
            # by checking False explicitly, it makes the default
            # behaviour to render meshes with no flag defined.
            #if self.node_flag(name_node, 'visible') is False:
            #    continue

            transform, geometry_name = self.scene.graph[current_node]

            if geometry_name is None:
                continue
            
            mesh = self.scene.geometry[geometry_name]
            
            if (hasattr(mesh, 'visual') and
                    mesh.visual.transparency):
                # put the current item onto the back of the queue
                if count < count_original:
                    node_names.append(current_node)
                    continue

            # add a new matrix to the model stack
            gl.glPushMatrix()
            # transform by the nodes transform
            gl.glMultMatrixf(_gl_matrix(transform))
            # get the mode of the current geometry
            mode = self.vertex_list_mode[geometry_name]
            # draw the mesh with its transform applied
            self.vertex_list[geometry_name].draw(mode=mode)
            # pop the matrix stack as we drew what we needed to draw
            gl.glPopMatrix()

    def node_flag(self, node, flag):
        if (hasattr(self.scene, 'flags') and
            node in self.scene.flags and
                flag in self.scene.flags[node]):
            return self.scene.flags[node][flag]
        return None

    def save_image(self, filename):
        colorbuffer = pyglet.image.get_buffer_manager().get_color_buffer()
        colorbuffer.save(filename)

    def flip(self):
        '''
        This function is the last thing executed in the event loop,
        so if we want to close the window (self) here is the place to do it.
        '''
        super(self.__class__, self).flip()


        if self._img is not None:
            print('Saving image', self._img)
            self.save_image(self._img)
            self.close()
            pyglet.app.exit()



def _view_transform(view):
    '''
    Given a dictionary containing view parameters,
    calculate a transformation matrix.
    '''
    transform = view['ball'].matrix()
    transform[0:3, 3] = view['center']
    transform[0:3, 3] -= np.dot(transform[0:3, 0:3], view['center'])
    transform[0:3, 3] += view['translation'] * view['scale'] * 5.0
    return transform


def geometry_md5(geometry):
    md5 = geometry.md5()
    if hasattr(geometry, 'visual'):
        md5 += str(geometry.visual.crc())
    return md5


def mesh_to_vertex_list(mesh, group=None):
    '''
    Convert a Trimesh object to arguments for an
    indexed vertex list constructor.
    '''
    normals = mesh.vertex_normals.reshape(-1).tolist()
    faces = mesh.faces.reshape(-1).tolist()
    vertices = mesh.vertices.reshape(-1).tolist()
    color_gl = _validate_colors(mesh.visual.vertex_colors, len(mesh.vertices))

    args = (len(mesh.vertices),  # number of vertices
            gl.GL_TRIANGLES,    # mode
            group,              # group
            faces,              # indices
            ('v3f/static', vertices),
            ('n3f/static', normals),
            color_gl)
    return args


def path_to_vertex_list(path, group=None):
    def stack_lines(indices):
        """
        Stack a list of values that represent a polyline into
        individual line segments with duplicated consecutive values.
        Parameters
        ----------
        indices: sequence of items
        Returns
        ---------
        stacked: (n,2) set of items
        In [1]: trimesh.util.stack_lines([0,1,2])
        Out[1]:
        array([[0, 1],
               [1, 2]])
        In [2]: trimesh.util.stack_lines([0,1,2,4,5])
        Out[2]:
        array([[0, 1],
               [1, 2],
               [2, 4],
               [4, 5]])
        In [3]: trimesh.util.stack_lines([[0,0],[1,1],[2,2], [3,3]])
        Out[3]:
        array([[0, 0],
               [1, 1],
               [1, 1],
               [2, 2],
               [2, 2],
               [3, 3]])
        """
        indices = np.asanyarray(indices)
        if is_sequence(indices[0]):
            shape = (-1, len(indices[0]))
        else:
            shape = (-1, 2)
        return np.column_stack((indices[:-1],
                                indices[1:])).reshape(shape)
    vertices = path.vertices
    lines = np.vstack([stack_lines(e.discrete(path.vertices))
                       for e in path.entities])
    index = np.arange(len(lines))

    args = (len(lines),         # number of vertices
            gl.GL_LINES,        # mode
            group,              # group
            index.reshape(-1).tolist(),  # indices
            ('v3f/static', lines.reshape(-1)),
            ('c3f/static', np.array([.5, .10, .20] * len(lines))))
    return args

def is_shape(obj, shape):
    """
    Compare the shape of a numpy.ndarray to a target shape,
    with any value less than zero being considered a wildcard
    Note that if a list- like object is passed that is not a numpy
    array, this function will not convert it and will return False.
    Parameters
    ---------
    obj:   np.ndarray to check the shape of
    shape: list or tuple of shape.
           Any negative term will be considered a wildcard
           Any tuple term will be evaluated as an OR
    Returns
    ---------
    shape_ok: bool, True if shape of obj matches query shape
    Examples
    ------------------------
    In [1]: a = np.random.random((100,3))
    In [2]: a.shape
    Out[2]: (100, 3)
    In [3]: trimesh.util.is_shape(a, (-1,3))
    Out[3]: True
    In [4]: trimesh.util.is_shape(a, (-1,3,5))
    Out[4]: False
    In [5]: trimesh.util.is_shape(a, (100,-1))
    Out[5]: True
    In [6]: trimesh.util.is_shape(a, (-1,(3,4)))
    Out[6]: True
    In [7]: trimesh.util.is_shape(a, (-1,(4,5)))
    Out[7]: False
    """
    # if the obj.shape is different length than
    # the goal shape it means they have different number
    # of dimensions and thus the obj is not the query shape
    if (not hasattr(obj, 'shape') or
            len(obj.shape) != len(shape)):
        return False
    # loop through each integer of the two shapes
    # multiple values are sequences
    # wildcards are less than zero (i.e. -1)
    for i, target in zip(obj.shape, shape):
        # check if current field has multiple acceptable values
        if is_sequence(target):
            if i in target:
                # obj shape is in the accepted values
                continue
            else:
                return False
        # check if current field is a wildcard
        if target < 0:
            if i == 0:
                # if a dimension is 0, we don't allow
                # that to match to a wildcard
                # it would have to be explicitly called out as 0
                return False
            else:
                continue
        # since we have a single target and a single value,
        # if they are not equal we have an answer
        if target != i:
            return False
    # since none of the checks failed the obj.shape
    # matches the pattern
    return True

def points_to_vertex_list(points, colors, group=None):
    points = np.asanyarray(points)
    

    if not is_shape(points, (-1, 3)):
        print(points)
        raise ValueError('Pointcloud must be (n,3)!')

    color_gl = _validate_colors(colors, len(points))

    index = np.arange(len(points))

    args = (len(points),         # number of vertices
            gl.GL_POINTS,        # mode
            group,              # group
            index.reshape(-1),  # indices
            ('v3f/static', points.reshape(-1)),
            color_gl)
    return args

def _validate_colors(colors, count):
    '''
    Given a list of colors (or None) return a GL- acceptable list of colors
    Parameters
    ------------
    colors: (count, (3 or 4)) colors
    Returns
    ---------
    colors_type: str, color type
    colors_gl:   list, count length
    '''

    colors = np.asanyarray(colors)
    count = int(count)
    if is_shape(colors, (count, (3, 4))):
        # convert the numpy dtype code to an opengl one
        colors_dtype = {'f': 'f',
                        'i': 'B',
                        'u': 'B'}[colors.dtype.kind]
        # create the data type description string pyglet expects
        colors_type = 'c' + str(colors.shape[1]) + colors_dtype + '/static'
        # reshape the 2D array into a 1D one and then convert to a python list
        colors = colors.reshape(-1).tolist()
    else:
        # case where colors are wrong shape, use a default color
        colors = np.tile([.5, .10, .20], (count, 1)).reshape(-1).tolist()
        colors_type = 'c3f/static'

    return colors_type, colors


def _gl_matrix(array):
    '''
    Convert a sane numpy transformation matrix (row major, (4,4))
    to a stupid GLfloat transformation matrix (column major, (16,))
    '''
    a = np.array(array).T.reshape(-1)
    return (gl.GLfloat * len(a))(*a)


def _gl_vector(array, *args):
    '''
    Convert an array and an optional set of args into a flat vector of GLfloat
    '''
    array = np.array(array)
    if len(args) > 0:
        array = np.append(array, args)
    vector = (gl.GLfloat * len(array))(*array)
    return vector


def is_none(obj):
    """
    Check to see if an object is None or not.
    Handles the case of np.array(None) as well.
    Parameters
    -------------
    obj: object to be checked
    Returns
    -------------
    is_none: bool, True if obj is None
    """
    if obj is None:
        return True
    if (is_sequence(obj) and
        len(obj) == 1 and
            obj[0] is None):
        return True
    return False


def is_sequence(obj):
    """
    Check if an object is a sequence or not.
    Parameters
    -------------
    obj: object to be checked
    Returns
    -------------
    is_sequence: bool, True if object is sequence
    """
    seq = (not hasattr(obj, "strip") and
           hasattr(obj, "__getitem__") or
           hasattr(obj, "__iter__"))

    # check to make sure it is not a set, string, or dictionary
    seq = seq and all(not isinstance(obj, i) for i in (dict,
                                                       set,
                                                       basestring))

    # numpy sometimes returns objects that are single float64 values
    # but sure look like sequences, so we check the shape
    if hasattr(obj, 'shape'):
        seq = seq and obj.shape != ()

    return seq


def type_bases(obj, depth=4):
    """
    Return the bases of the object passed.
    """
    # bases = collections.deque([list(obj.__class__.__bases__)])
    bases = deque([list(obj.__class__.__bases__)])
    for i in range(depth):
        bases.append([i.__base__ for i in bases[-1] if i is not None])
    try:
        bases = np.hstack(bases)
    except IndexError:
        bases = []
    # we do the hasattr as None/NoneType can be in the list of bases
    bases = [i for i in bases if hasattr(i, '__name__')]
    return np.array(bases)


def type_named(obj, name):
    """
    Similar to the type() builtin, but looks in class bases for named instance.
    Parameters
    ----------
    obj: object to look for class of
    name : str, name of class
    Returns
    ----------
    named class, or None
    """
    # if obj is a member of the named class, return True
    name = str(name)
    if obj.__class__.__name__ == name:
        return obj.__class__
    for base in type_bases(obj):
        if base.__name__ == name:
            return base
    raise ValueError('Unable to extract class of name ' + name)
