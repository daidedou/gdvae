# TTAA

import numpy as np
import numpy.random as npr
import numpy.linalg as LA 

class Color:

    def __init__(self, input_color=None, copy=True):
        self._eps = 0.00001
        if input_color is None: self._vector = np.random.uniform(size=3)
        else:                   self._vector = np.array(input_color, copy=copy)
        for c in self._vector:
            assert (c >= 0 and c <= 1), 'Invalid color given!'

    def string_rgb(self):
        return '%.2f,%.2f,%.2f' % (self._vector[0],self._vector[1],self._vector[2])

    def __repr__(self): return self.string_rgb()

    ####################################################################################
    # Setters
    ####################################################################################    

    def set(self, r, g, b):
        if r > 1 or r < 0 or g > 1 or g < 0 or b < 0 or b > 1:
            print('Invalid color! (%f,%f,%f)' % (r,g,b))
        self._vector[0] = r
        self._vector[1] = g
        self._vector[2] = b

    def setv(self, v):
        self.set(v[0], v[1], v[2]) 

    def setFromName(self, cname):
        self.setv(self.color_dict[cname.lower()])

    def setFromInts(self, r, g, b):
        self.set(r/255.0, g/255.0, b/255.0)

    ####################################################################################
    # Getters
    ####################################################################################    

    def as_normalized_rgb(self):
        return self._vector

    def as_rgb_int255(self):
        return np.array([int(255 * v) for v in self._vector])

    # Hue output is in degrees (in [0,360)); s,v are in [0,1]
    def as_normalized_hsv(self):
        r, g, b = self._vector
        min_val = np.min(self._vector)
        max_val = np.max(self._vector)
        v = max_val
        delta = max_val - min_val
        if delta < self._eps:
            return np.array([0,0,v])
        if max_val > 0.0:
            s = delta / max_val
        else:
            s = 0.0
            h = 0.0 # It's really undefined in this case
            return np.array([h,s,v])
        if ( abs(r - max_val) < delta ):
            h = (g - b) / delta
        else:
            if (abs(g - max_val) < delta ):
                h = 2.0 + (b - r) / delta
            else:
                h = 4.0 + (r - g) / delta
        h *= 60.0 # It's in degrees
        if h < 0.0: h += 360
        return np.array([h,s,v])


    ####################################################################################
    # Static Methods
    ####################################################################################

    color_dict = {
        "red"       : np.array([1, 0, 0],       dtype=np.float64),
        "green"     : np.array([0, 1, 0],       dtype=np.float64),
        "blue"      : np.array([0, 0, 1],       dtype=np.float64),
        "white"     : np.array([1, 1, 1],       dtype=np.float64),
        "black"     : np.array([0, 0, 0],       dtype=np.float64),
        "orange"    : np.array([1, 0.75, 0],    dtype=np.float64),
        "brown"     : np.array([0.4, 0.2, 0],   dtype=np.float64),
        "pink"      : np.array([1, 0.5, 1],     dtype=np.float64),
        "turqoise"  : np.array([0, 1, 0.89],    dtype=np.float64),
        "purple"    : np.array([0.72, 0, 0.9],  dtype=np.float64),
        "forest"    : np.array([0, 0.58, 0.18], dtype=np.float64),
        "yellow"    : np.array([1, 1, 0],       dtype=np.float64),
        "sky"       : np.array([0.1, 0.55, 1],  dtype=np.float64),
        "grey"      : np.array([0.5, 0.5, 0.5], dtype=np.float64)
    }

    def from_ints(r,g,b):
        return Color([r/255.0, g/255.0, b/255.0])

    def from_name(colorname): return Color( Color.color_dict[colorname.lower()] )

    def named_colors(): return Color.color_dict.keys()

    def random(): return Color() # Default is random

    # Gives a set of random, highly different colors
    # starting_colors is a list of 3D numpy arrays
    # Output is a list of 3D numpy arrays
    # e.g. see https://gist.github.com/adewes/5884820
    def generateDifferentRandomColors(desired_length, starting_colors=None, num_samples=250):
        if starting_colors is None: colors = []
        else: colors = starting_colors
        _eps = 0.00001
        sqdist = lambda u,v: np.inner(u-v, u-v)
        while len(colors) < desired_length:
            tries = npr.uniform(low=_eps, high=1-_eps, size=(num_samples, 3))
            min_dist_to_any_curr_color = lambda r:  np.minimum( [ sqdist(c,r) for c in colors ] )
            min_dists = [ min_dist_to_any_curr_color(tries[i,:]) for i in range(num_samples) ]
            ind_of_max_dist = np.argmax(min_dists)
            # Choose the max color and add it to the chosen colors
            colors.append( tries[ind_of_max_dist,:] )
        return colors 

    ############### Colour Interpolation Methods ###############
    # All functions have a parameter going from 0 to 1

    ### Interpolator Methods ###

    def color_multiinterpolator(color_list, times=None):
        '''
        Produces a color function f : [0,1] -> R^3 that linearly interpolates over color_list.
        Color_list contains a list of color objects, with length n.
        times, if given, must be of length n-2 and each entry has 0 < t_i < 1
        '''
        n = len(color_list)
        if n == 2: return Color.color_biinterpolator(color_list[0], color_list[1])
        if times is None: times = np.linspace(0.0, 1.0, num=n-2)
        if not times is None:
            assert len(times) + 2 == n, 'Times length mismatch'
            assert all( (t > 0.0 and t < 1.0) for t in times ), 'Untenable times choices'
            times = [0.0] + times + [1.0]
        seglens = []
        vecs = [c._vector for c in color_list]
        # Now generate the interpolator function
        def interp_func(t):
            # First get the boundary times around t
            j = 1
            while t > times[j]: j += 1
            ti = times[j-1]
            tj = times[j]
            # Then compute the linear interpolation between the colors at those times
            Ci = vecs[j-1]
            Cj = vecs[j]
            return ( (tj - t)*Ci + (t - ti)*Cj ) / (tj - ti)
        return interp_func

    def color_biinterpolator(c1, c2):
        return lambda t: c2._vector*t + (1-t)*c1._vector

    ### Built-in Colors ###

    def red_to_green(reverse=False):
        if not reverse: return lambda t: np.array([1.0-t,t,0.0])
        return lambda t: np.array([t,1.0-t,0.0])

    def blue_to_green(reverse=False):
        if not reverse: return lambda t: np.array([0.0,t,1.0-t])
        return lambda t: np.array([0.0,1.0-t,t])

    def red_to_blue(reverse=False):
        if not reverse: return lambda t: np.array([1.0-t,0.0,t])
        return lambda t: np.array([t,0.0,1.0-t])

    def blue_to_white_to_red():
        return lambda t: np.array( [min(1.0, 2*t), abs( - abs(2*t - 1) + 1 ), min(1.0, -2*t + 2)] )

    def black_to_white():
        return Color.color_biinterpolator(Color.from_name('black'), Color.from_name('white'))

    def black_to_green():
        return Color.color_biinterpolator(Color.from_name('black'), Color.from_name('green'))  

    def green_to_black():
        return Color.color_biinterpolator(Color.from_name('green'), Color.from_name('black'))      

    def hot_to_cold_1():
        colors = [ 
            Color.from_ints(0,0,77),      # Dark blue
            Color.from_ints(0,0,179),     # Deep blue
            Color.from_ints(255,153,51),  # Orange
            Color.from_ints(204,0,0),     # Deep red
            Color.from_ints(77,0,0)       # Dark red
        ]
        times = [0.1, 0.7, 0.9]
        return Color.color_multiinterpolator(colors, times)
        

    def jet():
        colors = [ 
            Color.from_ints(0,0,77),      # Dark blue
            Color.from_ints(0,0,179),     # Deep blue
            Color.from_ints(77,255,219),  # Light blue
            Color.from_ints(102,255,102), # Light green
            Color.from_ints(255,255,77),  # Yellow
            Color.from_ints(255,153,51),  # Orange
            Color.from_ints(204,0,0),     # Deep red
            Color.from_ints(77,0,0)       # Dark red
        ]
        times = [0.15, 0.35, 0.5, 0.625, 0.75, 0.9]
        return Color.color_multiinterpolator(colors, times)


### Canadian Spelling ###
Colour = Color 
#########################
