# TTAA

import numpy as np, numpy.random as npr, numpy.linalg as LA, itertools, os, sys
from numpy.polynomial.polynomial import polyfit
from matplotlib.pyplot import figure, show
from ..utils.StatsUtils import truncate_outliers, remove_outliers
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.stats import pearsonr

def plotCorrelation(xs, ys, t1=None, t2=None, outlier_removal=0.0, block=True):
    # Some simple fixes/checks
    def check1(vs):
        if type(vs[0]) is list: return [v[0] for v in vs]
        elif type(vs[0]) is np.ndarray: return [v[0] for v in vs]
        else: return vs
    xs, ys = np.array(check1(xs)), np.array(check1(ys))
    print("Shape:", xs.shape, ys.shape)
    # Remove outliers
    assert outlier_removal >= 0 and outlier_removal < 0.5, "Untenable outlier removal value"
    if outlier_removal > 0.000001:
        def getOutlierIndices(vs):
            p_upper = np.percentile(vs, 100 - (100 * outlier_removal))
            p_lower = np.percentile(vs, 100 * outlier_removal)
            inds = []
            for i,v in enumerate(vs):
                if v > p_upper or v < p_lower:
                    inds.append( i )
            return inds
        xinds, yinds = getOutlierIndices(xs), getOutlierIndices(ys)
        allInds = list( set(xinds).union(set(yinds)) )
        xs, ys = np.delete(xs, allInds), np.delete(ys, allInds)
        print("New Shape:", xs.shape, ys.shape)
    # Actual plotting
    f = figure()
    a = f.gca()
    p = a.scatter(xs, ys, alpha=0.25, c='b', marker='o')
    b, m = polyfit(xs, ys, 1)
    print('Best Fit Line -> Slope: %.3f, Intercept: %.3f' % (m,b))
    print('PearsonR:', pearsonr(xs,ys))
    if not t1 is None: a.set_xlabel(t1)
    if not t2 is None: a.set_ylabel(t2)
    a.plot(xs, b + m*xs, 'r-')
    if block:
        show()
    else:
        f.show()

def plotDistribution(vals, rm_outliers=False, outliers_thresh=0.025,
                     fit_spline=False, fit_poly=False, poly_power=2,
                     xtitle=None, ytitle='Frequency', block=True):

    print('Plotting distribution')

    # Settings
    num_curve_eval_points = 200
    num_histo_bins = 100

    # Outlier removal if needed
    vals = np.array(vals)
    if rm_outliers: vals = remove_outliers(vals, outliers_thresh)

    # Matplotlib preparations
    f = figure()
    a = f.gca()
    # Get n (the histogram bin values), bins (the bin edges), and patches
    n, bins, patches = a.hist(vals, 50, facecolor='green', alpha=0.75)

    # Prepare for curve fittings
    bins_avg = np.array([ bins[i]+bins[i+1] for i in range(0,len(bins)-1) ]).flatten() / 2
    nf = n.flatten()

    # Polynomial or spline fitting
    if fit_poly:
        coefs = np.polyfit(bins_avg, nf, poly_power) # Highest power first
        eval_points = np.linspace(np.min(bins_avg), np.max(bins_avg), num=num_curve_eval_points)
        curve_vals = np.polyval(coefs, eval_points)
    elif fit_spline:
        from scipy.interpolate import spline
        eval_points = np.linspace(np.min(bins_avg), np.max(bins_avg), num=num_curve_eval_points)
        curve_vals = spline(bins_avg, nf, eval_points, order=3)
        a.plot(eval_points, curve_vals, 'r-')

    # Plot settings
    if not xtitle is None: a.set_xlabel(xtitle)
    if not ytitle is None: a.set_ylabel(ytitle)

    if block:
        show()
    else:
        f.show()


def plotArray(data, block=True):
    print('Plotting array')
    f = figure()
    a = f.gca()
    a.plot(data)
    f.show()


def plotMultiArrays(data_list, form_list=None, block=True, linewidth=1.0):
    '''
    E.g. red dashed = r--, green triangles = g^, blue squares = bs
    '''

    f = figure()
    a = f.gca()

    if form_list is None:
        for data in data_list:
            a.plot(data, linewidth=linewidth)
    else:
        for f, d in zip(form_list, data_list):
            a.plot(d, f, linewidth=linewidth)

    if block:
        show()
    else:
        f.show()


def plotDimensionallyReducedVectorsIn2D(vectors, method='pca', point_labels=None, verbose=True,
                                        manifold_learning_options={}, print_type='random'):
    '''
    Given a set of n-dimensional vectors, dimensionally reduce the set to 2D and plot it.
    i.e. V is an m x n matrix.
    We can color based on the point labels if given. Must be a list of
    '''
    method = method.lower()
    methods = ['pca', 'tsne', 'isomap', 'lle']
    assert method in methods, 'Method must be one of' + str(methods)
    if print_type is None: print_type = 'none'
    print_type = print_type.lower().strip()
    assert print_type in ['none', 'random', 'all']
    if not point_labels is None:
        assert len(point_labels) == len(vectors), "Vectors-to-labels mismatch"
        # assert all([type(pl) is int for pl in point_labels]), "Point labels must be integers"

    # Create figure
    f = figure()
    a = f.gca()

    ### Manifold Learning / Dimensionality Reduction ###
    # At the end, we get x_new as an m x 2 matrix
    val_with_default = lambda val, default: val if not val is None else default
    if method == 'pca':
        if verbose: print('Applying PCA')
        from sklearn.decomposition import PCA
        whiten = val_with_default( manifold_learning_options.get('whiten'), False )
        pca = PCA(n_components=2, whiten=whiten)
        x_new = pca.fit_transform(vectors)
    elif method == 'tsne':
        if verbose: print('Applying T-SNE')
        from sklearn.manifold import TSNE
        learning_rate = val_with_default(
                            manifold_learning_options.get('learning_rate'), 200.0 )
        perplexity = val_with_default(
                            manifold_learning_options.get('perplexity'), 30.0 )
        early_exaggeration = val_with_default(
                            manifold_learning_options.get('early_exaggeration'), 12.0 )
        x_new = TSNE(n_components = 2,
                     perplexity = perplexity,
                     early_exaggeration = early_exaggeration,
                     learning_rate = learning_rate
                ).fit_transform(vectors)
    elif method == 'isomap':
        if verbose: print('Applying Isomap')
        from sklearn.manifold import Isomap
        n_neighbors = val_with_default(manifold_learning_options.get('n_neighbors'), 5)
        x_new = Isomap(n_neighbors=n_neighbors).fit_transform(vectors)
    elif method == 'lle':
        if verbose: print('Applying LLE')
        from sklearn.manifold import LocallyLinearEmbedding
        n_neighbors = val_with_default(manifold_learning_options.get('n_neighbors'), 5)
        x_new = LocallyLinearEmbedding(n_neighbors=n_neighbors).fit_transform(vectors)


    # Unzip the data matrix
    x, y = x_new[:,0], x_new[:,1]

    ### Add labels to the plot if given ###
    if not point_labels is None:
        unique_labels = list(set(point_labels))
        N = len(unique_labels)
        # If labels are not integers, make them so
        if not type(point_labels[0]) is int:
            label_dict = { unlab : i for i, unlab in enumerate(unique_labels) }
            point_labels = [ label_dict[p_lab] for p_lab in point_labels ]
        # Adapted from the method given here:
        # https://stackoverflow.com/questions/12487060/matplotlib-color-according-to-class-labels
        # Define the colormap
        cmap = plt.cm.jet
        # Extract all colors from the .jet map
        cmaplist = [cmap(i) for i in range(cmap.N)]
        # Create the new map
        cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
        # Define the bins and normalize
        bounds = np.linspace(0, N, N + 1)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        # Create figure
        splot = a.scatter(x, y,
                          c=point_labels,
                          cmap=cmap,
                          norm=norm)
    else:
        splot = a.scatter(x, y)

    # Print some coords
    if print_type == 'random':
        n_to_print = 100
        choices = npr.choice(len(x), size=n_to_print, replace=False)
        for c in choices:
            print(c, x[c], y[c])
    elif print_type == 'all':
        pass

    # Finally show the plot
    plt.show()
# </ End Dimensionality reduction plotter /> #
#--------------------------------------------#

# Plot the confusion matrix of a classifier
def plot_confusion_matrix(true_Y,
                          predicted_Y,
                          classes,
                          normalize = False,
                          title = 'Confusion matrix',
                          return_cm = False,
                          show_plot = True,
                          save_plot = False,
                          save_name = None,
                          add_colorbar = True,
                          cmap = plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    classes is a list of the category labels (e.g. list(range(10)) for mnist)
    """
    # Checks
    if save_plot:
        assert not save_name is None
    else:
        if save_name is None:
            print('Warning: save_name given but not asked to save file')
    # Generate confusion matrix with sklearn
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(true_Y,
                          predicted_Y,
                          labels = classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # Create figure
    f = figure()
    a = f.gca()

    mappable_y_tho = a.imshow(cm, interpolation='nearest', cmap=cmap)
    a.set_title(title)
    if add_colorbar:
        cbar = f.colorbar(mappable_y_tho)
    tick_marks = np.arange(len(classes))
    a.set_xticks(tick_marks, classes) 
    a.set_yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        a.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")
    a.set_ylabel('True label')
    a.set_xlabel('Predicted label')
    f.tight_layout()
    if save_plot:
        print('Saving plot to', save_name)
        f.savefig( save_name )
    if show_plot: plt.show()
    if return_cm: return cm





















#
