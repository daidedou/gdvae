
import numpy as np
import os, sys, math
from scipy.special import lpmv, sph_harm
from ..structures.PointCloud import PointCloud

class SampledUnitSphere:
	''' 
	Random Sampling of S^2. 
	'''

	def __init__(self, npoints, mesh=False, artificialInsertions=None):
		# Generate unit sphere with npoints
		# e.g. artificial insertions = [[0,0,1],[0,0,-1]]
		self.n = npoints
		self.points = []
		if not artificialInsertions is None:
			for p in artificialInsertions:
				self.points.append(np.array(p))
		mu, sigma = 0, 1
		rps = np.random.normal(mu, sigma, self.n * 3)
		for k in range(0,3*npoints,3):
			pu = np.array([ rps[k], rps[k+1], rps[k+2] ])
			pn = pu / np.linalg.norm( pu )
			self.points.append(pn)

	def asPointCloud(self):
		return PointCloud(self.points)

	# Reads a sampled unit sphere from a file
	def read(filename):
		assert filename.endswith('.unitsphere'), "Improper extension"
		ps = []
		with open(filename,"r") as f:
			for line in [k.strip() for k in f.readlines()]:
				p = line.split(",")
				ps.append( np.array([ float(c) for c in p ]) )
		return ps

	# Writes this sampled sphere to a file
	def write(filename):
		assert filename.endswith('.unitsphere'), "Improper extension"
		with open(filename,"w") as f:
			s = "\n".join([ ",".join([str(w) for w in p]) for p in self.points ])
			f.write(s)

	# Use spherical harmonics to get estimated heat map
	# See: Chung, Heat Kernel Smoothing on the Unit Sphere
	# Gets: K_t(p,q) for all t in ts
	def heatKernel(p,q,ts,numFuncsToUse=1000):
		hkm = np.zeros(len(ts))
		pDotQ = np.dot(p,q)
		for k in range(0,numFuncsToUse):
			c1 = (2 * k + 1) / (4 * np.pi)
			c3 = lpmv(0, k, pDotQ)
			for i,t in enumerate(ts):
				c2 = np.exp( -k * (k + 1) * t )
				hkm[i] += c1*c2*c3
		return hkm

	def heatKernel3(p,q,ts,numFuncsToUse=1000):
		# See: Model building in Two-sphere via Gauss-Weierstrass Kernel Smoothing Chung, 2005
		def rSphericalHarmonics(m,n,theta,phi):
			ymn = sph_harm(abs(m),n,theta,phi)
			if m <= -1:
				return np.sqrt(2)*np.imag(ymn)
			if m >= 1:
				return np.sqrt(2)*np.real(ymn)
			else:
				return np.real(ymn)
		hkm = np.zeros(len(ts))
		thetap, phip = SampledUnitSphere.cart2sph_unit(p)
		thetaq, phiq = SampledUnitSphere.cart2sph_unit(q)
		for i,t in enumerate(ts):
			for k in range(0,numFuncsToUse+1):
				coef = np.exp(-k*(k+1)*t)
				col = 0.0
				for m in range(-k,k+1):
					col += rSphericalHarmonics(m,k,thetap,phip)*rSphericalHarmonics(m,k,thetaq,phiq)
				hkm[i] += coef * col	
		return hkm

	def heatKernelSig(p,ts,numFuncsToUse=1000):
		return SampledUnitSphere.heatKernel(p,p,ts,numFuncsToUse=numFuncsToUse)

	def heatKernelSig3(p,ts,numFuncsToUse=1000):
		return SampledUnitSphere.heatKernel3(p,p,ts,numFuncsToUse=numFuncsToUse)

	def cart2sph_unit(v):
		x,y,z = v
		phi = np.arccos(z) # Elevation
		theta = np.arctan2(y,x) # Azimuth
		return [theta,phi]

	def cart2sph_generic(x,y,z):
	    XsqPlusYsq = x**2 + y**2
	    r = m.sqrt(XsqPlusYsq + z**2)            # r
	    elev = m.atan2(z,m.sqrt(XsqPlusYsq))     # theta
	    az = m.atan2(y,x)                        # phi
	    return r, elev, az

	def disp(self):
		for p in self.points: 
			print(p)

	def plot3D(self):
		from matplotlib import pyplot
		import pylab
		from mpl_toolkits.mplot3d import Axes3D
		fig = pylab.figure()
		ax = Axes3D(fig)
		def split3D(ps):
			xs = [p[0] for p in ps]
			ys = [p[1] for p in ps]
			zs = [p[2] for p in ps]
			return [xs,ys,zs]
		xs,ys,zs = split3D(self.points)
		ax.scatter(xs,ys,zs)
		pyplot.show()




