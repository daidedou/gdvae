
import os, sys, math, pickle, numpy as np
import numpy.linalg as LA
import numpy.random as npr
from .PointCloud import PointCloud
from sklearn.neighbors import KDTree
from ..viewing.Color import Color
from timeit import default_timer as timer
from .spectra.reuter_femlbo import fem_laplacian
from ..utils.VecUtils import angle_between, normalize_set, angle_between_normed
from ..utils.StatsUtils import truncate_outliers
from ..utils.FunctionUtils import sloped_step_function_asVector
from ..utils.ViewUtils import lineUpInX
from collections import Counter

class MinimalTriangleMesh:

    def __init__(self, verticesList, trianglesList, copy=True, verbose=True, name=None, no_color=False):
        self.vertices = np.array(verticesList, copy=copy) # list of 3d vectors
        self.faces = np.array(trianglesList, copy=copy) # list of integer triplets
        self.num_vertices = len(self.vertices)
        self.num_faces = len(self.faces)
        self.name = 'Unnamed_Mesh' if name is None else name
        if verbose: print('Building', self.name, '(|V|=%d, |F|=%d)' % (self.num_vertices, self.num_faces))
        self.epsilon = 1e-7
        # Set up vertex colors
        if not no_color: self.vertex_colors = [ Color() for vert in self.vertices ]

    def readFromFile(targetFile, copy=False):
        return _readFromFile(targetFile, MinimalTriangleMesh, copy=copy)

    def minimalClone(self, name=None, copy=True, verbose=True):
        name = self.name + '-clone' if name is None else name
        return MinimalTriangleMesh(self.vertices, self.faces, copy=copy, verbose=verbose, name=name)

    # Default extension for pickle files
    PCKL_EXT = '.tasp_trimesh'

    def colorVerticesWithFullMeshFunction(self, f):
        """
        Pass in f, a function to color the vertices based on:
        # (1) the node index and (2) the mesh object
        I.e. f : (vertex_index, FullTriangleMesh) -> [0,1]^3  // From spatial position to color vector
        """
        for i in range(self.num_vertices):
            self.vertex_colors[i].setv( np.copy( f( i, self ) ) )

    def colorVerticesWithNodalPosFunction(self, f):
        """
        Pass in f, a function to color the vertices based on their position vector
        I.e. f : R^3 -> [0,1]^3  // From spatial position to color vector
        """
        for i in range(self.num_vertices):
            self.vertex_colors[i].setv( np.copy( f( self.vertices[i] ) ) )

    def colorVerticesWithOneValue(self, v):
        '''
        Color the mesh a single color, v in [0,1]^3
        '''
        for i in range(self.num_vertices): self.vertex_colors[i].setv( np.copy( v ) )

    def colorVerticesWithSegmentation(self, segments_vector, colors=None, use_random_colors=False):
        '''
        Colors a mesh based on a segmentation labelling.
        segments_vector is a series of labels (e.g. ints) that assign each node to a cluster/segment
        '''
        posible_segs = set(segments_vector)
        num_segs = len(posible_segs)
        if not colors is None: assert num_segs == len(colors), 'Segments and colors mismatch'
        statcols = Color.color_dict.values()
        n_static_colors = len( statcols )
        if colors is None:
            if use_random_colors: colors = Color.generateDifferentRandomColors(num_segs)
            elif num_segs < n_static_colors: colors = list( statcols )[0:num_segs]
            else: colors = Color.generateDifferentRandomColors(num_segs)
        colors_map = { seg_label : color for seg_label, color in zip(list(posible_segs), colors) }
        coloring_function = lambda v_ind, M: colors_map[ segments_vector[v_ind] ]
        self.colorVerticesWithFullMeshFunction(coloring_function)

    def colorVerticesWithVectorOnNodes(self, vec_on_mesh, coloring_function, verbose=False,
                                       clip_outliers=False, outlier_threshold=0.025,
                                       normalize_into_01=True):
        '''
        Colors the mesh vertices using a vector v of size |V| x 1.
        First normalizes the vector into [0,1] to act as the t parameter.
        Then defines the colors with the coloring_function : [0,1] -> [0,1]^3
        '''
        if clip_outliers:
            vec_on_mesh = truncate_outliers(vec_on_mesh, outlier_threshold)
        if normalize_into_01:
            minv, maxv = np.min(vec_on_mesh), np.max(vec_on_mesh)
            if abs(maxv - minv) <= self.epsilon: vec_on_mesh = np.zeros( (len(vec_on_mesh)) )
            else: vec_on_mesh = (vec_on_mesh - minv) / (maxv - minv)
        if verbose:
            print('Color Min: %f (Color:' % minv, str(coloring_function(0.0).tolist()) + ")")
            print('Color Max: %f (Color:' % maxv, str(coloring_function(1.0).tolist()) + ")")
        self.colorVerticesWithFullMeshFunction( lambda i, mesh: coloring_function( vec_on_mesh[i] ) )

    def generateImage_oglSS(self, img_name, marked_points=None, marked_point_size=None, verbose=False,
                            marked_point_colors=None, additional_meshes=None, lineup=False,
                            normals_length=None):
        self.display_1(marked_points=marked_points, marked_point_size=marked_point_size, verbose=verbose,
                       marked_point_colors=marked_point_colors, additional_meshes=additional_meshes,
                       lineup=lineup, normals_length=normals_length, save_image=img_name)

    def display_1(self, verbose=True, marked_points=None, marked_point_size=None,
                  marked_point_colors=None, additional_meshes=None, lineup=False, normals_length=None,
                  save_image=None):

        if lineup:
            q = additional_meshes + [self]
            avg_edge_len = np.mean( [c.getAverageEdgeLength() for c in q] )
            lineUpInX(q, avg_edge_len*5)

        import trimesh
        from trimesh import Trimesh
        from trimesh.scene import Scene
        from ..viewing.SceneViewCustom import SceneViewer

        # Method for trimesh conversion
        def convToTrimesh(tasp_mesh):
            m = Trimesh(
                vertices=tasp_mesh.vertices.tolist(),
                faces=tasp_mesh.faces.tolist(),
                process=False,
                validate=False
            ) # to list needed!
            cc = [ vc.as_normalized_rgb().tolist() + [0.99] for vc in tasp_mesh.vertex_colors ]
            m.visual.vertex_colors = cc
            m.name = tasp_mesh.name
            # print('mn', m.name)
            return m
        # Convert calling mesh
        mesh = convToTrimesh(self)
        if verbose: print('Built mesh')
        # Process other meshes
        if not additional_meshes is None:
            additional_meshes_trimesh = [ convToTrimesh(a) for a in additional_meshes ]
            if verbose: print('Processed additional meshes')

        # NOTE: normals display with multiple models is broken

        if additional_meshes is None:
            scene = Scene(geometry=mesh) # mesh.scene(geometry=)
        else:
            scene = Scene(geometry=[mesh]+additional_meshes_trimesh)
        if verbose: print('Built scene')
        if not marked_points is None:
            marked_points = np.array(marked_points, copy=True)
        def viewer():
            SceneViewer(scene,
                tasp_mesh=self,
                marked_points=marked_points,
                marked_point_size=marked_point_size,
                marked_point_colors=marked_point_colors,
                normals_length=normals_length,
                save_image=save_image,
                additional_meshes=None )
        block = True
        if block: viewer()
        else:
            from threading import Thread
            Thread(target=viewer).start()

    # Marked points can be a list of vectors or a list of integers (the latter being vertex indices)
    def display(self, marked_points=None, marked_point_size=None, verbose=False, marked_point_colors=None,
                additional_meshes=None, lineup=False, normals_length=None):
        self.display_1(marked_points=marked_points, marked_point_size=marked_point_size, verbose=verbose,
                       marked_point_colors=marked_point_colors, additional_meshes=additional_meshes,
                       lineup=lineup, normals_length=normals_length)

    def viewMeshAcrossColorVectors(self, color_vecs, color_function, alignment_offset=None,
                                   same_color_scale=False, clip_outliers=False,
                                   outlier_threshold=0.005):
        '''
        Views n duplicates of this mesh, each with a different coloring.

        color_vecs is either a matrix or list of vectors.
        For each vector, we generate another mesh and color it with that vector.
        We then view all of them at once.
        '''
        cvecs = np.array(color_vecs)
        if clip_outliers:
            cvecs = [truncate_outliers(cv, outlier_threshold) for cv in cvecs]
        if same_color_scale:
            cvecs = normalize_set(cvecs)
        # Generate minimal clones
        othclones = [self.minimalClone() for j in range(len(cvecs)-1)]
        clones = othclones + [self]
        # Color each clone
        for i, c in enumerate(clones):
            norm_indep = not same_color_scale
            c.colorVerticesWithVectorOnNodes(cvecs[i,:], color_function, normalize_into_01=norm_indep)
        # Alignment in x
        lineUpInX(clones, alignment_offset)
        # View all models at once
        self.display(additional_meshes=othclones)

    def bounding_coordinates(self):
        # Format: [min_x, max_x, min_y, max_y, min_z, max_z]
        return [ self.min_x(), self.max_x(),
                 self.min_y(), self.max_y(),
                 self.min_z(), self.max_z() ]

    def min_x(self): return np.min(self.vertices[:,0], axis=0)
    def max_x(self): return np.max(self.vertices[:,0], axis=0)
    def min_y(self): return np.min(self.vertices[:,1], axis=0)
    def max_y(self): return np.max(self.vertices[:,1], axis=0)
    def min_z(self): return np.min(self.vertices[:,2], axis=0)
    def max_z(self): return np.max(self.vertices[:,2], axis=0)

    def axis_min(self, i): return np.min(self.vertices[:,i], axis=0)
    def axis_max(self, i): return np.max(self.vertices[:,i], axis=0)

    def bounding_box_lengths(self):
        bs = self.bounding_coordinates()
        return np.array([bs[1] - bs[0], bs[3] - bs[2], bs[5] - bs[4]])

    def meanNodePosition(self):
        return np.mean(self.vertices, axis=0)

    def has(self,s): return hasattr(self, s)

    def translate(self, translation_vec):
        '''
        Translates the mesh by the input vector in-place.
        Beware: affects the stored vertex positions.
        '''
        tvec = np.array(translation_vec)
        self.vertices += tvec
        if self.has('face_centers'): self.face_centers += tvec
        if self.has('node_tree'): self.node_tree = None

    # def scale(self, scale_factors_per_vertex)

    def centerAt(self, new_center):
        '''
        Centers the mesh to the input point in-place.
        Beware: affects the stored vertex positions.
        '''
        currMean = np.mean(self.vertices, axis=0)
        self.translate(new_center - np.array(currMean))
        if self.has('node_tree'): self.node_tree = None

class TriangleMesh(MinimalTriangleMesh):

    def __init__(self, verticesList, trianglesList, copy=True, verbose=True, name=None):
        super(TriangleMesh, self).__init__(verticesList, trianglesList, copy, verbose, name)
        if verbose: print('Constructing', self.name)
        # A map of nodes to the faces they participate in
        self.node2face_map = [ set() for vertex in self.vertices ]
        # A map of nodes to their neighboring vertices
        self.neighbours = [ set() for vertex in self.vertices ]
        # Extract edges
        if verbose: print('\tExtracting edges')
        edge_set = set()
        for i, face in enumerate(self.faces):
            for j in range(3):
                n1, n2 = face[j], face[(j+1) % 3]
                if n1 < n2: edge_set.add( (n1, n2) )
                else:       edge_set.add( (n2, n1) )
                # Update neighbors
                # print('i',i,' j',j,' n1',n1,' n2',n2,' face',face,' nnebs/|V|',len(self.neighbours))
                self.neighbours[n1].add( n2 )
                self.neighbours[n2].add( n1 )
                # Update face mapping
                self.node2face_map[n1].add( i )
                self.node2face_map[n2].add( i )
        self.edges = np.array( list(edge_set) )
        # A map of nodes to the edges they participate in
        self.node2edge_map = [ set() for vertex in self.vertices ]
        if verbose: print('\tBuilding node2edge map')
        for i, edge in enumerate(self.edges):
            v1, v2 = edge
            # Update node -> edges it participates in map
            self.node2edge_map[v1].add( i )
            self.node2edge_map[v2].add( i )

        # Lengths and sizes
        self.num_edges = len(self.edges)
        self.euler_characteristic = self.num_vertices - self.num_edges + self.num_faces
        self.genus = int( (self.euler_characteristic - 2) / (-2) )

        ### Check whether mesh is closed ###
        # If every edge has two faces it is part of, the mesh is ok
        self.is_closed_manifold = True
        # Boundary edges have only one attached face
        # Will be empty if we have a closed manifold
        self.boundary_edges = []

        # Update edge -> faces it participates in map
        self.edge2face_map = [ None for e in self.edges ]
        self.adjacent_faces = [ set() for f in self.faces ]
        if verbose: print('\tBuilding edge2face, triangle adjacenies, and boundaries map')
        for j, edge in enumerate(self.edges):
            # Add to edge2face map
            v1, v2 = edge
            f1 = self.node2face_map[v1] # all faces around v1
            f2 = self.node2face_map[v2] # all faces around v2
            shared = list(set.intersection(f1, f2)) # faces shared by v1 & v2, and thus their edge
            self.edge2face_map[j] = shared
            if len(shared) == 1: # no face neighbor
                self.boundary_edges.append(edge)
            elif len(shared) == 2: # one face neighbor over this edge
                # Add to adjacent faces maps
                self.adjacent_faces[shared[0]].add(shared[1])
                self.adjacent_faces[shared[1]].add(shared[0])
            # TODO "non-manifold" case with more than two triangles sharing an edge
        self.is_closed_manifold = (len(self.boundary_edges) == 0)

    def print_info(self):
        print('-'*20, '\nInformation for', self.name, '\n'+'-'*20)
        print('Num Vertices: %d' % self.num_vertices)
        print('Num Edges: %d' % self.num_edges)
        print('Num Faces: %d' % self.num_faces)
        print('Is Closed Manifold?', self.is_closed_manifold)
        if not self.is_closed_manifold: print('Boundary edges:', len(self.boundary_edges))
        ed, fd = self.num_edges / self.num_vertices, self.num_faces / self.num_vertices
        print('V:E:F = 1:%.2f:%.2f' % (ed, fd))
        print('Genus: %d (Euler Characteristic: %d)' % (self.genus, self.euler_characteristic))
        els = self.getEdgeLengths()
        minel, maxel, avgel = np.min(els), np.max(els), np.mean(els)
        print("Min, Max, Avg Edge lengths: %.5f, %.5f, %.5f" % (minel, maxel, avgel))
        print('Mean Node Position:', np.mean(self.vertices, axis=0))
        xma = (self.min_x(), self.min_y(), self.min_z(), self.max_x(), self.max_y(), self.max_z())
        print('Bounding values: mins = (%.2f, %.2f, %.2f), maxs = (%.2f, %.2f, %.2f)' % xma)
        n_components, labels, adjmat = self.connectedComponents()
        print('Num_connected_components:', n_components)

    def get_neighbours_about_node(self,node_index): return self.neighbours[node_index]
    def get_adjacent_faces_about_face(self,face_index): return self.adjacent_faces[face_index]
    def get_faces_around_node(self,node_index): return self.node2face_map[node_index]
    def get_edges_about_node(self,node_index): return self.node2edge_map[node_index]
    def get_triangles_with_edge(self,edge_index): return self.edge2face_map[edge_index]

    def one_ring_positions_about_node_index(self, node_index, include_input_node=True):
        nebs = self.get_neighbours_about_node(node_index)
        ring = [self.vertices[j] for j in nebs]
        if include_input_node: ring.insert(0, self.vertices[node_index])
        return np.array(ring)

    # Input is any point in the ambient space; output(s) are vertices on the manifold
    def get_closest_vertices_to_point(self,input_point,num_closest_verts_to_get,include_distances=False):
        dists, inds = self.node_tree.query([input_point], k=num_closest_verts_to_get)
        if include_distances: return dists, inds
        else: return inds

    # Generates a kd-tree of the vertices for fast nearest neighbours search
    def build_kdtree(self): self.node_tree = KDTree(self.vertices) # X is n_samples by n_features

    def approximate_local_node_area(self, node_index):
        '''
        Get the area associated to a vertex, approximated by (1/3) the area of the faces of which is is part
        (i.e. the barycentric area approximation)
        '''
        neb_faces, s = self.get_faces_around_node(node_index), 0.0
        for find in neb_faces: s += self.face_areas[find]
        return s / 3.0

    def get_approximate_node_areas(self, store=True):
        if self.has('approximate_node_areas'):
            return self.approximate_node_areas
        areas = np.array([self.approximate_local_node_area(i) for i in range(self.num_vertices)])
        if store: self.approximate_node_areas = areas
        return areas

    def constructAdjacencyMatrix(self):
        from scipy.sparse import lil_matrix
        adj_mat = lil_matrix( (self.num_vertices, self.num_vertices), dtype=np.int8 )
        for e in self.edges:
            i, j = e
            adj_mat[i, j] = 1
            adj_mat[j, i] = 1
        return adj_mat

    def connectedComponents(self, adjmat=None):
        from scipy.sparse.csgraph import connected_components
        if adjmat is None: adjmat = self.constructAdjacencyMatrix()
        n_components, labels = connected_components(adjmat, directed=False)
        return n_components, labels, adjmat

    ##### Edge Length Computations #####

    def getEdgeLengths(self):
        """
        Compute the lengths of all the edges.
        Adds a field to the object (self.edge_lengths).
        """
        if self.has('edge_lengths'): return self.edge_lengths
        self.edge_lengths = np.zeros((self.num_edges,1))
        for i, (v1, v2) in enumerate(self.edges):
            self.edge_lengths[i] = LA.norm(self.vertices[v1] - self.vertices[v2])
        return self.edge_lengths

    def getAverageEdgeLength(self):
        if not self.has('edge_lengths'): self.getEdgeLengths() # computes the edge_lengths
        return np.mean( self.edge_lengths )

    ##### Face Information Calculations #####

    def computeFaceInformation(self, verbose=True, dont_correct=False, correct_winding=False,
                               ignore_face_normals=False):
        '''
        Computes and sets the face_angles, normals, centers, and areas
        '''
        # Face angles: compute three angles per face (one per node in the triangle)
        # The angles are ordered as in the triangle order and in radians
        self.face_angles = np.zeros((self.num_faces, 3))
        if not ignore_face_normals: self.face_normals = np.zeros((self.num_faces, 3))
        self.face_centers = np.zeros((self.num_faces, 3))
        self.face_areas = np.zeros((self.num_faces))
        if verbose: print('\tComputing face angles and normals')
        epsilon = 1e-9 
        sqrt_eps = math.sqrt(epsilon)
        for k, face in enumerate(self.faces):
            v1 = self.vertices[ face[0] ]
            v2 = self.vertices[ face[1] ]
            v3 = self.vertices[ face[2] ]
            s1i = v2 - v1
            s2i = v3 - v1
            s3i = v3 - v2
            n1 = LA.norm(s1i)
            n2 = LA.norm(s2i)
            n3 = LA.norm(s3i)
            # Treat all angles as -1 if the norms are unacceptable
            if (n1 < epsilon) or (n2 < epsilon) or (n3 < epsilon):
                if verbose: print('Face %d: untenable norm detected' % k)
                self.face_angles[k, :] -= 1
                continue
            s1 = s1i / n1
            s2 = s2i / n2
            s3 = s3i / n3
            # Node 1 angle
            s1dots2 = np.clip( np.dot(s1,s2), -1 + epsilon, 1 - epsilon )
            a1 = math.acos(s1dots2)
            self.face_angles[k, 0] = a1
            # Node 2 angle
            s1dotns3 = np.clip( np.dot(-s1,s3), -1 + epsilon, 1 - epsilon )
            a2 = math.acos(s1dotns3)
            self.face_angles[k, 1] = a2
            # Node 3 angle
            self.face_angles[k, 2] = np.pi - a1 - a2
            ### Face area ###
            # See: Boldo, How to Compute the Area of a Triangle: a Formal Revisit
            # Need: 0 <= c <= b <= a
            a = max(n1, n2, n3)
            c = min(n1, n2, n3)
            b = (n1 + n2 + n3) - (a + c)
            if c < epsilon:
                if verbose: print('Warning [%d]: small side-length detected (c)' % k)
                c = epsilon
            if b < epsilon:
                if verbose: print('Warning [%d]: small side-length detected (b)' % k)
                b = epsilon
            if a < epsilon:
                if verbose: print('Warning [%d]: small side-length detected (a)' % k)
                a = epsilon
            inner_prod = (a + (b + c)) * (c - (a - b)) * (c + (a - b)) * (a + (b - c))
            if inner_prod < epsilon: inner_prod = epsilon
            self.face_areas[k] = 0.25 * math.sqrt( inner_prod )
            ### Compute surface normal ###
            # note that this normal could be flipped
            if not ignore_face_normals:
                self.face_normals[k, :] = np.cross(s1, s2)
                self.face_normals[k, :] /= LA.norm(self.face_normals[k, :])
            ### Face centers ###
            self.face_centers[k] = (v1 + v2 + v3) / 3.0
        # No need to correct if we are ignoring them
        if not ignore_face_normals:
            # Run graph-theoretic consistency check on the face normal orientations
            if not dont_correct: self._correctFaceNormals(verbose)
            # Attempt to correct triangle winding (for front/back face assignment) using the
            # presumably outward facing triangle normal
            if correct_winding: self._correctFaceWinding(verbose)

    def _correctFaceWinding(self, verbose=True):
        '''
        Attempts to correct the face winding i.e. face ordering based on alignment to the normals
        '''
        if verbose: print('Correcting face ordering')
        num_corrected = 0
        for i,face in enumerate(self.faces):
            n = self.face_normals[i]
            p0, p1, p2 = self.vertices[face[0]], self.vertices[face[1]], self.vertices[face[2]]
            test_val = np.dot( np.cross(p1-p0, p2-p0), n )
            if test_val < 0.0:
                num_corrected += 1
                self.faces[i] = [face[0], face[2], face[1]]
        print('\tCorrected %d face windings' % num_corrected)

    # Uses graph propagation to enforce normals consistency on the mesh faces
    def _correctFaceNormals(self, verbose=True):
        if verbose: print('\tRunning face normals correction')
        epsilon, want_to_do_inconsis_check = self.epsilon, False
        # Heuristic: start with the point furthest in the y-direction, and assume its normal
        # should also point in the positive y direction, approximately. This point also serves
        # as the starting point of the propagation.
        target_face_ind = np.argmax(self.face_centers[:,1], axis = 0)
        init_face_normal = self.face_normals[target_face_ind]
        if init_face_normal[1] < 0: # Flip if normal is pointing "down"
            self.face_normals[target_face_ind] *= -1
        # Track which faces have been checked. Use only those for later corrections.
        corrected = [False for face in self.faces]
        corrected[target_face_ind] = True
        # Start running graph propagation
        init_nebs = self.get_adjacent_faces_about_face(target_face_ind)
        face_queue = Queue(init_nebs)
        # Flip checker
        pi_over_2, counter = np.pi / 2.0, 0
        def should_flip(v1, v2):
            dp = np.dot(v1, v2)
            if dp >  0.99: dp -= epsilon
            if dp < -0.99: dp += epsilon
            return math.acos( dp ) > pi_over_2
        while face_queue.hasItems():
            counter += 1
            # if counter == self.num_faces / 2: print('\t\t50% complete')
            #if counter > self.num_faces / 2: sys.exit(0)
            # print(face_queue.items)
            curr_face = face_queue.dequeue()
            # Skip this one if already processed
            if corrected[curr_face]: continue
            # Get current normal
            curr_normal = self.face_normals[curr_face]
            # Get face neighbours that have been corrected
            nebs = self.get_adjacent_faces_about_face(curr_face)
            checked_nebs = [neb for neb in nebs if corrected[neb]]
            # Flag to not flip this time
            skip_flipping = False
            # Error check 1: no neighbours found
            if len(checked_nebs) == 0:
                # Might be normal if there are unconnected components
                em1 = 'Error: unable to find a checked neighbour!'
                print('%s (face: %d, nebs_len: %d)' % (em1, curr_face, len(nebs)))
                skip_flipping = True
            # Error check 2: if norm is unacceptable (i.e. not 1), just take a checked neighbour's value
            normal_norm = LA.norm(curr_normal)
            if normal_norm < 1 - epsilon or normal_norm > 1 + epsilon:
                cfi = curr_face
                emsg = 'by replacing its value with a checked'
                print('Warning: correcting current normal %d %s neighbour\'s value' % (cfi, emsg))
                self.face_normals[curr_face] = self.face_normals[checked_nebs[0]] # First neighbour
                skip_flipping = True
            # Perform flipping if needed
            if not skip_flipping:
                # Get whether to flip or not, by comparing to each corrected neighbour
                to_flip = [ should_flip(curr_normal, self.face_normals[neb_ind]) for neb_ind in checked_nebs ]
                # Flip if needed
                if all(to_flip): self.face_normals[curr_face] *= -1
                # Check for inconsistency. Can be unavoidable, depending on topology and so on
                if verbose and want_to_do_inconsis_check:
                    if (False in to_flip) and (True in to_flip):
                        em2 = '\t\tInconsistency in recommended direction of neighbours!'
                        print('%s (face: %d, nebs_len: %d)' % (em2, curr_face, len(nebs)))
                        print('\t\t\tEncountered', nebs, to_flip)
            # Note this one is done
            corrected[curr_face] = True
            # Queue up unchecked neighbours
            face_queue.enqueue_all([neb for neb in nebs if not corrected[neb]])
        # Quick check (for completeness)
        if not all(corrected):
            nffalse = sum( map(lambda x: 0 if x else 1, corrected) )
            print('Unexpected inconsistency: some faces uncorrected (%d/%d)' % (nffalse, self.num_faces))


    ##### Surface Normals Computations #####

    def computeVertexNormals(self, verbose=True, weight_by_tip_angles=True):
        '''
        Compute vertex normals using the face normals (which must be pre-computed), via
        tip-angle weighting.
        '''
        epsilon = self.epsilon
        if not self.has('face_normals'): self.computeFaceInformation(verbose)
        self.vertex_normals = np.zeros((self.num_vertices, 3))
        if verbose: print('\tComputing vertex surface normals')
        for i, v in enumerate(self.vertices):
            associated_faces = self.node2face_map[i]
            n_assoc_faces = len(associated_faces)
            running_mean, running_ind = np.zeros((3)), 1.0
            face_weights = []
            stored_normals = []
            # For each face around the current vertex
            for k,face in enumerate(associated_faces):
                # Surface normal of the current face
                curr_normal = self.face_normals[face]
                # Vector magnitude of the current surface normal
                curr_normal_mag = LA.norm(curr_normal)
                # Check if the current magnitude is acceptable
                # Skip this face if the normal magnitude is less than one
                if curr_normal_mag < 1 - epsilon: continue
                if any(np.isnan(curr_normal)): continue
                # Ensure you are averaging over matching normals
                mean_norm = LA.norm(running_mean)
                # Note if the mean norm is too small, the only faces seen so far have been degenerate
                # Flip vector if doesn't match current normal direction.
                if k > 0 and mean_norm > epsilon:
                    # Check if angle is larger than 90 degrees. If so, flip it.
                    lengthc = np.dot(running_mean, curr_normal) / mean_norm
                    # Avoid numerical problems by tiny offset
                    if lengthc >  0.99: lengthc -= epsilon
                    if lengthc < -0.99: lengthc += epsilon
                    # Compute final angle
                    angle = math.acos( lengthc )
                    # Flip if the vectors are more than 90 degrees apart
                    if angle > np.pi / 2: curr_normal *= -1
                # Update the running mean with the value for this normal
                running_mean += (curr_normal - running_mean) / running_ind
                running_ind += 1
                # Store the normals and their weights, if needed
                if weight_by_tip_angles:
                    curr_face = self.faces[face]
                    if   i == curr_face[0]: w = self.face_angles[face, 0]
                    elif i == curr_face[1]: w = self.face_angles[face, 1]
                    else:                   w = self.face_angles[face, 2]
                    face_weights.append(w)
                    stored_normals.append(curr_normal)
            # Finally aggregate the normals seen for this (the ith) vertex
            if weight_by_tip_angles:
                for j, n in enumerate(stored_normals):
                    self.vertex_normals[i, :] += face_weights[j] * n
                self.vertex_normals[i, :] /= LA.norm(self.vertex_normals[i,:])
            else:
                mean_norm = LA.norm(running_mean)
                if mean_norm > epsilon: # Leave as zeros if unable to get normal
                    self.vertex_normals[i, :] = running_mean / mean_norm # ensure normalized
                else:
                    print('Failed to find stable normal at vertex %d' % i)

    def computeFaceNormalsFromVertexNormals(self, verbose=True):
        '''
        Compute the face normals from the vertex normals.
        Faces have a well-defined normal, unlike vertices, so the vertex normals are just used to determine
        the orientation of the face normals.
        '''
        if verbose: print('Computing face normals from vertex normals')
        self.face_normals = np.zeros((self.num_faces, 3))
        eps = self.epsilon
        for i, f in enumerate(self.faces):
            # Vertex indices
            vi1, vi2, vi3 = f
            # Vertex normals
            vn1, vn2, vn3 = self.vertex_normals[vi1], self.vertex_normals[vi2], self.vertex_normals[vi3]
            # Vertex coordinates and edge vectors
            v1, v2, v3 = self.vertices[vi1], self.vertices[vi2], self.vertices[vi3]
            s1i, s2i, s3i = v2 - v1, v3 - v1, v3 - v2
            n1, n2, n3 = LA.norm(s1i), LA.norm(s2i), LA.norm(s3i)
            s1, s2, s3 = s1i / n1, s2i / n2, s3i / n3
            # Average of vertex normals
            v_avg = (v1 + v2 + v3) / 3.0
            v_avg /= LA.norm(v_avg)
            # Compute a local face normal
            fn = np.cross(s1, s2)
            nf = LA.norm(fn)
            if nf < eps: fn = v_avg
            else:        fn /= nf
            # Flip the face normals to match the surrounding vertex normals
            if angle_between_normed(v_avg, fn) > np.pi / 2:
                fn = -fn
            # Assign to the shape
            self.face_normals[i, :] = fn

    def _pruneSinglePoints(node_positions, faces, verbose=True, return_as_mesh=False, generatedClass=None,
                           copy_faces=True):
        '''
        Generates a new mesh with single points (i.e. no points participating in a triangle) removed.
        Updates faces (or a copy of it) to cope with the changes to the vertices.
        '''
        faces = np.array(faces, copy=copy_faces)
        # Checks if node j is present in any faces
        def presentInFaces(j):
            for f in faces:
                if j in f: return True
            return False
        # map from old V inds to new ones, the list of new vertices, and the pruning counter
        old_to_new_node_map, new_verts, c = {}, [], 0
        # Loop over each vertex and decide whether to keep it
        new_vert_ind = 0
        for i in range(len(node_positions)):
            isThere = presentInFaces(i)
            # Old vertex ind = i
            if isThere:
                # New vertex is @ new_vert_ind
                old_to_new_node_map[i] = new_vert_ind
                new_vert_ind += 1
                # Store the new vertex if it is present in the faces
                new_verts.append(node_positions[i])
            else: c += 1
        if verbose: print('Generated verts map')
        # Now map each face index to its new position
        for i, f in enumerate(faces):
            for j, tc in enumerate(f):
                faces[i, j] = old_to_new_node_map[ tc ]
        if verbose: print('Pruned %d single points' % c)
        # Return results (either mesh or [V,F] as a tuple)
        if not return_as_mesh: return new_verts, faces
        if generatedClass is None: generatedClass = TriangleMesh
        return generatedClass(new_verts, faces, verbose=verbose)

    def computeFemLbo(self, spectrum_size=100, normalization=None, useEigh=True, verbose=False):
        '''
        Solves the Helmholtz Eigenproblem to get the linear FEM discretized LBO
        Returns [spectrum, eigenvectors] = [Lambda, Phi]
        Note that the eigenvectors are the columns: the eigenvalue w[i] has evec give by the column Phi[:,i].
        See: Reuter et al, Discrete Laplace-Beltrami Operators for Shape Analysis and Segmentation, 2009
        '''
        return fem_laplacian(self.vertices, self.faces, spectrum_size, normalization, useEigh, verbose)

    def cotanLaplacianOperatorMatrix(self, area_normalize):
        '''
        Gives the Laplacian of the mesh L, as a matrix operator defined on functions on the manifold vertices.

        Returns either:
        (1) the negative cotan weight matrix -W, if area_normalize is false
        (2) L = -S^{-1} W, where S is the diagonal node areas matrix
        '''
        from scipy.sparse import csc_matrix as cscm, lil_matrix as lilm
        if not self.has('cotan_weight'):
            self._computeCotanWeights(assign_to_self=True)
        W = self.cotan_weight
        if not area_normalize: return -W # No need to normalize by surface area
        S, n = np.diag(self.get_approximate_node_areas()), self.num_vertices
        S_inv = lilm((n, n))
        for i in range(n): S_inv[i,i] = 1.0 / S[i,i]
        S_inv = S_inv.tocsc()
        L = -S_inv.dot(W)
        return L

    def _computeCotanWeights(self, assign_to_self=True):
        '''
        Computes the cotan weight matrix of the current shape.
        Assigns self.cotan_weights if assign_to_self is true (default).
        '''
        if self.has('cotan_weight'): return self.cotan_weight
        nv = self.num_vertices
        from scipy.sparse import lil_matrix as lilm
        W = lilm((nv, nv))
        for edge_ind, edge in enumerate(self.edges):
            i, j = edge
            faces_around_edge = self.edge2face_map[edge_ind]
            is_boundary = (len(faces_around_edge) == 1)
            # Retrieve first angle
            target_face_ind_0 = faces_around_edge[0]
            face0 = self.faces[ target_face_ind_0 ]
            if   not face0[0] in edge: k = 0
            elif not face0[1] in edge: k = 1
            else:                      k = 2
            alpha_ij = self.face_angles[target_face_ind_0, k]
            # Boundary edges
            if is_boundary:
                W[i,j] = ( 1.0 / math.tan(alpha_ij) ) / 2.0 # 
                W[j,i] = W[i,j]
            # Interior edges
            else:
                # Retrieve second angle
                target_face_ind_1 = faces_around_edge[1]
                face1 = self.faces[ target_face_ind_1 ]
                if   not face1[0] in edge: k = 0
                elif not face1[1] in edge: k = 1
                else:                      k = 2
                beta_ij = self.face_angles[target_face_ind_1, k] # 
                # Fill in weight matrix
                W[i,j] = ( (1.0 / math.tan(alpha_ij)) + (1.0 / math.tan(beta_ij)) ) / 2.0
                W[j,i] = W[i,j]
        # Fill matrix diagonal: sum over each row (including the diag doesn't matter since it's zero)
        for cv in range(nv): W[cv, cv] = -1 * np.sum(W[cv,:])
        # Convert to a sparse format better suited for linalg operations
        W = W.tocsc()
        # Assign to self, if needed
        if assign_to_self: self.cotan_weight = W
        return W


    def computeCotanLboSpectrum(self, spectrum_size=100, verbose=True,
                                      dont_assign_to_shape=False,
                                      eigenvalues_only=False,
                                      allow_mat_perturbations=False,
                                      marq_lev_coef=0.0,
                                      node_areas_pert=0.0,
                                      max_iters=None,
                                      tol=0,
                                      normalize_by_lambda_1=False):
        if verbose: print('Computing discrete LBO via cotan weights')
        # from scipy.sparse import csr_matrix as csrm
        from scipy.sparse import csc_matrix as cscm #, lil_matrix as lilm
        from scipy.sparse.linalg import eigsh # Sparse-capable generalized eigenvalue solver
        from scipy.sparse import diags as ssdiags
        # Simple check to ensure relevant information is present
        if not self.has('face_areas'): # hasattr(self, 'face_areas'):
            print('Face information not present! Computing.')
            self.computeFaceInformation(verbose = verbose)
        ### Assemble S matrix
        nodeAreas = ssdiags( self.get_approximate_node_areas(store=not dont_assign_to_shape), offsets=0 )
        # if not dont_assign_to_shape:
        #     self.approximate_node_areas = np.diag(nodeAreas)
        S = cscm( nodeAreas )
        ### Assemble W matrix
        W = self._computeCotanWeights(assign_to_self = True)
        ### Damp the system for numerical stability
        if allow_mat_perturbations:
            # Note that S is a DIA matrix so this is fine but W is CSC, meaning
            # adding to the diagonal changes its sparsity structure (since some
            # of its diagonal entries could have unstored zeros previously)
            S.setdiag( S.diagonal() + node_areas_pert )
            W.setdiag( W.diagonal() + marq_lev_coef)
        ### Solve generalized eigenvalue problem
        if verbose: start_time = timer()
        if verbose: print('Solving generalized eigenvalue problem (for spectrum size %d)' % spectrum_size)
        # Solves: W Phi = S Phi Lambda, for Phi and Lambda
        # Returns:
        # (1) Array of k eigenvalues, and
        # (2) Array of k eigenvectors. v[:,i] is the evec of eigenvalue Lambda[i].
        Lambda, Phi = eigsh(
            W, # Sparse weight matrix
            k = spectrum_size, # Number of desired eigenvalues/eigenvectors
            M = S, # The generalized coefficient matrix for the eigenvalue problem
            sigma = 0, # Get eigenvalues near zero, using shift-invert mode
            which = 'LM', # Return the eigenvalues of smallest magnitude (in shift-invert mode)
            maxiter = max_iters,
            tol = tol
        )
        if verbose: print('\tSpectrum computation took %.2fs' % (timer() - start_time))
        ### Return results
        Lambda, Phi = np.sort(-Lambda), np.fliplr(Phi)
        if normalize_by_lambda_1:
            Lambda = Lambda / Lambda[1]
        if not dont_assign_to_shape:
            if verbose: print('\tStoring spectrum and eigenbasis in shape')
            self.Lambda = Lambda
            self.Phi = Phi
            # self.cotan_weight = W # equivalent to the (negative) unnormalized Laplacian matrix
        if eigenvalues_only: return Lambda
        return Lambda, Phi

    def _get_spectrum(self, Lambda=None, Phi=None, only_lambda=False, only_phi=False, verbose=True,
                      spectrum_size=100):
        '''
        Retrieves the LBO spectrum of the shape, computing it if necessary.
        If passed a spectrum (e.g. from a Schrodinger/Hamiltonian operator), it will simply return that.
        '''
        # If we are passing in a spectrum to use, just use it directly
        both_present = (not Lambda is None) and (not Phi is None)
        if both_present:
            if only_lambda: return Lambda
            if only_phi: return Phi
            return Lambda, Phi
        # If stored in the shape, assign it to the local variable
        if self.has('Lambda'): Lambda = self.Lambda
        if self.has('Phi'): Phi = self.Phi
        # If necessary, compute the LBO (default)
        both_none = (Lambda is None) and (Phi is None)
        if (only_lambda and Lambda is None) or (only_phi and Phi is None) or both_none:
            Lambda, Phi = self.computeCotanLboSpectrum(spectrum_size=spectrum_size, verbose=verbose)
        if only_lambda: return Lambda
        if only_phi: return Phi
        return Lambda, Phi

    def manifoldHarmonicTransformFunc(self, spatial_func, Phi=None, verbose=True,
                                      spectrum_size=1000, D=None):
        '''
        Computes the manifold harmonic transform of a function defined on a mesh via the LBO spectrum.
        '''
        Phi = self._get_spectrum(Phi=Phi, only_phi=True, spectrum_size=spectrum_size)
        if D is None: D = np.diag( self.get_approximate_node_areas() )
        return ( spatial_func * (np.dot(D, Phi)).T ).T.sum(axis=0)

    def inverseManifoldHarmonicTransformFunc(self, freq_func, Phi=None, verbose=True,
                                             spectrum_size=1000, freq_filter=None):
        '''
        Inverts the manifold harmonic transform applied to a function, i.e. converting it from frequency
        space back to the spatial domain.
        '''
        Phi = self._get_spectrum(Phi=Phi, only_phi=True, spectrum_size=spectrum_size)
        if freq_filter is None:
            return np.sum(freq_func * Phi, axis=1)
        else:
            return np.sum(freq_filter * freq_func * Phi, axis=1)

    def manifoldHarmonicTransform(self, Lambda=None, Phi=None, spectrum_size=1000,
                                  verbose=True, weight_matrix=None, store_high_freq_offsets=True):
        Lambda, Phi = self._get_spectrum(Lambda,Phi, spectrum_size=spectrum_size)
        if weight_matrix is None: weight_matrix = np.diag( self.get_approximate_node_areas() )
        # Transform the embedding function of the manifold (i.e. the coordinates)
        x_coords, y_coords, z_coords = self.vertices[:,0], self.vertices[:,1], self.vertices[:,2]
        if verbose: print('Performing manifold harmonic transform')
        projected_embedding = [ self.manifoldHarmonicTransformFunc(x_coords, Phi=Phi, D=weight_matrix),
                                self.manifoldHarmonicTransformFunc(y_coords, Phi=Phi, D=weight_matrix),
                                self.manifoldHarmonicTransformFunc(z_coords, Phi=Phi, D=weight_matrix) ]
        if store_high_freq_offsets:
            if verbose: print('\tStoring high frequency offsets')
            regen_manifold = self.generateManifoldFromHarmonicTransform(projected_embedding,
                                    verbose=False, use_full_triangle_mesh=False)
            new_v = regen_manifold.vertices
            self.hf_vertex_offsets = self.vertices - new_v
        return projected_embedding


    def generateManifoldFromHarmonicTransform(self, freq_coords, Lambda=None, Phi=None, verbose=True,
                                      spectrum_size=1000, use_full_triangle_mesh=True, new_name=None,
                                      freq_filter=None, use_hf_offsets=True):
        '''
        Applies the inverse manifold harmonic transform to frequency-space coordinate functions.
        E.g. after filtering. This gives us back the embedding function.

        use_hf_offsets: controls whether to use the high frequency offsets (if stored previously)
        freq_filter: a vector of length spectrum_size that serves as the filter function
                     (Can pass 'lpf' or 'hpf' to use the default low or high pass filters)

        See: Vallet and Levy, 2007
        '''
        Lambda, Phi = self._get_spectrum(Lambda, Phi, spectrum_size=spectrum_size)
        if verbose: print('Performing manifold harmonic transform')
        # Frequency filter defaults
        if   freq_filter == 'lpf': freq_filter = _default_lpf_vec(Phi.shape[1])
        elif freq_filter == 'hpf': freq_filter = _default_hpf_vec(Phi.shape[1])
        vx = self.inverseManifoldHarmonicTransformFunc(freq_coords[0], Phi=Phi, freq_filter=freq_filter)
        vy = self.inverseManifoldHarmonicTransformFunc(freq_coords[1], Phi=Phi, freq_filter=freq_filter)
        vz = self.inverseManifoldHarmonicTransformFunc(freq_coords[2], Phi=Phi, freq_filter=freq_filter)
        new_faces = self.faces
        n = self.num_vertices
        new_verts = np.zeros((n, 3))
        new_verts[:, 0] = vx
        new_verts[:, 1] = vy
        new_verts[:, 2] = vz
        # Vertex offsets
        if self.has('hf_vertex_offsets') and use_hf_offsets:
            if freq_filter is None:
                new_verts += self.hf_vertex_offsets
            else:
                # This case is slightly complicated. We need to assign a value to the filter on the spectrum
                # values larger (i.e. frequencies smaller) than that covered by the spectral basis, in order
                # to apply it to the high frequency offsets. Let omega_m=sqrt(lambda_m) be the largest
                # eigenvalue in the spectrum that we compute. The true maximum frequency of the mesh is
                # omega_M, with omega_m < omega_M. We then need to compute:
                #    f_{HF} = 1/(omega_M - omega_m) int_{omega_m}^{omega_M} F(omega) domega
                # which is the average filter value on this high-frequency part of the spectrum.
                # I simply take this to be the last value of the filter (i.e. the one assigned to the largest
                # spectral value). Since it is constant, so too is f_{HF}.
                filtered_offset = freq_filter[-1]
                new_verts += filtered_offset * self.hf_vertex_offsets
        new_class = FullTriangleMesh if use_full_triangle_mesh else TriangleMesh
        if new_name is None: new_name = self.name + 'invMHT' # default name
        if verbose: print('\tConstructing new mesh', new_name)
        return new_class(new_verts, new_faces, name=new_name)

    def GPS_embedding(self, Lambda=None, Phi=None, targets=None, spectrum_size=50):
        '''
        Following Rustamov, 2007, we compute the global point signature across the manifold.
        The result is a |V| x |S| matrix, the GPS embedding of the shape.
        '''
        Lambda, Phi = self._get_spectrum(Lambda, Phi, spectrum_size=spectrum_size)
        eval_coefs = 1.0 / np.sqrt(Lambda[1:])
        return Phi[:, 1:] * eval_coefs

    def clusterEmbedding(self, embedding, clustering_options=['kmeans', 8, 3], verbose=True):
        '''
        Clusters a set of embedded points space across the embedded shape.
        E.g. can be the truncated GPS embedded form of the shape.

        Returns a |V| x 1 vector of integers with the label of the cluster for each vertex.

        Clustering Options:
            ['kmeans', num_clusters, num_reinitializations]
            ['ward', num_clusters]
            ['dbscan', epsilon, min_samples]
        '''
        if verbose: print('Clustering embedding (algo: %s)' % clustering_options[0])
        if clustering_options[0] == 'kmeans':
            from sklearn.cluster import KMeans
            nc, nri = clustering_options[1], clustering_options[2]
            res = KMeans(n_clusters=nc, n_init=nri, random_state=9713).fit(embedding)
        elif clustering_options[0] == 'ward':
            from sklearn.cluster import AgglomerativeClustering as Aggc
            res = Aggc(n_clusters=clustering_options[1]).fit(embedding)
        elif clustering_options[0] == 'dbscan':
            from sklearn.cluster import DBSCAN
            eps, ms = clustering_options[1], clustering_options[2]
            res = DBSCAN(eps=eps, min_samples=ms).fit(embedding)
        else:
            print('Unknown option', clustering_options[0])
            return None
        return res.labels_

    def readFromFile(targetFile, copy=False, print_info=True, verbose=True):
        return _readFromFile(targetFile, TriangleMesh, copy=copy, print_info=print_info, verbose=verbose)

    def writeAsPickle(self, outputFileName):
        if not '.' in outputFileName: outputFileName += self.PCKL_EXT
        with open(outputFileName, 'wb') as phandle:
            pickle.dump(self, phandle)

    # Note: be careful with modifications, since the kd-tree will still be associated to the
    # mesh object (not just the point cloud one). Not sure how this affects garbage collection.
    def asPointCloud(self, copy=True, pass_kdtree=True):
        tree = self.node_tree if pass_kdtree else None
        return PointCloud(self.vertices, copy=copy, precomputed_kdtree=tree)

    def generateAreaWeightedRandomPointCloud(self, num_points, verbose=True, return_as_point_cloud=True):
        '''
        Generates a point cloud from the current mesh by randomly sampling from this mesh.
        Performs uniform sampling over the manifold by (1) choosing a face based on its area and
        (2) using barycentric coordinates to randomly sample from the triangle face.
        '''
        if verbose: print('Generating area-weighted point cloud (%d points)' % num_points)
        # Weighted sample from triangles
        probs = self.face_areas / np.sum(self.face_areas)
        face_inds = npr.choice(self.num_faces, size=num_points, p=probs)
        # Uniform sampling from each triangle via barycentric coordinates
        bary_samples = npr.uniform(low=0.0, high=1.0, size=(num_points, 2))
        # Generate actual coordinates
        new_points = np.zeros((num_points, 3))
        for i in range(num_points):
            u1, u2 = bary_samples[i,:]
            su1 = np.sqrt(u1)
            v1, v2, v3 = self.vertices[self.faces[ face_inds[i] ], :]
            new_points[i] = (1-su1)*v1 + (su1*(1-u2))*v2 + (u2*su1)*v3
        if return_as_point_cloud: return PointCloud(new_points)
        else:                     return new_points

    def generateAreaWeightedRandomPointCloudWithNormals(self, num_points, verbose=True):
        '''
        Generates a point cloud from the current mesh by randomly sampling from this mesh.
        Performs uniform sampling over the manifold by (1) choosing a face based on its area and
        (2) using barycentric coordinates to randomly sample from the triangle face.
        Also estimates unit normals for each sampled point.
        Returns two num_points x 3 arrays (i.e. [xi, yi, zi], [nx, ny, nz])
        '''
        if not self.has('face_normals'):
            if verbose: print('Face normals required to be present. Computing.')
            self.computeFaceInformation(verbose=verbose, dont_correct=False, correct_winding=False)
            # FullTriangleMesh.computeFaceInfoAndVertexNormals(self)
        if verbose: print('Generating area-weighted point cloud (%d points) with normals' % num_points)
        # Weighted sample from triangles
        probs = self.face_areas / np.sum(self.face_areas)
        face_inds = npr.choice(self.num_faces, size=num_points, p=probs)
        # Obtain the normals
        new_normals = np.zeros((num_points, 3))
        for i, fi in enumerate(face_inds): new_normals[i] = self.face_normals[fi]
        # Uniform sampling from each triangle via barycentric coordinates
        bary_samples = npr.uniform(low=0.0, high=1.0, size=(num_points, 2))
        # Generate actual coordinates
        new_points = np.zeros((num_points, 3))
        for i in range(num_points):
            u1, u2 = bary_samples[i,:]
            su1 = np.sqrt(u1)
            v1, v2, v3 = self.vertices[self.faces[ face_inds[i] ], :]
            new_points[i] = (1-su1)*v1 + (su1*(1-u2))*v2 + (u2*su1)*v3
        return new_points, new_normals

    def sampleNodesFromDistribution(self, probdist, num_nodes, with_replacement=False):
        '''
        Samples num_nodes integers from the mesh, using probdist as the discrete density over the nodes.
        with_replacement: whether to sample with or without replacement.
        '''
        # Ascertain input is a valid probdist
        assert (probdist >= 0.0).all(), "Probdist must be non-negative"
        # Normalize
        probdist /= probdist.sum()
        # Sample node indices without replacement
        vs = npr.choice(self.num_vertices, size=num_nodes, replace=with_replacement, p=probdist)
        return vs

    def sampleNodesWeightedByArea(self, num_nodes, with_replacement=False):
        '''
        Samples uniformly from the shape, based on the approximate Voronoi area associated to the node.
        '''
        area_weights = self.get_approximate_node_areas()
        return self.sampleNodesFromDistribution(area_weights, num_nodes, with_replacement=with_replacement)

    def generateSubmeshFromNodes(self, node_indices_to_keep, prune_single_points=True,
                                 keep_all_associated_triangles=True, verbose=True, return_as_mesh=True):
        V, F = self.vertices, self.faces
        if keep_all_associated_triangles:
            # Keep all triangles with at least one point being kept
            finds = []
            for fi, f in enumerate(self.faces):
                for tc in f:
                    if tc in node_indices_to_keep:
                        finds.append(fi)
                        break
            new_V, new_F = self.generateSubmeshFromTriangles(finds, return_as_mesh=False, verbose=verbose)
        else:
            # Keep only triangles with all corners being kept
            finds = []
            for fi, f in enumerate(self.faces):
                for tc in f:
                    if (        tc[0] in node_indices_to_keep
                            and tc[1] in node_indices_to_keep
                            and tc[2] in node_indices_to_keep   ):
                        finds.append(fi)
            new_V, new_F = self.generateSubmeshFromTriangles(finds, return_as_mesh=False, verbose=verbose)
        # Generate a new mesh or return the lists (pruning single points if desired)
        if prune_single_points:
            return TriangleMesh._pruneSinglePoints(new_V,
                                                   new_F,
                                                   verbose=verbose,
                                                   generatedClass=type(self),
                                                   return_as_mesh=return_as_mesh)
        elif return_as_mesh:
            class_to_use = type(self) # use the type of the caller to generate the mesh
            return class_to_use(new_V, new_F, verbose=verbose)
        else: return new_V, new_F

    def generateSubmeshFromTriangles(self, triangle_indices_to_keep, verbose=True, return_as_mesh=True):
        V, F = self.vertices, self.faces
        new_F_uncorr = [] # uncorrected node indices here
        # Grab the faces corresponding to the target indices of faces to keep
        for fi in triangle_indices_to_keep: new_F_uncorr.append( F[fi] )
        # new_F_uncorr now has all triangles that we want to keep
        new_V_inds = set()
        for f in new_F_uncorr: new_V_inds.update(f)
        new_V_inds = list(new_V_inds)
        # new_V_inds now has every vertex involved in new_F_uncorr
        old_vert_to_new_vert_ind_map = {}
        for i, v_ind in enumerate(new_V_inds):
            old_vert_to_new_vert_ind_map[v_ind] = i
        # Fix the vertices of the faces so that the indices match
        if verbose: print('Remapping face indices')
        pof = lambda f: [old_vert_to_new_vert_ind_map[tc] for tc in f] # process old vertex
        new_F = [ pof(f) for f in new_F_uncorr ]
        # Grab the actual vertices
        new_V = V[new_V_inds, :]
        if return_as_mesh: return TriangleMesh(new_V, new_F, verbose=verbose)
        return new_V, new_F

    def deleteTriangles(self, face_indices_to_destroy, verbose=True, return_as_mesh=True):
        if verbose: print("Deleting %d faces" % len(face_indices_to_destroy))
        to_keep = np.array(list( set(list(range( self.num_faces ))) - set(face_indices_to_destroy) ))
        new_F = np.array(self.faces[to_keep,:], copy=True)
        if verbose: print('Starting pruning')
        return TriangleMesh._pruneSinglePoints(self.vertices,
                                               new_F,
                                               verbose=verbose,
                                               return_as_mesh=return_as_mesh,
                                               copy_faces=False)

    def extractLargestConnectedComponent(self, verbose=True, prune_single_points=True,
                                 keep_all_associated_triangles=True, return_as_mesh=True):
        if verbose: print('Extracting largest connected components')
        n_components, labels, adjmat = self.connectedComponents()
        counter = Counter(labels)
        if verbose: print('Components:', ", ".join([ "%d -> %d" % (k,counter[k]) for k in counter.keys() ]) )
        key, count = counter.most_common(1)[0]
        node_indices_to_keep = [ i for i in range(self.num_vertices) if labels[i] == key ]
        return self.generateSubmeshFromNodes(
                        node_indices_to_keep,
                        prune_single_points=prune_single_points,
                        keep_all_associated_triangles=keep_all_associated_triangles,
                        return_as_mesh=return_as_mesh,
                        verbose=verbose)

    def generateIcosahedron(class_to_use=None):
        # Adapted From: http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
        t = (1.0 + math.sqrt(5.0)) / 2.0;
        points = np.array([
            [-1,  t,  0], [1,  t,  0], [-1, -t,  0], [1, -t,  0], [0, -1,  t],  [0,  1,  t],
            [0, -1, -t],  [0,  1, -t], [t,  0, -1],  [t,  0,  1], [-t,  0, -1], [-t,  0,  1]
        ])
        faces = np.array([
            [0, 11, 5],  [0, 5, 1],  [0, 1, 7],  [0, 7, 10], [0, 10, 11], [1, 5, 9],  [5, 11, 4],
            [11, 10, 2], [10, 7, 6], [7, 1, 8],  [3, 9, 4],  [3, 4, 2],   [3, 2, 6],  [3, 6, 8],
            [3, 8, 9],   [4, 9, 5],  [2, 4, 11], [6, 2, 10], [8, 6, 7],   [9, 8, 1]
        ])
        if class_to_use is None: class_to_use = FullTriangleMesh
        icosahedronMesh = class_to_use( points, faces, name='icosahedron' )
        return icosahedronMesh

    def generateIcoSphere(n_subdivs=4, subdivType=2, noisify=0.0, correct_winding=True):
        '''
        Simply subdivide an icosahedron n times, then correct the vertices to enforce sphericity
        '''
        icosahedron = FullTriangleMesh.generateIcosahedron()
        for i in range(n_subdivs):
            if subdivType == 1: v,f = icosahedron.subdivideTriangles_nodelink()
            else: v,f = icosahedron.subdivideTriangles_edgesplit()
            icosahedron = FullTriangleMesh(v, f, name='icosphere', correct_winding=correct_winding)
        if noisify > icosahedron.epsilon:
            icosahedron = icosahedron.addGaussianNoiseToVertices(sigma=noisify)
        #for vertex in icosahedron.vertices: vertex /= LA.norm(vertex) # removes normal-aligned noise
        # Force sphericity (removes normal-aligned noise)
        normalizer = LA.norm(icosahedron.vertices, axis=1)
        vertices = icosahedron.vertices / normalizer[:, np.newaxis]
        icosahedron = FullTriangleMesh(vertices, icosahedron.faces)
        return icosahedron

    def addGaussianNoiseToVertices(self, sigma=None, correct_winding=False):
        if sigma is None: sigma = self.getAverageEdgeLength() / 10.0
        V = self.vertices + np.random.normal(scale=sigma, size=(self.num_vertices, 3))
        return FullTriangleMesh(V, self.faces, name=self.name+'-noised', correct_winding=correct_winding)

    # w = uniform noise interval width
    def addUniformNoiseToVertices(self, w=0.2, correct_winding=False):
        V = self.vertices + np.random.uniform(low=-1*w/2.0, high=w/2.0, size=(self.num_vertices, 3))
        return FullTriangleMesh(V, self.faces, name=self.name+'-noised', correct_winding=correct_winding)

class FullTriangleMesh(TriangleMesh):
    '''
    Represents a triangle mesh, with many useful properties precomputed
    '''

    # Input Data: points and triangle faces
    def __init__(self, verticesList, trianglesList, copy=True, verbose=True, name=None,
                 correct_normals=True, correct_winding=False, keep_only_largest_connected_component=True,
                 vertex_normals=None, print_info=False):
        if verbose: start = timer()

        # Call the super class constructor of the regular TriangleMesh class
        super(FullTriangleMesh, self).__init__(verticesList, trianglesList, copy, verbose, name)

        ### Now onto the "extra" computations ###
        if not vertex_normals is None:
            # First, assign the vertex normals directly
            self.vertex_normals = vertex_normals / LA.norm(vertex_normals, axis=-1)[:, np.newaxis]
            # Second, compute the face information, without the face normals
            self.computeFaceInformation(verbose=verbose, ignore_face_normals=True)
            # Third, compute the face normals from the vertex normals
            self.computeFaceNormalsFromVertexNormals(verbose=verbose)
        else: # when vertex_normals are not given, compute them
            FullTriangleMesh.computeFaceInfoAndVertexNormals(self,
                                                             verbose=verbose,
                                                             correct_normals=correct_normals,
                                                             correct_winding=correct_winding)

        if verbose: print('\tFinished in %.2fs' % (timer() - start))
        if print_info: self.print_info()

    def readFromFile(targetFile, copy=False, name=None, correct_normals=True, print_info=True, verbose=True):
        return _readFromFile(targetFile, FullTriangleMesh, copy=copy, name=name, verbose=verbose,
                             correct_normals=correct_normals, print_info=print_info)

    def computeFaceInfoAndVertexNormals(triangle_mesh, verbose=True, correct_normals=True,
                                        correct_winding=False):
        '''
        Upgrade from a TriangleMesh to a FullTriangleMesh
        '''
        # Get face angles, normals, centers, and areas
        triangle_mesh.computeFaceInformation(verbose=verbose,
                                    dont_correct=(not correct_normals),
                                    correct_winding=correct_winding)
        # Computing vertex normals
        triangle_mesh.computeVertexNormals(verbose=verbose)

#***********************************************************************************************************#

##### Miscellaneous #####

class Queue(object):
 def __init__(self, starting_vals=None):
    self.items = []
    if not starting_vals is None: self.items += starting_vals
 def isEmpty(self): return self.items==[]
 def enqueue(self,item): self.items.insert(0,item)
 def dequeue(self): return self.items.pop()
 def size(self): return (len(self.items))
 def pop(self): return self.items.pop()
 def add(self,item): return self.items.insert(0,item)
 def hasItems(self): return not self.isEmpty()
 def enqueue_all(self,itlist):
    for item in itlist: self.enqueue(item)


def _readFromFile(targetFile, input_class, copy=False, name=None, correct_normals=True,
                  print_info=True, verbose=True):
    """
    Possible inputs:
        (1) Pass a .off, .obj, .vert, .tri, or .ply file
        (2) Pass a prefix where a {prefix}.tri and {prefix}.vert exist
        (3) Pass a pickle file containing a input_class object
    Output: a TriangleMesh corresponding to the input
    """
    # Pickle case
    if targetFile.endswith('.pickle') or targetFile.endswith(input_class.PCKL_EXT):
        with open(targetFile, 'rb') as tpickle:
            return pickle.load(tpickle)

    v, f, vn = None, None, None

    # File case
    ext = targetFile.split('.')[-1]
    from ..io.tmesh_io import readOffFile, readTrianglesPlusVerticesMeshFiles, readPlyFile, readObjFile
    if ext == 'off':
        v, f = readOffFile(targetFile)
    if ext == 'ply':
        v, f = readPlyFile(targetFile)
    if ext == 'obj':
        v, f, vn = readObjFile(targetFile, verbose=verbose)
    if ext == 'tri':
        v, f = readTrianglesPlusVerticesMeshFiles(targetFile, targetFile[:-4] + '.vert')
    if ext == 'vert':
        v, f = readTrianglesPlusVerticesMeshFiles(targetFile[:-5] + '.tri', targetFile)
    if os.path.exists(targetFile + '.tri') and os.path.exists(targetFile + '.vert'):
        v, f = readTrianglesPlusVerticesMeshFiles(targetFile + '.tri', targetFile + '.vert')

    if v is None or f is None:
        print('Target', targetFile, 'was not found')
        sys.exit(1)

    # Default mesh name is the filename
    if name is None: name = targetFile

    if (input_class is FullTriangleMesh) and (not vn is None):
        # Here normals correction does not matter
        return input_class(v, f, copy=copy, name=name, vertex_normals=vn,
                           print_info=print_info, verbose=verbose)
    if (input_class is FullTriangleMesh):
        return input_class(v, f, copy=copy, name=name, correct_normals=correct_normals,
                           print_info=print_info, verbose=verbose)
    else:
        return input_class(v, f, copy=copy, name=name, verbose=verbose)

    # If we made it here, there was a problem
    print('Error processing input', targetFile, '\nOnly handles ply, off, obj, or tri+vert.')
    return None

# Returns a function that computes a generalized logistic with the input parameters
def _generalized_logistic_function(A,K,C,Q,B,v,M):
    return lambda t: A + (K - A) / (( C + Q * np.exp(-B*(t-M)))**(1.0/v) )

# def _default_low_pass_filter(lpf_slope=9.0,offset=0.5):
#     return lambda t: 1.0 - 1.0 / ( 1.0 + np.exp(-lpf_slope*(t-offset)) )

def _default_enhancement_filter(upper=3.0, inc_start=0.25):
    m = (1.0 - upper) / (inc_start - 1.0)
    b = upper - m
    def hp(t):
        a = np.array(t,copy=True)
        a[ a < inc_start  ] = 1.0
        a[ a >= inc_start ] = m*a[ a >= inc_start ] + b
        return a
    return hp

def _default_lpf_vec(nvals):
    return sloped_step_function_asVector(1.0, 0.5, 0.3, 0.9, nvals)

def _default_hpf_vec(nvals):
    return sloped_step_function_asVector(1.0, 3.0, 0.3, 0.9, nvals)

#------------------------#
if __name__ == '__main__':
    mesh = FullTriangleMesh.readFromFile('temp/cat0')
    mesh.display()

#
