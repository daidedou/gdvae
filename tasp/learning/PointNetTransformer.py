####################################################################################
# Implementation of PointNet in PyTorch
# Taken from: meder411
# Accessible at: https://github.com/meder411/PointNet-PyTorch
#
# Modified by TTAA
####################################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as grad

##-----------------------------------------------------------------------------
# Class for Transformer. Subclasses PyTorch's own "nn" module
#
# Computes a KxK affine transform from the input data to transform inputs
# to a "canonical view"
##
class Transformer(nn.Module):

    def __init__(self,
                 num_points,
                 K=3,
                 device=None,
                 block1_size=64,
                 block2_size=128,
                 block3_size=1024,
                 MLP_sizes=(512,256)):

        # Call the super constructor
        super(Transformer, self).__init__()

        # Number of dimensions of the data
        self.K = K

        # Size of input
        self.N = num_points

        # Initialize identity matrix on the GPU (do this here so it only
        # happens once)
        if not device is None:
            self.device = device
            self.identity = grad.Variable(
                                torch.eye(self.K).float().view(-1).to(device)
                            )
        else:
            self.identity = grad.Variable( torch.eye(self.K).float().view(-1) )

        # First embedding block
        self.block1 = nn.Sequential(
            nn.Conv1d(K, block1_size, 1),
            nn.BatchNorm1d(block1_size),
            nn.ReLU())

        # Second embedding block
        self.block2 = nn.Sequential(
            nn.Conv1d(block1_size, block2_size, 1),
            nn.BatchNorm1d(block2_size),
            nn.ReLU())

        # Third embedding block
        self.block3 = nn.Sequential(
            nn.Conv1d(block2_size, block3_size, 1),
            nn.BatchNorm1d(block3_size),
            nn.ReLU())

        # Multilayer perceptron
        self.mlp = nn.Sequential(
            nn.Linear(block3_size, MLP_sizes[0]),
            nn.BatchNorm1d(MLP_sizes[0]),
            nn.ReLU(),
            nn.Linear(MLP_sizes[0], MLP_sizes[1]),
            nn.BatchNorm1d(MLP_sizes[1]),
            nn.ReLU(),
            nn.Linear(MLP_sizes[1], K * K))


    # Take as input a B x K x N matrix of B batches of N points with K
    # dimensions
    def forward(self, x):

        # Compute the feature extractions
        # Output should ultimately be B x 1024 x N
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)

        # print('xs',x.shape)
        # print('N', self.N) <-- the problem, must be correctly set!

        # Pool over the number of points
        # Output should be B x 1024 x 1 --> B x 1024 (after squeeze)
        x = F.max_pool1d(x, self.N).squeeze(2)

        # Run the pooled features through the multi-layer perceptron
        # Output should be B x K^2
        x = self.mlp(x)

        # Add identity matrix to transform
        # Output is still B x K^2 (broadcasting takes care of batch dimension)
        x += self.identity

        # Reshape the output into B x K x K affine transformation matrices
        x = x.view(-1, self.K, self.K)

        return x
