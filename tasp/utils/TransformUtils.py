
# TTAA

import numpy as np 
import numpy.linalg as LA 
import numpy.random as npr 

def random3DRotationMatrix_QR():
    '''
    Generates a random unitary matrix via QR decomposition of a normally distributed random matrix.
    Not clear if this is a uniformly distributed rotation though.
    Note it might cause reflections.
    '''
    init_matrix = npr.normal(loc=0.0, scale=1.0, size=(3,3))
    Q,R = LA.qr(init_matrix, mode='complete') # Q orthonormal, R upper-triangular
    return Q

def random3DRotationMatrix_AxisComposed():
    '''
    Generates a random rotation matrix by composing three axis-aligned rotations.
    This one does not appear to be uniformly distributed (over the space of rotations).
    (See Miles, 1965, "On Random Rotations in R^3")
    '''
    a,b,c = npr.uniform(low=0.0, high=2*np.pi, size=3)
    Rx = np.array([ [1,          0,          0], 
                    [0,          np.cos(a),  -np.sin(a)], 
                    [0,          np.sin(a),  np.cos(a)]  ])
    Ry = np.array([ [np.cos(b),  0, np.sin(b) ],
                    [0,          1,          0],
                    [-np.sin(b), 0,          np.cos(b)]  ])
    Rz = np.array([ [np.cos(c),  -np.sin(c), 0],
                    [np.sin(c),  np.cos(c),  0],
                    [0,          0,          1]          ])
    return np.dot(Rx, np.dot(Ry, Rz))


def random3DRotationMatrix_unitary():
    '''
    Similar to QR, we generate 3 orthonormal vectors and form a rotation matrix with them.
    Should be ~uniformly distributed over the space of rotations.
    '''
    v1 = npr.normal(size=(3,1))
    v2 = npr.normal(size=(3,1))
    v1 /= LA.norm(v1)
    v2 /= LA.norm(v2)
    while v1.T.dot(v2) > 0.99: # Numerical stability
        v2 = npr.normal(size=(3,1))
        v2 /= LA.norm(v2)
    # Orthogonalize
    v2 = v2 - np.dot(v1.T, v2)*v1
    v2 /= LA.norm(v2) # v1, v2 now orthonormal
    # Generate final axis
    v3 = np.cross(v1, v2, axis=0)
    v3 /= LA.norm(v3)
    # Build matrix
    rot = np.concatenate((v1, v2, v3), axis=1)
    # Remove reflections
    if LA.det(rot) < 0:
        rot *= -1.0
    return rot




