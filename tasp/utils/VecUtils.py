
import numpy as np 
import numpy.linalg as LA 
import numpy.random as npr 

_EPS = 0.0000001

def _clipped_arccos(a):
    # Returns in [0,pi]
    return np.arccos( np.clip(a, -1.0 + _EPS, 1.0 - _EPS) )

def angle_between(v1, v2):
    cofa = np.dot(v1, v2) / (LA.norm(v1) * LA.norm(v2))
    return _clipped_arccos(cofa)

def angle_between_normed(v1, v2):
    '''
    Angle between two vectors, assuming both inputs are normalized to length 1
    '''
    return _clipped_arccos( np.dot(v1,v2) )

def closest_angle_between(v1, v2):
    d1 = np.dot(v1, v2) / (LA.norm(v1) * LA.norm(v2))
    return min( _clipped_arccos(d1), _clipped_arccos(-d1) )

def norm_squared(v):
    return np.inner(v, v)

def normVec(v):
    return v / LA.norm(v)

def normalize_set(vecs):
    '''
    Normalizes a set of m vectors (as a list or matrix) by the listwide
    minimum and maximum.
    '''
    vecs = np.array(vecs)
    low, high = np.min(vecs), np.max(vecs)
    return (vecs - low) / (high - low)

def average_L2_dist_between_vector_sets(U, V, squared_distance=False):
    '''
    Each U, V is a list (or matrix) of vectors.
    Each row is a separate vector. (Careful the inputs should be not be transposed).
    '''
    U, V = np.array(U), np.array(V)
    num_vecs_u, vec_dim_u = U.shape
    num_vecs_v, vec_dim_v = V.shape
    assert vec_dim_v == vec_dim_u, "Must compute pairwise distance between vectors of the same size"
    vec_dim = vec_dim_u
    total_vec_num = num_vecs_v * num_vecs_u
    # Repeats each row, beside each other
    dup_U = np.repeat(U, num_vecs_v, axis=0)
    # Repeats the full matrix
    dup_V = np.broadcast_to(V, [num_vecs_u, num_vecs_v, vec_dim]).reshape((total_vec_num, vec_dim))
    # Difference between all the vectors pairwise
    diff = dup_U - dup_V
    # Norm each row (i.e. between each pair of vectors)
    if squared_distance: dists = (diff**2).sum(axis=1)
    else:                dists = LA.norm(diff, axis=1)    
    # Take the average of all the vector distances
    return np.sum(dists) / total_vec_num


