
import numpy as np 
import numpy.linalg as LA 
import numpy.random as npr 


def remove_outliers(v, thresh_percentile):
    '''
    Return a new array with all outliers outside of the thresh percentile removed.
    E.g. passing in thresh_percentile = 0.05 will remove the top 5% and bottom 5% of the data.
    In other words, 2 * thresh_percentile percent of the data is removed in total.
    '''
    assert thresh_percentile > 0.0 and thresh_percentile < 0.5, 'Must have thresh in (0,1)'
    p_upper = np.percentile(v, 100 - (100 * thresh_percentile))
    p_lower = np.percentile(v, 100 * thresh_percentile)
    return np.array([ s for s in v if s <= p_upper and s >= p_lower ])

def truncate_outliers(v, thresh_percentile):
    '''
    Return a new array with all outliers outside of the thresh percentile replaced with .
    E.g. passing in thresh_percentile = 0.05 will alter the top 5% and bottom 5% of the data.
    The bottom/top data values are replaced with the bottom/top thresh_percentile values, resp. 
    '''
    assert thresh_percentile > 0.0 and thresh_percentile < 0.5, 'Must have thresh in (0,1)'
    p_upper = np.percentile(v, 100 - (100 * thresh_percentile))
    p_lower = np.percentile(v, 100 * thresh_percentile)
    return np.clip(v, p_lower, p_upper)

def truncate_outliers_inplace(v, thresh_percentile):
    '''
    Same as truncate outliers but in-place.
    '''
    assert thresh_percentile > 0.0 and thresh_percentile < 0.5, 'Must have thresh in (0,1)'
    p_upper = np.percentile(v, 100 - (100 * thresh_percentile))
    p_lower = np.percentile(v, 100 * thresh_percentile)
    clip = lambda s: p_lower if s < p_lower else (p_upper if s > p_upper else s)
    for i in range(len(v)): v[i] = clip(v[i])

