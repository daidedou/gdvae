import os, sys, numpy as np, gc, time
from collections import Counter
from tasp import FullTriangleMesh
from tasp.structures.PointCloud import PointCloud
from tasp.projects.IEVAE import DataHandling as DH
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec, run_training
from tasp.learning.Utils import convert_greyscale_image_to_mesh, partial_chamfer_over_sets
import torch
from torch.autograd import Variable
import numpy.random as npr
from torch.nn import functional as F

### Settings ###
#--------------#

### DATASET SETTINGS ###
DATASETS_LOCATION = 'data'
DATASETS = { 'mnist'    : { 'train' : 'mnist-combined-training-tasp_meshes.pickle',
                            'test'  : 'mnist-combined-test-tasp_meshes.pickle' },
           }

### TRAINING SETTINGS ###
# N_LOSS_SAMPLES not used
settings = {
    'smal2' : {
        'CONV_LAYER_SIZES' : [ 50, 100, 200, 400 ],
        'TRANSFORMER_POS'  : [ 0, 2 ],
        'CONV_DIM'         : 500,
        'FC_LAYERS'        : [ 800 ],
        'LATENT_DIM'       : 400,
        'DECODER_SIZES'    : [ 800, 800, 2000 ],
        'INPUT_NP'         : 1500,
        'OUTPUT_NP'        : 1500,
        'B'                : 30,
        'LEARNING_RATE'    : 0.00025,
        'N_EPOCHS'         : 500,
        'CHAMW'            : 20.0,
        'HAUSSW'           : 0.1,
        'MAX_AUG_ROT'      : np.pi / 6,
        #
        'N_LOSS_SAMPLES'   : 0, #1600, #
        'N_RC_SAMPLES'     : 0,
        'RC_COEF'          : 0,
        'ROTATION_TYPE'    : 'z',
        'USE_RC'           : False,
        'DECNORM'          : 'layer' # None
    },
    'sursmpl2' : {
        'CONV_LAYER_SIZES' : [ 50, 100, 200, 400 ],
        'TRANSFORMER_POS'  : [ 0, 2 ],
        'CONV_DIM'         : 500,
        'FC_LAYERS'        : [ 500 ],
        'LATENT_DIM'       : 300,
        'DECODER_SIZES'    : [ 500, 800, 2000 ],
        'INPUT_NP'         : 2000,
        'OUTPUT_NP'        : 2000,
        'B'                : 20,
        'LEARNING_RATE'    : 0.00025,
        'N_EPOCHS'         : 100,
        'CHAMW'            : 20.0,
        'HAUSSW'           : 0.1,
        'ROTATION_TYPE'    : 'y',
        'MAX_AUG_ROT'      : np.pi / 6,
        #
        'N_LOSS_SAMPLES'   : 0, #1600, # 
        'N_RC_SAMPLES'     : 0,
        'RC_COEF'          : 0.00,
        'USE_RC'           : False,
        'DECNORM'          : None # 'layer' # None
    },
    'mnist' : {
        'CONV_LAYER_SIZES' : [ 50, 100, 200, 300 ],
        'TRANSFORMER_POS'  : [ 0, 2 ],
        'CONV_DIM'         : 400,
        'FC_LAYERS'        : [ 300 ],
        'LATENT_DIM'       : 250,
        'DECODER_SIZES'    : [ 300, 500, 800 ],
        'INPUT_NP'         : 1000,
        'OUTPUT_NP'        : 1000,
        'N_EPOCHS'         : 100,
        'B'                : 36,
        'LEARNING_RATE'    : 0.00005,
        'CHAMW'            : 10.0,
        'HAUSSW'           : 1.00,
        #
        'N_LOSS_SAMPLES'   : 1000, #
        'N_RC_SAMPLES'     : 30,
        'RC_COEF'          : 5.0,
        'ROTATION_TYPE'    : 'z',
        'USE_RC'           : False, # True
        'DECNORM'          : None # 'layer' # None
    },
    'dyna' : {
        'CONV_LAYER_SIZES' : [ 50, 100, 200, 300 ],
        'TRANSFORMER_POS'  : [ 0, 2 ],
        'CONV_DIM'         : 400,
        'FC_LAYERS'        : [ 300 ],
        'LATENT_DIM'       : 250,
        'DECODER_SIZES'    : [ 300, 500, 1000 ],
        'INPUT_NP'         : 2000,
        'OUTPUT_NP'        : 2000,
        'B'                : 32,
        'LEARNING_RATE'    : 0.00005,
        'N_EPOCHS'         : 200,
        'CHAMW'            : 10.0,
        'HAUSSW'           : 1.0,
        'N_LOSS_SAMPLES'   : 0,
        'N_RC_SAMPLES'     : 20,
        'RC_COEF'          : 1.00,
        'ROTATION_TYPE'    : 'y',
        'USE_RC'           : False,
        'DECNORM'          : None # 'layer' # None
    },
}
    

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

def main_print(model_name):
    print('Loading model', model_name)
    model = Autoencoder_IEVAE_fixedDec.load(model_name, mode='eval')
    print('Architecture')
    print(model)
    print('Training Parameters')
    print(model.training_params)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

def main_train(key, device_manual):

    assert key in settings.keys(), "Key not present in settings. Use one of" + str(settings.keys())

    S = settings[key]
    
    ###############################
    USE_DP = False # dataparallel
    ###############################

    #-- Loss Function Constants --#
    A_1 = 0.75
    A_2 = 0.5
    CHAM_W = S['CHAMW']  
    HAUSS_W = S['HAUSSW']
    #-----------------------------#
    
    #################################################################
    ### AE Settings ###
    # > Encoder
    CONV_LAYER_SIZES = S['CONV_LAYER_SIZES']  # [50, 100, 200, 300]
    TRANSFORMER_POSITIONS = S['TRANSFORMER_POS']  #[0, 2]
    CONV_DIM = S['CONV_DIM']
    FC_LAYER_SIZES = S['FC_LAYERS'] # [300, 300]
    LATENT_DIM = S['LATENT_DIM']  #300
    # > Decoder
    DEC_SIZES = S['DECODER_SIZES'] # [300, 500, 1200]
    INPUT_SIZE = S['INPUT_NP']
    OUTPUT_SIZE = S['OUTPUT_NP']
    USE_GPU = True
    DECNORM = S['DECNORM']
    ### Training Settings ###
    #-- Algo Settings --#
    dataset_loc = DATASETS_LOCATION #'Datasets'
    dataset_prefix = DATASETS[key]['train']
    dataset_suffix = 'pickle'
    N_EPOCHS = S['N_EPOCHS'] # 100 if 'mnist' in dataset_prefix else 500
    BATCH_SIZE = S['B'] # 140 # Split by DataParallel
    LEARNING_RATE = S['LEARNING_RATE'] # 0.0002 # 0.00005
    DATAFILE_COVERINGS = 1
    N_SAMPLES_FOR_LOSS = S['N_LOSS_SAMPLES'] # 1000 # TODO used?
    WEIGHT_DECAY_CONST = 0.0 # 1e-7
    PRINT_EVERY = 50
    SAVE_EVERY = 200
    ALLOW_MODEL_OVERWRITE = True # Whether to train from scratch
    EXIT_EARLY_NB = None
    dataset_to_combine_from = 'mnist-tasp_meshes-test-ld' # put combined above to contruct it
    ### Rotational Consistency  ###
    USE_RC = S['USE_RC']  # True
    RC_COEF = S['RC_COEF']
    RC_SAMPLES_SIZE = S['N_RC_SAMPLES']
    #-- Data Control and Augmentation --#
    activate_filter = False
    PRELOAD_FILES = True
    ROTATE_DATA = True
    if 'MAX_AUG_ROT' in S.keys():
        MAX_ROT_ANGLE = S['MAX_AUG_ROT']
    else:
        MAX_ROT_ANGLE = np.pi  # in [0, pi], rotates in [-a, a]
    GPU_ID = 0 if device_manual is None else int(device_manual)
    print('Use GPU?', USE_GPU, "GPU_ID?", GPU_ID, "Using data parallel?", USE_DP) 
    #################################################################

    import datetime as dt
    cdt = dt.datetime.today().strftime('%m%d%y')
    SAVE_NAME = key + '-' + cdt + '-AE.pt'
    NUM_SUBBATCHES = 1 # Leave at 1
    
    R_TYPE = S['ROTATION_TYPE']
    if R_TYPE == 'z':
        ROTATE_Z_ONLY = True
        ROTATE_Y_ONLY = False
    elif R_TYPE == 'y':
        ROTATE_Z_ONLY = False
        ROTATE_Y_ONLY = True
    else:
        print('Unrecognized rot_type', R_TYPE); sys.exit(0)
    assert not (ROTATE_Z_ONLY and ROTATE_Y_ONLY), "Specify only one rotation"

    ### Checks ###
    if not ALLOW_MODEL_OVERWRITE and os.path.exists(SAVE_NAME):
        print(SAVE_NAME,'already exists!')
        sys.exit(1)

    ### Create Model ###
    ae_model = Autoencoder_IEVAE_fixedDec(
        num_input_points = INPUT_SIZE,
        num_output_points = OUTPUT_SIZE,
        #use_gpu = USE_GPU,
        latent_dim = LATENT_DIM,
        conv_dim = CONV_DIM,
        conv_layer_sizes = CONV_LAYER_SIZES,
        fc_layer_sizes = FC_LAYER_SIZES,
        transformer_positions = TRANSFORMER_POSITIONS,
        DLS = DEC_SIZES, #,
        decoder_normalization = DECNORM
    )

    DEVICE = torch.device("cuda:%d" % GPU_ID)

    ### Prepare Data ###
    is_data_file = lambda s: s.startswith(dataset_prefix) and s.endswith(dataset_suffix)
    dataset_files = [ f for f in os.listdir(dataset_loc) if is_data_file(f) ]
    train_files = [ os.path.join(dataset_loc, f) for f in dataset_files ]
    print('Found data files:', dataset_files)

    #if len(dataset_files) == 0: #
    #    print('Generating combined file')
    #    is_data_filec = lambda s: s.startswith(dataset_to_combine_from) and s.endswith(dataset_suffix)
    #    dataset_filesc = [ f for f in os.listdir(dataset_loc) if is_data_filec(f) ]
    #    train_filesc = [ os.path.join(dataset_loc, f) for f in dataset_filesc ]
    #    print('Found data files:', dataset_filesc)
    #    dataa = []
    #    for tfile in train_filesc:
    #        print('\tReading tfile')
    #        dataset_object = DH.IEVAE_Dataset.read_from_file(
    #                        tfile, file_filter=None, compute_trees=False)
    #        dataa += dataset_object.data
    #        print('\tCurrent length:', len(dataa))
    #    new_file = dataset_prefix + '.pickle'
    #    print('Saving', new_file)
    #    newdh = DH.IEVAE_Dataset( dataa )
    #    newdh.save_to_file(new_file)
    #    sys.exit(0)

    ### Run Training ###
    run_training(
         ae_model,
         dataset_files = train_files,
         path_to_save_model = SAVE_NAME,
         use_data_parallel = USE_DP,
         ### Training parameters ###
         num_epochs = N_EPOCHS,
         batch_size = BATCH_SIZE,
         learning_rate = LEARNING_RATE,
         dataset_coverings = DATAFILE_COVERINGS,
         n_loss_samples = N_SAMPLES_FOR_LOSS,
         l2_reg_weight_decay = WEIGHT_DECAY_CONST, #
         ### Meta-parameters ###
         print_every = PRINT_EVERY,
         save_every = SAVE_EVERY,
         allow_initial_overwrites = ALLOW_MODEL_OVERWRITE,
         exit_early_after_n_batches = EXIT_EARLY_NB, #
         device = DEVICE,
         num_sub_batches = NUM_SUBBATCHES,
         ### Weight constants ###
         alpha_1 = A_1, # Chamfer max-avg weighting
         alpha_2 = A_2, # Approximate Hausdorff max-avg weighting
         chamfer_weight = CHAM_W,
         hausdorff_weight = HAUSS_W,
         ### Rotational Consistency  ###
         use_rc = USE_RC,
         rc_coef = RC_COEF,
         rc_sample_size = RC_SAMPLES_SIZE,
         ### Training data params ###
         file_filter = None, #file_filter,
         preload_files = PRELOAD_FILES,
         rotate_training_data = ROTATE_DATA,
         only_rotate_z_axis = ROTATE_Z_ONLY,
         only_rotate_y_axis = ROTATE_Y_ONLY,
         max_rotation_angle = MAX_ROT_ANGLE
        )

#################################################################
if __name__ == '__main__':
    main()
#################################################################


#
